//
// Created by Zhang Yulei on 7/2/21.
//

#ifndef QPLOT_PLOT_UTIL_H
#define QPLOT_PLOT_UTIL_H

#include "TPaveText.h"
#include "TFrame.h"

TPaveText *CreatePaveText(double x1, double y1, double x2, double y2,
                                 const TString &text, double textsize,
                                 double textfont, int color = 1) {

  auto *tex = new TPaveText();
  tex->SetFillColor(0);
  tex->SetTextSize(0.05);
  tex->SetFillStyle(0);
  tex->SetBorderSize(0);

  tex->AddText(text.Data());
  tex->SetX1NDC(x1);
  tex->SetY1NDC(y1);
  tex->SetX2NDC(x2);
  tex->SetY2NDC(y2);
  tex->SetTextSize(textsize);
  tex->SetTextColor(color);
  tex->SetTextFont(textfont);
  return tex;
}

void GetX1Y1X2Y2(TVirtualPad *c, double &x1, double &y1, double &x2,
                        double &y2) {
  x1 = c->GetFrame()->GetX1() + c->GetLeftMargin();
  y1 = c->GetFrame()->GetY1() + c->GetBottomMargin();
  x2 = c->GetFrame()->GetX2() - c->GetRightMargin();
  y2 = c->GetFrame()->GetY2() - c->GetTopMargin();
}

void PlotLogo_LLP(TCanvas *c, double x_offset, double y_offset) {
  double x1, x2, y1, y2;
  GetX1Y1X2Y2(c, x1, y1, x2, y2);

  x1 += 0.085;
  y1 += 0.02;

  TString str = "";

  double x, y;

  x = x1 - 0.025 + x_offset;
  y = y1 + 0.73 + y_offset;
  str = "CEPC";
  auto *DStext = CreatePaveText(x, y, x, y, str, 0.035, 72, 1);
  DStext->Draw("same");

  double delx = 0.115 * 696 * gPad->GetWh() / (472 * gPad->GetWw()) * 1.5 / 2.5;
  str = "Simulation";
  DStext = CreatePaveText(x + delx, y, x + delx, y, str, 0.032, 42, 1);
  DStext->Draw("same");

  y -= 0.035;
  str = "#sqrt{s} = 240 GeV";
  DStext = CreatePaveText(x, y, x, y, str, 0.025,52, 1);
  DStext->Draw("same");

  y -= 0.035;
  str = "e^{+}e^{-} #rightarrow ZH #rightarrow q#bar{q} + X_{1} + X_{2}";
  DStext = CreatePaveText(x, y, x, y, str, 0.025, 52, 1);
  DStext->Draw("same");

  y -= 0.035;
  str = "X_{1} #rightarrow #nu#bar{#nu}, X_{2} #rightarrow q#bar{q}";
  DStext = CreatePaveText(x, y, x, y, str, 0.025, 52, 1);
  DStext->Draw("same");
}

#endif // QPLOT_PLOT_UTIL_H
