//
// Created by Zhang Yulei on 7/13/21.
//

#include "FF_Helper.h"
#include "yaml-cpp/yaml.h"

#include <map>

using std::string;
using std::map, std::pair;

int main(int argc, char *argv[]) {

    TString dir = "./ROOT/merged/";
    TString norm_suf = "_selected.root";

    vector<double> var_range_t({0., 20., 25., 500});
    std::map<TString, vector<vector<double>>> var_range;
//    var_range.push_back(var_range_t);
//    var_range.push_back(var_range_t);

    map<TString, FF_Sample_Type> sample;
    // Data
    sample.insert(pair("data", FF_Sample_Type::Data));
    // Prompt
    sample.insert(pair("Signal", FF_Sample_Type::Prompt));
    sample.insert(pair("WZ", FF_Sample_Type::Prompt));
    sample.insert(pair("VV", FF_Sample_Type::Prompt));
    sample.insert(pair("ttH", FF_Sample_Type::Prompt));
    sample.insert(pair("ttW", FF_Sample_Type::Prompt));
    sample.insert(pair("ttZ", FF_Sample_Type::Prompt));
    sample.insert(pair("tZ", FF_Sample_Type::Prompt));
    sample.insert(pair("VVV", FF_Sample_Type::Prompt));
    sample.insert(pair("VH", FF_Sample_Type::Prompt));
    // Fakes
    sample.insert(pair("WJ", FF_Sample_Type::MCFake));
    sample.insert(pair("ZJ_DY", FF_Sample_Type::MCFake));
    sample.insert(pair("ZJ_Sherpa", FF_Sample_Type::MCFake));
    sample.insert(pair("tt", FF_Sample_Type::MCFake));
    sample.insert(pair("VG", FF_Sample_Type::MCFake));

    if (argc == 2) {
        cout << "-- Reading config file from: " << argv[1] << endl;
        auto Node = YAML::LoadFile(argv[1]);

        dir = Node["sample_dir"].as<string>();
        norm_suf = Node["sample_suffix"].as<string>();
        var_range.clear();

        for (auto lep: {"electron", "muon"}) {
            var_range.insert({lep, {}});
            for (auto vr: Node["var_range"][lep]) {
                var_range_t.clear();
                for (auto num: vr) {
                    var_range_t.push_back(num.as<double>());
                }
                var_range.at(lep).push_back(var_range_t);
            }
        }

        sample.clear();
        for (auto d: Node["Samples"]["data"])
            sample.insert(pair(d.as<string>(), FF_Sample_Type::Data));
        for (auto d: Node["Samples"]["prompt"])
            sample.insert(pair(d.as<string>(), FF_Sample_Type::Prompt));
        for (auto d: Node["Samples"]["mcfake"])
            sample.insert(pair(d.as<string>(), FF_Sample_Type::MCFake));
    }


    /*
     * Main Block
     */
    FF_Helper ff;
    cout << "-- Totally " << sample.size() << " samples..." << endl;
    for (const auto &s: sample) {
        ff.add_sample(dir + s.first + norm_suf, s.second);
    }
    // muon
    ff.process_FF(var_range.at("muon").at(0), "lep_Pt_1", "muon p_{T}^{l_{1}} [GeV]", FF_Lepton_Type::Muon);
    ff.process_FF(var_range.at("muon").at(1), "lep_Pt_2", "muon p_{T}^{l_{2}} [GeV]", FF_Lepton_Type::Muon);

    ff.draw_FF("p_{T}^{#mu} [GeV]", "ff_muon", vector<TString>({"#slash{#mu}_{1}", "#slash{#mu}_{2}"}));

    double mu_d = ff.calculate_FF_yields(var_range.at("muon").at(0), FF_Lepton_Type::Muon, false);
    double mu_c = ff.calculate_samples_yields(FF_Lepton_Type::Muon, false);

    double mu_dd = ff.calculate_FF_yields(var_range.at("muon").at(0), FF_Lepton_Type::Muon, true);
    double mu_com = ff.calculate_samples_yields(FF_Lepton_Type::Muon, true);

    ff.clean();
    // electron
    ff.process_FF(var_range.at("electron").at(0), "lep_Pt_1", "e p_{T}^{l_{1}} [GeV]", FF_Lepton_Type::Electron);
    ff.process_FF(var_range.at("electron").at(1), "lep_Pt_2", "e p_{T}^{l_{2}} [GeV]", FF_Lepton_Type::Electron);

    ff.draw_FF("p_{T}^{e} [GeV]", "ff_electron", vector<TString>({"#slash{e}_{1}", "#slash{e}_{2}"}));

    double el_d = ff.calculate_FF_yields(var_range.at("electron").at(0), FF_Lepton_Type::Electron, false);
    double el_c = ff.calculate_samples_yields(FF_Lepton_Type::Electron, false);

    double el_dd = ff.calculate_FF_yields(var_range.at("electron").at(0), FF_Lepton_Type::Electron, true);
    double el_com = ff.calculate_samples_yields(FF_Lepton_Type::Electron, true);

    cout << "lee DD Fakes: " << el_d << endl;
    cout << "lee Data-Prompt: " << el_c << endl;
    cout << "lmm DD Fakes: " << mu_d << endl;
    cout << "lmm Data-Prompt: " << mu_c << endl;
    cout << "lem DD Fakes: " << mu_dd + el_dd << endl;
    cout << "lem Data-Prompt: " << mu_com + el_com << endl;

    return 0;
}
