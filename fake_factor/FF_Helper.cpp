//
// Created by Zhang Yulei on 7/13/21.
//

#include "FF_Helper.h"
#include "TPad.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TLegend.h"

#include "DarkStyle.h"
#include "Plot_Util.h"

#include <numeric>

using std::to_string;

FF_Helper::FF_Helper() {
    t_data = new TChain("total");
    t_prompt = new TChain("total");
    t_mcfake = new TChain("total");

    t.push_back(t_data);
    t.push_back(t_prompt);
    t.push_back(t_mcfake);

    weight = "weight";
    num_type = vector<TCut>({"(FlavorCategory == 1 || FlavorCategory == 5)",
                             "(FlavorCategory == 4 || FlavorCategory == 8)",
                             "(FlavorCategory == 3 || FlavorCategory == 7)",
                             "(FlavorCategory == 2 || FlavorCategory == 6)"});
    den_type = vector<TCut>({"FakeCategory == 1", "FakeCategory == 2", "FakeCategory == 3", "FakeCategory == 4"});

    SetDarkSHINEStyle();
}

FF_Helper::~FF_Helper() {
    for (auto h: t) delete h;
}

void FF_Helper::add_sample(const TString &f_name, FF_Sample_Type st) {
    cout << "-- Adding sample: " << f_name << endl;
    switch (st) {
        case FF_Sample_Type::Data:
            t_data->Add(f_name);
            break;
        case FF_Sample_Type::Prompt:
            t_prompt->Add(f_name);
            break;
        case FF_Sample_Type::MCFake:
            t_mcfake->Add(f_name);
            break;
        default:
            break;
    }
}

void
FF_Helper::process_FF(const vector<double> &var_range, const TString &var, const TString &var_title,
                      FF_Lepton_Type lt) {
    int n_xbin = static_cast<int>(var_range.size()) - 1;
    int region_num = (lt == FF_Lepton_Type::Electron) ? 0 : 1;

    cout << "-- Drawing: " << var << endl;

    auto c = new TCanvas("c", "c", 2200, 2000);

    vector<TH1D *> hist_num;
    {// Draw NUM
        hist_num.reserve(3);
        for (int i = 0; i < 3; ++i) {
            hist_num.push_back(
                    new TH1D(("hnum" + to_string(i)).data(), ("hnum" + to_string(i)).data(), n_xbin, var_range.data()));
        }

        for (int i = 0; i < 3; ++i) {
            t.at(i)->Draw(var + ">>hnum" + to_string(i),
                          weight * (num_type.at(region_num) && "Evt_FF_CR_C == 1"));

//            c->SaveAs(Form("hnum%i.png", i));
        }
    }

    vector<TH1D *> hist_den;
    {// Draw DEN
        hist_den.reserve(3);
        for (int i = 0; i < 3; ++i) {
            hist_den.push_back(
                    new TH1D(("hden" + to_string(i)).data(), ("hden" + to_string(i)).data(), n_xbin, var_range.data()));
        }

        for (int i = 0; i < 3; ++i) {
            t.at(i)->Draw(var + ">>hden" + to_string(i), weight * (den_type.at(region_num) && "Evt_FF_CR_D == 1"));

//            c->SaveAs(Form("hden%i.png", i));
        }
    }

    { // Calculate FF by NUM/DEN
        auto h_ff_num = dynamic_cast<TH1D *>(hist_num.at(0)->Clone("h_ff_num"));
        h_ff_num->Add(hist_num.at(1), -1.);

        auto h_ff_den = dynamic_cast<TH1D *>(hist_den.at(0)->Clone("h_ff_den"));
        h_ff_den->Add(hist_den.at(1), -1.);

//        h_ff_num->Draw("hist");
//        c->SaveAs(Form("hffnum.png"));
//        h_ff_den->Draw("hist");
//        c->SaveAs(Form("hffden.png"));

        h_ff_num->Divide(h_ff_den);

        // Register FF results
        ff.clear();
        ff_err.clear();
        x.clear();
        x_err.clear();
        for (int i = 1; i <= h_ff_num->GetNbinsX(); ++i) {
            ff.push_back(h_ff_num->GetBinContent(i));
            ff_err.push_back(h_ff_num->GetBinError(i));
            x.push_back(h_ff_num->GetBinCenter(i));
            x_err.push_back(h_ff_num->GetBinWidth(i) / 2.);
        }
        all_ff.push_back(ff);
        all_ff_err.push_back(ff_err);
        all_x.push_back(x);
        all_x_err.push_back(x_err);

        dump_ffs();

        delete h_ff_num;
        delete h_ff_den;
    }

    for (auto h: hist_num) delete h;
    for (auto h: hist_den) delete h;

    delete c;
}

void FF_Helper::draw_FF(const TString &var_x, const TString &file_name, const vector<TString> &leg_name) {
    auto c = new TCanvas("c1", "c1", 2200, 2000);
    c->cd();
    gStyle->SetPalette(kWaterMelon);
    gPad->SetLogx();
    vector<TGraphErrors *> grs;
    double max_y = 0.;
    for (unsigned i = 0; i < all_ff.size(); ++i) {
        int n = static_cast<int>(all_ff.at(i).size());
        grs.push_back(new TGraphErrors(n, all_x.at(i).data(), all_ff.at(i).data(),
                                       all_x_err.at(i).data(), all_ff_err.at(i).data()));

        if (auto m = (*max_element(all_ff.at(i).begin(), all_ff.at(i).end())); m > max_y)
            max_y = m;
    }
    auto mg = new TMultiGraph();
    for (auto gr: grs) {
        gr->SetLineWidth(2);
        gr->SetMarkerSize(3);
        mg->Add(gr);
    }
    mg->Draw("AP pmc plc");
    mg->GetXaxis()->SetTitle(var_x);
    mg->GetYaxis()->SetTitle("Fake Factor");
    mg->SetMaximum(max_y * 1.8);

    // Writing Latex and legend
    TPaveText *DStext;
    TLegend *leg;
    {
        double x1, x2, y1, y2;
        GetX1Y1X2Y2(c, x1, y1, x2, y2);

        leg = new TLegend(0.75, 0.77, 0.92, 0.88);
        for (unsigned i = 0; i < grs.size(); ++i) {
            leg->AddEntry(grs.at(i), leg_name.at(i), "lp");
        }

        TString str = "";

        double x_, y_;

        x_ = 0.22;
        y_ = 0.88;
        str = "ATLAS";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.035, 72, 1);
        DStext->Draw("same");

        double delx = 0.115 * 696 * gPad->GetWh() / (472 * gPad->GetWw()) * 1.5 / 2.0;
        str = "Simulation Internal";
        DStext = CreatePaveText(x_ + delx, y_, x_ + delx, y_, str, 0.032, 42, 1);
        DStext->Draw("same");

        y_ -= 0.035;
        str = "13 TeV, 139 fb^{-1}";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.025, 42, 1);
        DStext->Draw("same");

        y_ -= 0.035;
        str = "diHiggs 3-lepton";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.025, 42, 1);
        DStext->Draw("same");
    }
    leg->Draw("same");

    c->Update();
    c->SaveAs(file_name + ".pdf");
    c->SaveAs(file_name + ".png");

    delete c;
    delete mg;
}


void FF_Helper::dump_ffs() {
    cout << "Fake Factor: [";
    for (auto num: ff) {
        printf("%5.3f, ", num);
    }
    cout << "]" << endl;

    cout << "Fake Factor Error: [";
    for (auto num: ff_err) {
        printf("%5.3f, ", num);
    }
    cout << "]" << endl;

    cout << "X: [";
    for (auto num: x) {
        printf("%5.3f, ", num);
    }
    cout << "]" << endl;
}

double FF_Helper::calculate_FF_yields(const vector<double> &var_range, FF_Lepton_Type lt, bool combine) {
    int n_xbin = static_cast<int>(var_range.size()) - 1;
    TCut cat_cut = (lt == FF_Lepton_Type::Electron) ? den_type.at(0) : den_type.at(1);

    if (combine) cat_cut = ((lt == FF_Lepton_Type::Electron) ? den_type.at(2) : den_type.at(3));

    cout << "-- Calculating yields..." << endl;

    vector<TString> fake_cut;
    for (int j = 0; j < n_xbin; ++j) {
        TString tmp1 = ("( lep_Pt_1 >= " + to_string(var_range.at(j)) +
                        ((j < n_xbin - 1) ? (" && lep_Pt_1 < " + to_string(var_range.at(j + 1))) : "")) +
                       ") * (Fake_Lep_idx == 1)" +
                       " *" + (to_string(all_ff.at(0).at(j)));
        TString tmp2 = ("( lep_Pt_2 >= " + to_string(var_range.at(j)) +
                        ((j < n_xbin - 1) ? (" && lep_Pt_2 < " + to_string(var_range.at(j + 1))) : "")) +
                       ") * (Fake_Lep_idx == 2)" +
                       " *" + (to_string(all_ff.at(1).at(j)));
        fake_cut.emplace_back(Form("((%s) + (%s))", tmp1.Data(), tmp2.Data()));
    }
    auto final_cut = std::accumulate(fake_cut.begin(), fake_cut.end(), TString("0"),
                                     [](const TString &a, const TString &b) {
                                         return a + TString("+") + b;
                                     });

    auto c = new TCanvas("c", "c", 2200, 2000);

    vector<TH1D *> hist_num;
    {// Draw NUM
        hist_num.reserve(2);
        for (int i = 0; i < 2; ++i) {
            hist_num.push_back(
                    new TH1D(("h" + to_string(i)).data(), ("h" + to_string(i)).data(), 2, 0, 2));
        }

        for (int i = 0; i < 2; ++i) {
//            t.at(i)->Draw(("Evt_FF_CR_B >> h" + to_string(i)).c_str(),
//                          weight * (den_type.at(3) && "Evt_FF_CR_B == 1"));

            t.at(i)->Draw(("Evt_FF_CR_B >> h" + to_string(i)).c_str(),
                          weight * (cat_cut && "Evt_FF_CR_B == 1") * Form("(%s)", final_cut.Data()));
        }
    }

    auto h_ff_num = dynamic_cast<TH1D *>(hist_num.at(0)->Clone("h_ff"));
    h_ff_num->Add(hist_num.at(1), -1.);

    double result = h_ff_num->Integral();
    cout << ((lt == FF_Lepton_Type::Electron) ? "Electron ==> " : "Muon ==> ") << "FF fakes: "
            << result << ((combine) ? "lem" : "") << endl;

    for (auto h: hist_num) delete h;
    delete c;

    return result;
}

double FF_Helper::calculate_samples_yields(FF_Lepton_Type lt, bool combine) {
    TCut cat_cut = (lt == FF_Lepton_Type::Electron) ? num_type.at(0) : num_type.at(1);

    if (combine) cat_cut = ((lt == FF_Lepton_Type::Electron) ? num_type.at(2) : num_type.at(3));

    auto c = new TCanvas("c", "c", 2200, 2000);
    vector<TH1D *> hist_num;
    {// Draw NUM
        hist_num.reserve(2);
        for (int i = 0; i < 2; ++i) {
            hist_num.push_back(
                    new TH1D(("h" + to_string(i)).data(), ("h" + to_string(i)).data(), 2, 0, 2));
        }

        for (int i = 0; i < 2; ++i) {
            t.at(i)->Draw(("Evt_FF_CR_A >> h" + to_string(i)).c_str(), weight * (cat_cut && "Evt_FF_CR_A == 1"));
        }
    }

    auto h_ff_num = dynamic_cast<TH1D *>(hist_num.at(0)->Clone("h_ff"));
    h_ff_num->Add(hist_num.at(1), -1.);

    double result = h_ff_num->Integral();
    cout << ((lt == FF_Lepton_Type::Electron) ? "Electron ==> " : "Muon ==> ") << "Data - Prompt: "
         << result << ((combine) ? " (lem)" : "") << endl;

    for (auto h: hist_num) delete h;
    delete c;

    return result;
}

void FF_Helper::clean() {
    ff.clear();
    ff_err.clear();
    all_ff.clear();
    all_ff_err.clear();
    all_x.clear();
    all_x_err.clear();
}



