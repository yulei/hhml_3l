//
// Created by Zhang Yulei on 7/13/21.
//

#ifndef HHML_3L_FF_HELPER_H
#define HHML_3L_FF_HELPER_H

#include "TChain.h"
#include "TString.h"
#include "TH1D.h"
#include <TCut.h>
#include "TCanvas.h"

#include <vector>
#include <iostream>

using std::vector, std::tuple, std::tie;
using std::cout, std::cerr, std::endl;

enum class FF_Sample_Type {
    None, Data, Prompt, MCFake
};

enum class FF_Lepton_Type {
    None, Electron, Muon
};

class FF_Helper {
public:
    FF_Helper();

    ~FF_Helper();

    void add_sample(const TString &fn, FF_Sample_Type st = FF_Sample_Type::None);

    void
    process_FF(const vector<double> &var_range, const TString &var, const TString &var_title, FF_Lepton_Type lt);

    void draw_FF(const TString &var_x, const TString &file_name, const vector<TString>& leg_name);

    void dump_ffs();

    double calculate_FF_yields(const vector<double> &var_range, FF_Lepton_Type lt, bool combine = false);

    double calculate_samples_yields(FF_Lepton_Type lt, bool combine = false);

    void clean();

private:
    TChain *t_data;
    TChain *t_prompt;
    TChain *t_mcfake;

    vector<TChain *> t;

    TCut weight;
    vector<TCut> num_type; // prompt type
    vector<TCut> den_type; // anti-tight type

    vector<double> ff;
    vector<double> ff_err;

    vector<vector<double>> all_ff;
    vector<vector<double>> all_ff_err;

    vector<double> x;
    vector<double> x_err;

    vector<vector<double>> all_x;
    vector<vector<double>> all_x_err;
};


#endif //HHML_3L_FF_HELPER_H
