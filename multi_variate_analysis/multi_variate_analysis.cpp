//
// Created by Zhang Yulei on 8/16/21.
//

#include "mva.h"

int main(int argc, char **argv) {

    mva m;
    if (argc >= 2)
        m.ReadConfig(argv[1]);
    else
        m.ReadConfig();

    m.Process();
}