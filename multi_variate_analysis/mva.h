//
// Created by Zhang Yulei on 8/16/21.
//

#ifndef HHML_3L_MVA_H
#define HHML_3L_MVA_H

#include <cstdlib>
#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <iterator>
#include <sstream>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TRandom3.h"

#include "TMVA/Factory.h"
#include "TMVA/Reader.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#include "TMVA/CrossValidation.h"

#include "yaml-cpp/yaml.h"

#include <vector>

using std::vector;

enum class status {
    Train, Apply, All
};

class mva {
public:
    mva();

    ~mva() = default;

    void ReadConfig(const TString &file = "mva.yaml");

    void Process();

    void Train();

    void Apply();

    status cur_status;
private:
    TChain *t_data{};
    TChain *t_sig{};
    TChain *t_bkg{};

    TString output_dataset;
    TString output_dir;
    TString sample_dir;
    TString sample_suffix;
    TString fake_suffix;

    YAML::Node Node;

    vector<std::string> in_samples;
    vector<std::string> int_var;
    vector<std::string> double_var;
    vector<std::string> spectator;

    vector<std::string> var_vec;
    std::unordered_map<std::string, float> var_map;
    std::map<std::string, ULong64_t *> spectator_map;
    std::map<std::string, int *> int_map;
    std::map<std::string, double *> double_map;

    int training_fold{};
    TString global_selection;
};


#endif //HHML_3L_MVA_H
