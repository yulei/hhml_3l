//
// Created by Zhang Yulei on 8/16/21.
//

#include "mva.h"

#include <utility>
#include <iostream>
#include <filesystem>
#include <TTreeReader.h>
#include "TTreeReaderArray.h"

using std::cout, std::endl, std::cerr;
namespace fs = std::filesystem;

mva::mva() {
    cur_status = status::All;
    t_data = new TChain("total");
    t_sig = new TChain("total");
    t_bkg = new TChain("total");
}

void mva::Train() {
    TMVA::Tools::Instance();

    auto outputFile = TFile::Open(output_dataset + "_TMVAOutput.root", "RECREATE");

    TMVA::Factory factory("TMVAClassification", outputFile,
                          "!V:ROC:!Correlations:!Silent:Color:DrawProgressBar:AnalysisType=Classification");

    auto loader = new TMVA::DataLoader("BDT_" + output_dataset);

    /* Int */
    for (const auto &var: int_var) {
        loader->AddVariable(var);
    }
    /* Double */
    for (const auto &var: double_var) {
        loader->AddVariable(var);
    }
    /* Spectators */
    for (const auto &var: spectator) {
        loader->AddSpectator(var);
    }

    long test_sig = 1;
    long test_bkg = 1;
//    long test_sig = (long) (((double) t_sig->Draw("weight", global_selection.Data()) / training_fold) *
//                            (training_fold - 1) * 0.25);
//    long test_bkg = (long) (((double) t_bkg->Draw("weight", global_selection.Data()) / training_fold) *
//                            (training_fold - 1) * 0.25);

    cout << "-- Signal Test Entries: " << test_sig << endl;
    cout << "-- Background Test Entries: " << test_bkg << endl;

    loader->AddSignalTree((TTree *) t_sig, 1.0);   //signal weight  = 1
    loader->AddBackgroundTree((TTree *) t_bkg, 1.0);   //background weight = 1

    loader->SetWeightExpression("weight");

    TString PrepareOption = Form("!V"
                                 ":nTest_Signal=%ld"
                                 ":nTest_Background=%ld"
                                 ":SplitMode=Random",
                                 test_sig, test_bkg);
    loader->PrepareTrainingAndTestTree(global_selection.Data(), global_selection.Data(),
                                       PrepareOption.Data());

    UInt_t numFolds = training_fold;
    TString analysisType = "Classification";
    TString splitType = "Deterministic";
    TString splitExpr = "long(fabs([EvtNum]))%long([NumFolds])";

    TString cvOptions = Form("!V"
                             ":!Silent"
                             ":ModelPersistence"
                             ":AnalysisType=%s"
                             ":SplitType=%s"
                             ":NumFolds=%i"
                             ":SplitExpr=%s"
                             ":FoldFileOutput=True",
                             analysisType.Data(), splitType.Data(), numFolds,
                             splitExpr.Data());

    TMVA::CrossValidation cv{"TMVACrossValidation", loader, outputFile, cvOptions};

    //Boosted Decision Trees
//    cv.BookMethod(TMVA::Types::kBDT, "BDT",
//                  "!H:!V:NTrees=500:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20:IgnoreNegWeightsInTraining=True");

    cv.BookMethod(TMVA::Types::kBDT, "BDTG",
                  "!H:!V:NTrees=1000:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=20:MaxDepth=3:NegWeightTreatment=IgnoreNegWeightsInTraining");


    cv.Evaluate();

    // --------------------------------------------------------------------------

    //
    // Process some output programmatically, printing the ROC score for each
    // booked method.
    //
    size_t iMethod = 0;
    for (auto &&result: cv.GetResults()) {
        std::cout << "Summary for method " << cv.GetMethods()[iMethod++].GetValue<TString>("MethodName")
                  << std::endl;
        for (UInt_t iFold = 0; iFold < cv.GetNumFolds(); ++iFold) {
            std::cout << "\tFold " << iFold << ": "
                      << "ROC int: " << result.GetROCValues()[iFold]
                      << ", "
                      << "BkgEff@SigEff=0.3: " << result.GetEff30Values()[iFold]
                      << std::endl;
        }
    }

    // --------------------------------------------------------------------------

    //
    // Save the output
    //
    outputFile->Close();

    std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
    std::cout << "==> TMVACrossValidation is done!" << std::endl;
}

void mva::Apply() {
    // This loads the library
    TMVA::Tools::Instance();
    // Set up the TMVA::Reader
    auto *reader = new TMVA::Reader("");

    /* Int */
    for (const auto &var: int_var) {
        reader->AddVariable(var, &var_map.at(var));
    }
    /* Double */
    for (const auto &var: double_var) {
        reader->AddVariable(var, &var_map.at(var));
    }
    /* Spectators */
    for (const auto &var: spectator) {
        reader->AddSpectator(var, &var_map.at(var));
    }

    // Book the serialised methods
    TString jobname("TMVACrossValidation");
    {
        TString methodName = "BDT_" + output_dataset;
        TString weightfile = "BDT_" + output_dataset + "/weights/TMVACrossValidation_BDTG.weights.xml";

        Bool_t weightfileExists = !gSystem->AccessPathName(weightfile);
        if (weightfileExists) {
            reader->BookMVA(methodName, weightfile);
        } else {
            std::cout << "Weightfile for method " << methodName << " not found."
                                                                   " Did you run TMVACrossValidation with a specified"
                                                                   " splitExpr?" << std::endl;
            exit(0);
        }
    }

    for (const auto &in_sample: in_samples) {
        fs::copy_file((sample_dir + in_sample).Data(), (output_dir + in_sample).Data(),
                      fs::copy_options::overwrite_existing);

        auto cur_file = new TFile((output_dir + in_sample).Data(), "UPDATE");
        auto cur_tree = cur_file->Get<TTree>("total");
        auto nentries = cur_tree->GetEntries();
        std::cout << "-- " << in_sample << ": total entries of " << nentries << std::endl;

        double mva_score;
        auto b_score = cur_tree->Branch("BDTG_score", &mva_score, "BDTG_score/D");

        for (const auto &var: int_var) {
            cur_tree->SetBranchAddress(var.data(), int_map.at(var));
        }
        for (const auto &var: double_var) {
            cur_tree->SetBranchAddress(var.data(), double_map.at(var));
        }
        for (const auto &var: spectator) {
            cur_tree->SetBranchAddress(var.data(), spectator_map.at(var));
        }
        bool Evt_SR;
        cur_tree->SetBranchAddress("Evt_SR", &Evt_SR);

        for (auto i = 0; i < nentries; i++) {
//            for (auto i = 0; i < 20; i++) {
            auto module = nentries >= 20 ? nentries / 20 : 1;
            if (i % module == 0) cout << "==> evt = " << i << endl;
            cur_tree->GetEntry(i);

            for (const auto &var: int_var) {
                var_map.at(var) = (float) (*int_map.at(var));
            }
            for (const auto &var: double_var) {
                var_map.at(var) = (float) (*double_map.at(var));
            }
            for (const auto &var: spectator) {
                var_map.at(var) = static_cast<float>(*spectator_map.at(var));
            }

            mva_score = reader->EvaluateMVA("BDT_" + output_dataset);

            b_score->Fill();
        }

        cur_file->cd();
        cur_tree->Write("", TObject::kOverwrite);
        cur_file->Close();

        delete cur_file;

        std::cout << "--" << in_sample << ": done ..." << std::endl;
    }
    std::cout << "Done ..." << std::endl;
}

void mva::Process() {
    if (cur_status == status::Train || cur_status == status::All)
        this->Train();
    if (cur_status == status::Apply || cur_status == status::All)
        this->Apply();
}


void mva::ReadConfig(const TString &file) {
    cout << "-- Reading config file from: " << file << endl;
    Node = YAML::LoadFile(file.Data());

    output_dataset = Node["output_dataset"].as<std::string>();

    sample_dir = Node["sample_dir"].as<std::string>();
    sample_suffix = Node["sample_suffix"].as<std::string>();
    fake_suffix = Node["fake_suffix"].as<std::string>();
    output_dir = Node["output_dir"].as<std::string>();

    auto rt = Node["Run_Type"].as<std::string>();
    if (rt == "Train") {
        cur_status = status::Train;
    } else if (rt == "Apply") {
        cur_status = status::Apply;
    } else if (rt == "All") {
        cur_status = status::All;
    }
    training_fold = Node["training_fold"].as<int>();

    global_selection = Node["Global_Selection"].as<std::string>();

    auto sn = Node["Samples"];
    for (auto data: sn["data"]) {
        t_data->Add((std::string(sample_dir).append(data.as<std::string>()).append(sample_suffix)).c_str());
        in_samples.emplace_back(std::string(data.as<std::string>()).append(sample_suffix));
    }
    for (auto data: sn["signal"]) {
        t_sig->Add((std::string(sample_dir).append(data.as<std::string>()).append(sample_suffix)).c_str());
        in_samples.emplace_back(std::string(data.as<std::string>()).append(sample_suffix));
    }
    for (auto data: sn["prompt"]) {
        t_bkg->Add((std::string(sample_dir).append(data.as<std::string>()).append(sample_suffix)).c_str());
        in_samples.emplace_back(std::string(data.as<std::string>()).append(sample_suffix));
    }
    for (auto data: sn["mcfake"]) {
        t_bkg->Add((std::string(sample_dir).append(data.as<std::string>()).append(sample_suffix)).c_str());
        in_samples.emplace_back(std::string(data.as<std::string>()).append(sample_suffix));
    }
    for (auto data: sn["ddfake"]) {
        t_bkg->Add((std::string(sample_dir).append(data.as<std::string>()).append(fake_suffix)).c_str());
        in_samples.emplace_back(std::string(data.as<std::string>()).append(fake_suffix));
    }

    auto vn = Node["Variables"];
    for (auto var: vn["int"]) {
        int_var.emplace_back(var.as<std::string>().data());
        var_map.insert(std::make_pair(var.as<std::string>(), 0.));
        int_map.insert(std::make_pair(var.as<std::string>(), new int));
    }
    for (auto var: vn["double"]) {
        double_var.emplace_back(var.as<std::string>().data());
        var_map.insert(std::make_pair(var.as<std::string>(), 0.));
        double_map.insert(std::make_pair(var.as<std::string>(), new double));
    }
    for (auto var: vn["spectator"]) {
        spectator.emplace_back(var.as<std::string>().data());
        var_map.insert(std::make_pair(var.as<std::string>(), 0.));
        spectator_map.insert(std::make_pair(var.as<std::string>(), new ULong64_t));
    }
    var_vec.insert(var_vec.end(), int_var.begin(), int_var.end());
    var_vec.insert(var_vec.end(), double_var.begin(), double_var.end());
    var_vec.insert(var_vec.end(), spectator.begin(), spectator.end());

}