//
// Created by Zhang Yulei on 7/13/21.
//

#ifndef HHML_3L_NORM_HELPER_H
#define HHML_3L_NORM_HELPER_H

#include "TChain.h"
#include "TString.h"
#include "TH1D.h"
#include <TCut.h>

#include <vector>
#include <iostream>

using std::vector, std::tuple, std::tie;
using std::cout, std::cerr, std::endl;

enum Norm_Sample_Type {
    None, Data, Target, Other
};

typedef tuple<int, double, double> Var_Range;

class Norm_Helper {
public:
    Norm_Helper();

    ~Norm_Helper();

    void add_sample(const TString &fn, Norm_Sample_Type st = None);

    void normalize(const TString &var_name, Var_Range var_r, const TCut &region_cut);

    static void dump_info(TH1D *h, const TString &info_suffix);

    void dump_nfs(double cur_nf = 1.0);

    void draw_nfs(const vector<double>& nf = vector<double>(), const vector<double>& nfe = vector<double>(),
                  const TString &var_name = "nJets");

    [[nodiscard]] const vector<double> &getNormFactor() const {
        return norm_factor;
    }

    [[nodiscard]] const vector<double> &getNormFactorErr() const {
        return norm_factor_err;
    }

private:
    TChain *t_data;
    TChain *t_target_mc;
    TChain *t_other_mc;

    TH1D *var_hist{};

    vector<double> norm_factor;
    vector<double> norm_factor_err;
};


#endif //HHML_3L_NORM_HELPER_H
