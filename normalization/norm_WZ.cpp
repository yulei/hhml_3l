//
// Created by Zhang Yulei on 7/13/21.
//

#include "Norm_Helper.h"

#include "yaml-cpp/yaml.h"
#include <map>
#include <string>

using std::map, std::pair, std::string;


int main(int argc, char *argv[]) {

    TString dir = "./ROOT/merged/";
    TString norm_suf = "_selected.root";
    TString fake_suf = "_fake.root";

    TCut region_cut = "Evt_WZ_CR == 1";
    TString var_name = "nJets";
    Var_Range var_r(5, 0, 5);

    map<TString, Norm_Sample_Type> sample;

    sample.insert(pair("data", Data));
    sample.insert(pair("WZ", Target));

    // Prompt
    sample.insert(pair("VV", Other));
    sample.insert(pair("ttH", Other));
    sample.insert(pair("ttW", Other));
    sample.insert(pair("ttZ", Other));
    sample.insert(pair("tZ", Other));
    sample.insert(pair("VVV", Other));
    sample.insert(pair("Signal", Other));
    sample.insert(pair("VH", Other));

    // Fakes
    sample.insert(pair("WJ", Other));
    sample.insert(pair("ZJ_DY", Other));
    sample.insert(pair("ZJ_Sherpa", Other));
    sample.insert(pair("tt", Other));
    sample.insert(pair("VG", Other));

    vector<double> nf;
    vector<double> nfe;

    if (argc == 2) {
        auto Node = YAML::LoadFile(argv[1]);

        dir = Node["sample_dir"].as<string>();
        norm_suf = Node["sample_suffix"].as<string>();
        region_cut = Node["region_cut"].as<string>().data();
        var_name = Node["var_name"].as<string>();
        var_r = Var_Range(Node["var_range"][0].as<int>(),
                          Node["var_range"][1].as<double>(),
                          Node["var_range"][2].as<double>());

        sample.clear();
        for (auto d: Node["Samples"]["data"])
            sample.insert(pair(d.as<string>(), Data));
        for (auto d: Node["Samples"]["target"])
            sample.insert(pair(d.as<string>(), Target));
        for (auto d: Node["Samples"]["other"])
            sample.insert(pair(d.as<string>(), Other));

        if (auto d = Node["nf_value"]; d.IsDefined()) {
            for (auto val : d) nf.push_back(val.as<double>());
        }
        if (auto d = Node["nf_error"]; d.IsDefined()) {
            for (auto val : d) nfe.push_back(val.as<double>());
        }
        assert(nf.size() == nfe.size());
    }

    Norm_Helper nh;
    for (const auto &s: sample) {
        nh.add_sample(dir + s.first + norm_suf, s.second);
    }
    nh.normalize(var_name, var_r, region_cut);

    if (nf.empty() && nfe.empty())
        nh.draw_nfs();
    else
        nh.draw_nfs(nf, nfe);

    return 0;
}
