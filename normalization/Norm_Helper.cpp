//
// Created by Zhang Yulei on 7/13/21.
//


#include "Norm_Helper.h"
#include "TCanvas.h"
#include "Plot_Util.h"
#include "DarkStyle.h"
#include "TGraphErrors.h"
#include "TLine.h"

void Norm_Helper::add_sample(const TString &f_name, Norm_Sample_Type st) {

    cout << "-- Adding sample: " << f_name << endl;

    switch (st) {
        case Data:
            t_data->Add(f_name);
            break;
        case Target:
            t_target_mc->Add(f_name);
            break;
        case Other:
            t_other_mc->Add(f_name);
            break;
        default:
            break;
    }
}

void Norm_Helper::normalize(const TString &var_name, Var_Range var_r, const TCut &region_cut) {
    int xbin = std::get<0>(var_r);
    double xmin = std::get<1>(var_r);
    double xmax = std::get<2>(var_r);
    TCut weight = "weight";

    if (xbin <= 0) cerr << "Error with setting norm config." << endl;

    TCanvas c("c","c",1000, 800);

    TH1D hist_data("h_data", "h_data", xbin, xmin, xmax);
    TH1D hist_target("h_target", "h_target", xbin, xmin, xmax);
    TH1D hist_other("h_other", "h_other", xbin, xmin, xmax);

    t_data->Draw(var_name + ">>h_data", region_cut);
    t_target_mc->Draw(var_name + ">>h_target", region_cut * weight);
    t_other_mc->Draw(var_name + ">>h_other", region_cut * weight);

    dump_info(&hist_data, "   ");
    dump_info(&hist_other, " - ");
    dump_info(&hist_target, " / ");

    var_hist = dynamic_cast<TH1D *>(hist_data.Clone("h_norm"));
    var_hist->Add(&hist_other, -1);
    var_hist->Divide(&hist_target);

    dump_info(var_hist, " = ");

    norm_factor.clear();
    norm_factor_err.clear();
    for (int i = 1; i <= xbin; ++i) {
        norm_factor.push_back(var_hist->GetBinContent(i));
        norm_factor_err.push_back(var_hist->GetBinError(i));
    }

    { // Print Results
        cout << "Norm Factor: [";
        for (auto num: norm_factor) {
            printf("%5.3f, ", num);
        }
        cout << "]" << endl;

        cout << "Norm Factor Error: [";
        for (auto num: norm_factor_err) {
            printf("%5.3f, ", num);
        }
        cout << "]" << endl;
    }
}

Norm_Helper::Norm_Helper() {
    t_data = new TChain("total");
    t_target_mc = new TChain("total");
    t_other_mc = new TChain("total");
}

Norm_Helper::~Norm_Helper() {
    delete t_data;
    delete t_target_mc;
    delete t_other_mc;
}

void Norm_Helper::dump_info(TH1D *h, const TString &info_suffix) {
    cout << info_suffix << "[";
    for (int i = 1; i <= h->GetNbinsX(); i++) {
        printf("%5.3f, ", h->GetBinContent(i));
    }
    cout << "]" << endl;
}

void Norm_Helper::dump_nfs(double cur_nf) {
    cout << "Norm Factor: [";
    for (auto num: norm_factor) {
        printf("%5.3f, ", num * cur_nf);
    }
    cout << "]" << endl;

    cout << "Norm Factor Error: [";
    for (auto num: norm_factor_err) {
        printf("%5.3f, ", num * cur_nf);
    }
    cout << "]" << endl;

}

void Norm_Helper::draw_nfs(const vector<double>& nf, const vector<double>& nfe, const TString &var_name) {
    SetDarkSHINEStyle();

    auto c = new TCanvas("c1", "c1", 2200, 2000);
    c->cd();

    if (!nf.empty()) norm_factor = nf;
    if (!nfe.empty()) norm_factor_err = nfe;

    int n = static_cast<int>(norm_factor.size());
    vector<double> x;
    vector<double> x_err;
    for (int i = 0; i < n; ++i) {
        x.push_back(i+0.5);
        x_err.push_back(0.5);
    }
    auto gr = new TGraphErrors(n,x.data(), norm_factor.data(),x_err.data(), norm_factor_err.data());
    gr->Draw("AP");
    gr->GetXaxis()->SetTitle(var_name);
    gr->GetYaxis()->SetTitle("Norm Factor");
    gr->SetLineWidth(2);
    gr->SetMarkerSize(3);
    gr->SetMaximum((*max_element(norm_factor.begin(), norm_factor.end())) * 1.5);

    // Writing Latex and legend
    TPaveText *DStext;
    {
        TString str = "";

        double x_, y_;

        x_ = 0.22;
        y_ = 0.88;
        str = "ATLAS";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.035, 72, 1);
        DStext->Draw("same");

        double delx = 0.115 * 696 * gPad->GetWh() / (472 * gPad->GetWw()) * 1.5 / 2.0;
        str = "Simulation Internal";
        DStext = CreatePaveText(x_ + delx, y_, x_ + delx, y_, str, 0.032, 42, 1);
        DStext->Draw("same");

        y_ -= 0.035;
        str = "13 TeV, 139 fb^{-1}";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.025, 42, 1);
        DStext->Draw("same");

        y_ -= 0.035;
        str = "diHiggs 3-lepton";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.025, 42, 1);
        DStext->Draw("same");

        y_ -= 0.035;
        str = "WZ re-normalization";
        DStext = CreatePaveText(x_, y_, x_, y_, str, 0.025, 42, 1);
        DStext->Draw("same");
    }

    auto line_y1 = new TLine( 0, 1.0, n, 1.0);
    line_y1->SetLineStyle(9);
    line_y1->SetLineColorAlpha(14, 0.5);
    line_y1->Draw("same");

    c->Update();
    c->SaveAs("WZ_NF.pdf");
    c->SaveAs("WZ_NF.png");

    delete c;

}
