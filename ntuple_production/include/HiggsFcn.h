#ifndef MN_HiggsFcn_H_
#define MN_HiggsFcn_H_

#include "Minuit2/FCNBase.h"

#include <iostream>
#include <utility>
#include <vector>
#include <cmath>
#include <cassert>
#include "TLorentzVector.h"
#include "TMath.h"

using namespace ROOT::Minuit2;

class HiggsFcn : public FCNBase {

public:


    HiggsFcn(std::vector<double> meas,
             std::vector<double> pos,
             std::vector<double> mvar) : theMeasurements(std::move(meas)),
                                         thePositions(std::move(pos)),
                                         theMVariances(std::move(mvar)),
                                         theErrorDef(1.) {}

    ~HiggsFcn() override = default;

    [[nodiscard]] virtual double Up() const { return theErrorDef; }

    double operator()(const std::vector<double> &par) const override {
        /*
         *  thePositions: met_met, met_phi, l0_pt, l0_eta, l0_phi, l0_E, l1_pt, l1_eta, l1_phi, l1_E, l2_pt, l2_eta, l2_phi, l2_E,  j1_Pt, j1_Eta, j1_Phi, j1_E, j2_Pt, j2_Eta, j2_Phi, j2_E
         *
         *  theMeasurements: m_total, m_h(lvlv), m_h(lvjj), m_vvv, m_W, m_W*, deltaR_HH, m_ll, m_jj 
         *
         *  par: v0_px, v0_py, v0_pz, v1_px, v1_py, v1_pz, v2_pz
         *
         */

        assert(par.size() == 7);

        TLorentzVector v0;
        v0.SetXYZM(par[0], par[1], par[2], 0.);
        TLorentzVector v1;
        v1.SetXYZM(par[3], par[4], par[5], 0.);
        TLorentzVector v2;
        v2.SetXYZM(thePositions[0] * TMath::Cos(thePositions[1]) - par[0] - par[3],
                   thePositions[0] * TMath::Sin(thePositions[1]) - par[1] - par[4], par[6], 0.);

        TLorentzVector l0;
        l0.SetPtEtaPhiE(thePositions[2], thePositions[3], thePositions[4], thePositions[5]);
        TLorentzVector l1;
        l1.SetPtEtaPhiE(thePositions[6], thePositions[7], thePositions[8], thePositions[9]);
        TLorentzVector l2;
        l2.SetPtEtaPhiE(thePositions[10], thePositions[11], thePositions[12], thePositions[13]);

        TLorentzVector j0;
        j0.SetPtEtaPhiE(thePositions[14], thePositions[15], thePositions[16], thePositions[17]);
        TLorentzVector j1;
        j1.SetPtEtaPhiE(thePositions[18], thePositions[19], thePositions[20], thePositions[21]);

        // Global Constraint
        double M_total = ((l0 + v0) + (l1 + v1) + (l2 + v2) + j0 + j1).M();
        double M_vvv = (v0 + v1 + v2).M();
        // distance between H1 and H2
        if (isnan((l0 + v0 + l1 + v1).Phi()) || isnan((l2 + v2 + j0 + j1).Phi())
            || isnan((l0 + v0 + l2 + v2).Phi()) || isnan((l1 + v1 + j0 + j1).Phi())
            || isnan((l1 + v1 + l2 + v2).Phi()) || isnan((l0 + v0 + j0 + j1).Phi())
                )
            return NAN;
        double deltaR_HH01 = (l0 + v0 + l1 + v1).DeltaR((l2 + v2 + j0 + j1));
        double deltaR_HH02 = (l0 + v0 + l2 + v2).DeltaR((l1 + v1 + j0 + j1));
        double deltaR_HH12 = (l1 + v1 + l2 + v2).DeltaR((l0 + v0 + j0 + j1));

        //std::cout<<"Phi 01: "<<( l0 + v0 + l1 + v1 ).Phi()<<" "<<( l2 + v2 + j0 + j1 ).Phi()<<std::endl;
        //std::cout<<"Phi 02: "<<( l0 + v0 + l2 + v2 ).Phi()<<" "<<( l1 + v1 + j0 + j1 ).Phi()<<std::endl;
        //std::cout<<"Phi 12: "<<( l2 + v2 + l1 + v1 ).Phi()<<" "<<( l0 + v0 + j0 + j1 ).Phi()<<std::endl;

        // H = Leptonic W + Leptonic W
        double M_h01 = (l0 + v0 + l1 + v1).M();
        double M_h02 = (l0 + v0 + l2 + v2).M();
        double M_h12 = (l1 + v1 + l2 + v2).M();
        // H = Leptonic W + Hadronic W
        double M_hj0 = (l0 + v0 + j0 + j1).M();
        double M_hj1 = (l1 + v1 + j0 + j1).M();
        double M_hj2 = (l2 + v2 + j0 + j1).M();
        // Leptonic W
        double M_lv00 = (l0 + v0).M();
        double M_lv11 = (l1 + v1).M();
        double M_lv22 = (l2 + v2).M();
        // Hadronic W
        double M_jj = (j0 + j1).M();

        // Minimization Components
        double M_h_c1 = pow(M_h01 - theMeasurements[1], 2) / pow(theMVariances[1], 2) +
                        pow(M_hj2 - theMeasurements[2], 2) / pow(theMVariances[2], 2);
        double M_h_c2 = pow(M_h02 - theMeasurements[1], 2) / pow(theMVariances[1], 2) +
                        pow(M_hj1 - theMeasurements[2], 2) / pow(theMVariances[2], 2);
        double M_h_c3 = pow(M_h12 - theMeasurements[1], 2) / pow(theMVariances[1], 2) +
                        pow(M_hj0 - theMeasurements[2], 2) / pow(theMVariances[2], 2);

        double M_WW_l1_0 = pow(M_lv00 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv11 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_l2_0 = pow(M_lv00 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv22 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_l3_0 = pow(M_lv11 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv22 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_l1_1 = pow(M_lv11 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv00 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_l2_1 = pow(M_lv22 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv00 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_l3_1 = pow(M_lv22 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv11 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_l1_min = TMath::Min(M_WW_l1_0, M_WW_l1_1);
        double M_WW_l2_min = TMath::Min(M_WW_l2_0, M_WW_l2_1);
        double M_WW_l3_min = TMath::Min(M_WW_l3_0, M_WW_l3_1);

        double M_WW_j1_0 = pow(M_lv00 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_jj - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_j2_0 = pow(M_lv11 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_jj - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_j3_0 = pow(M_lv22 - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_jj - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_j1_1 = pow(M_jj - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv00 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_j2_1 = pow(M_jj - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv11 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_j3_1 = pow(M_jj - theMeasurements[4], 2) / pow(theMVariances[4], 2) +
                           pow(M_lv22 - theMeasurements[5], 2) / pow(theMVariances[5], 2);
        double M_WW_j1_min = TMath::Min(M_WW_j1_0, M_WW_j1_1);
        double M_WW_j2_min = TMath::Min(M_WW_j2_0, M_WW_j2_1);
        double M_WW_j3_min = TMath::Min(M_WW_j3_0, M_WW_j3_1);

        //double R_HH01 = pow( deltaR_HH01 - theMeasurements[6] , 2)/pow(theMVariances[6],2);
        //double R_HH02 = pow( deltaR_HH02 - theMeasurements[6] , 2)/pow(theMVariances[6],2);
        //double R_HH12 = pow( deltaR_HH12 - theMeasurements[6] , 2)/pow(theMVariances[6],2);
        double R_HH01 = 0.;
        double R_HH02 = 0.;
        double R_HH12 = 0.;
        // 3 Combanition
        // (l1+l2) + l3jj
        double M_1 = M_h_c1 + M_WW_l1_min + M_WW_j3_min + R_HH01;
        // (l1+l3) + l2jj
        double M_2 = M_h_c2 + M_WW_l2_min + M_WW_j2_min + R_HH02;
        // (l2+l3) + l1jj
        double M_3 = M_h_c3 + M_WW_l3_min + M_WW_j1_min + R_HH12;

        // Pz peak at 0 for lowest pT neutrino
        double Pz1 = (v0.Pt() < v1.Pt()) ? v0.Pz() : v1.Pz();
        double Pt1 = (v0.Pt() < v1.Pt()) ? v0.Pt() : v1.Pt();
        double Pz2 = (v2.Pt() < Pt1) ? v2.Pz() : Pz1;

        double chi2 = 0.;
        chi2 = pow(M_total - theMeasurements[0], 2) / pow(theMVariances[0], 2)
               + pow(M_vvv - theMeasurements[3], 2) / pow(theMVariances[3], 2)
               + TMath::Min(M_1, TMath::Min(M_2, M_3))
               + pow(Pz2 - 0.0, 2) / pow(8.0, 2);

        return chi2;
    }

    [[nodiscard]] std::vector<double> measurements() const { return theMeasurements; }

    [[nodiscard]] std::vector<double> positions() const { return thePositions; }

    [[nodiscard]] std::vector<double> variances() const { return theMVariances; }

    void setErrorDef(double def) { theErrorDef = def; }

private:

    std::vector<double> theMeasurements;
    std::vector<double> thePositions;
    std::vector<double> theMVariances;
    double theErrorDef;
};


#endif //MN_HiggsFcn_H_
