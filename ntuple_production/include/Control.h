//
// Created by Zhang Yulei on 7/6/21.
//

#ifndef HHML_3L_CONTROL_H
#define HHML_3L_CONTROL_H

#include "nTuple_Analysis.h"

#include "yaml-cpp/yaml.h"

#include "TString.h"

using FF_Dict = std::map<TString, vector<double>>;

class Control {
public:
    Control(const Control &) = delete;

    Control &operator=(Control const &) = delete;

    static Control *CreateInstance();

    // Read data from yaml
    bool ReadYAML(const TString& file_in);

public:
    /*************************************/
    /*  Define all the variables needed  */
    /*************************************/
    map<TString, Sample_Type> samples;
    vector<map<TString, Sample_Type>> groups;

    TString out_dir;

    // WZ-normalization
    vector<double> nf_WZ;

    // pT-parameterized bin
    FF_Dict pT_range_1;
    FF_Dict pT_range_2;
    // Calculated Fake Factor (DD)
    FF_Dict ff_1;
    FF_Dict ff_2;
    // Fake factor uncertainties
    FF_Dict ff_1_err;
    FF_Dict ff_2_err;
    // Calculated Template Fit NFs
    double NF_HF_e{};
    double NF_MatConv_e{};
    double NF_HF_m{};

    // Cuts
    bool ZMassVeto{};
    bool M_l_Cut{};
    bool Evt_Tight{};
    int N_bJets{};
    int N_Jets{};

    vector<int> lep_quality_0;
    vector<int> lep_quality_1;
    vector<int> lep_quality_2;

    // Higgsness
    bool if_Higgsness = false;

private:
    Control();
};

extern Control *dControl;
#endif //HHML_3L_CONTROL_H
