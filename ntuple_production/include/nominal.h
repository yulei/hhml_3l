//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jul 27 14:42:54 2021 by ROOT version 6.20/00
// from TTree nominal/nominal
// found on file: /lustre/collider/zhangyulei/ATLAS/fw1/trilep/v4/mc16a/450662.root
//////////////////////////////////////////////////////////

#ifndef nominal_h
#define nominal_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class nominal {
public :
    TTree *fChain;   //!pointer to the analyzed TTree or TChain
    Int_t fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

    // Declaration of leaf types
    Float_t lep_ID_0;
    Float_t lep_ID_1;
    Float_t lep_ID_2;
    Float_t lep_ID_3;
    Int_t lep_Index_0;
    Int_t lep_Index_1;
    Int_t lep_Index_2;
    Int_t lep_Index_3;
    Float_t lep_sigd0PV_0;
    Float_t lep_sigd0PV_1;
    Float_t lep_sigd0PV_2;
    Float_t lep_sigd0PV_3;
    Float_t lep_Z0SinTheta_0;
    Float_t lep_Z0SinTheta_1;
    Float_t lep_Z0SinTheta_2;
    Float_t lep_Z0SinTheta_3;
    Float_t lep_d0_0;
    Float_t lep_d0_1;
    Float_t lep_d0_2;
    Float_t lep_d0_3;
    Float_t lep_z0_0;
    Float_t lep_z0_1;
    Float_t lep_z0_2;
    Float_t lep_z0_3;
    Float_t lep_vz_0;
    Float_t lep_vz_1;
    Float_t lep_vz_2;
    Float_t lep_vz_3;
    Float_t lep_deltaz0_0;
    Float_t lep_deltaz0_1;
    Float_t lep_deltaz0_2;
    Float_t lep_deltaz0_3;
    Float_t lep_Pt_0;
    Float_t lep_Pt_1;
    Float_t lep_Pt_2;
    Float_t lep_Pt_3;
    Float_t lep_E_0;
    Float_t lep_E_1;
    Float_t lep_E_2;
    Float_t lep_E_3;
    Float_t lep_Eta_0;
    Float_t lep_Eta_1;
    Float_t lep_Eta_2;
    Float_t lep_Eta_3;
    Float_t lep_Phi_0;
    Float_t lep_Phi_1;
    Float_t lep_Phi_2;
    Float_t lep_Phi_3;
    Char_t lep_isTightLH_0;
    Char_t lep_isTightLH_1;
    Char_t lep_isTightLH_2;
    Char_t lep_isTightLH_3;
    Char_t lep_isMediumLH_0;
    Char_t lep_isMediumLH_1;
    Char_t lep_isMediumLH_2;
    Char_t lep_isMediumLH_3;
    Char_t lep_isLooseLH_0;
    Char_t lep_isLooseLH_1;
    Char_t lep_isLooseLH_2;
    Char_t lep_isLooseLH_3;
    Char_t lep_isLoose_0;
    Char_t lep_isLoose_1;
    Char_t lep_isLoose_2;
    Char_t lep_isLoose_3;
    Char_t lep_isTight_0;
    Char_t lep_isTight_1;
    Char_t lep_isTight_2;
    Char_t lep_isTight_3;
    Char_t lep_isMedium_0;
    Char_t lep_isMedium_1;
    Char_t lep_isMedium_2;
    Char_t lep_isMedium_3;
    Int_t lep_isolationLoose_0;
    Int_t lep_isolationLoose_1;
    Int_t lep_isolationLoose_2;
    Int_t lep_isolationLoose_3;
    Int_t lep_isolationGradient_0;
    Int_t lep_isolationGradient_1;
    Int_t lep_isolationGradient_2;
    Int_t lep_isolationGradient_3;
    Int_t lep_isolationGradientLoose_0;
    Int_t lep_isolationGradientLoose_1;
    Int_t lep_isolationGradientLoose_2;
    Int_t lep_isolationGradientLoose_3;
    Int_t lep_isolationTightTrackOnly_0;
    Int_t lep_isolationTightTrackOnly_1;
    Int_t lep_isolationTightTrackOnly_2;
    Int_t lep_isolationTightTrackOnly_3;
    Char_t lep_isolationFCLoose_0;
    Char_t lep_isolationFCLoose_1;
    Char_t lep_isolationFCLoose_2;
    Char_t lep_isolationFCLoose_3;
    Char_t lep_isolationFCTight_0;
    Char_t lep_isolationFCTight_1;
    Char_t lep_isolationFCTight_2;
    Char_t lep_isolationFCTight_3;
    Int_t lep_isolationPflowTight_0;
    Int_t lep_isolationPflowTight_1;
    Int_t lep_isolationPflowTight_2;
    Int_t lep_isolationPflowTight_3;
    Int_t lep_isolationPflowLoose_0;
    Int_t lep_isolationPflowLoose_1;
    Int_t lep_isolationPflowLoose_2;
    Int_t lep_isolationPflowLoose_3;
    Char_t lep_isPrompt_0;
    Char_t lep_isPrompt_1;
    Char_t lep_isPrompt_2;
    Char_t lep_isPrompt_3;
    Int_t lep_truthType_0;
    Int_t lep_truthType_1;
    Int_t lep_truthType_2;
    Int_t lep_truthType_3;
    Int_t lep_truthOrigin_0;
    Int_t lep_truthOrigin_1;
    Int_t lep_truthOrigin_2;
    Int_t lep_truthOrigin_3;
    Char_t lep_chargeIDBDTLoose_0;
    Char_t lep_chargeIDBDTLoose_1;
    Char_t lep_chargeIDBDTLoose_2;
    Char_t lep_chargeIDBDTLoose_3;
    Double_t lep_chargeIDBDTResult_0;
    Double_t lep_chargeIDBDTResult_1;
    Double_t lep_chargeIDBDTResult_2;
    Double_t lep_chargeIDBDTResult_3;
    Float_t tau_pt_0;
    Float_t tau_pt_1;
    Float_t tau_eta_0;
    Float_t tau_eta_1;
    Float_t tau_charge_0;
    Float_t tau_charge_1;
    Float_t tau_width_0;
    Float_t tau_width_1;
    Int_t tau_passJetIDRNNLoose_0;
    Int_t tau_passJetIDRNNLoose_1;
    Int_t tau_passJetIDRNNMed_0;
    Int_t tau_passJetIDRNNMed_1;
    Int_t tau_passEleOLR_0;
    Int_t tau_passEleOLR_1;
    Int_t tau_passEleBDT_0;
    Int_t tau_passEleBDT_1;
    Int_t tau_passEleBDTMed_0;
    Int_t tau_passEleBDTMed_1;
    Int_t tau_passMuonOLR_0;
    Int_t tau_passMuonOLR_1;
    Char_t lep_isFakeLep_0;
    Char_t lep_isFakeLep_1;
    Char_t lep_isFakeLep_2;
    Char_t lep_isFakeLep_3;
    Char_t lep_isTrigMatch_0;
    Char_t lep_isTrigMatch_1;
    Char_t lep_isTrigMatch_2;
    Char_t lep_isTrigMatch_3;
    Char_t lep_isTrigMatchDLT_0;
    Char_t lep_isTrigMatchDLT_1;
    Char_t lep_isTrigMatchDLT_2;
    Char_t lep_isTrigMatchDLT_3;
    UChar_t lep_ambiguityType_0;
    UChar_t lep_ambiguityType_1;
    UChar_t lep_ambiguityType_2;
    UChar_t lep_ambiguityType_3;
    Char_t lep_isQMisID_0;
    Char_t lep_isQMisID_1;
    Char_t lep_isQMisID_2;
    Char_t lep_isQMisID_3;
    Char_t lep_isConvPh_0;
    Char_t lep_isConvPh_1;
    Char_t lep_isConvPh_2;
    Char_t lep_isConvPh_3;
    Char_t lep_isExtConvPh_0;
    Char_t lep_isExtConvPh_1;
    Char_t lep_isExtConvPh_2;
    Char_t lep_isExtConvPh_3;
    Char_t lep_isIntConvPh_0;
    Char_t lep_isIntConvPh_1;
    Char_t lep_isIntConvPh_2;
    Char_t lep_isIntConvPh_3;
    Char_t lep_isBrems_0;
    Char_t lep_isBrems_1;
    Char_t lep_isBrems_2;
    Char_t lep_isBrems_3;
    Float_t lep_Mtrktrk_atPV_CO_0;
    Float_t lep_Mtrktrk_atPV_CO_1;
    Float_t lep_Mtrktrk_atPV_CO_2;
    Float_t lep_Mtrktrk_atPV_CO_3;
    Float_t lep_Mtrktrk_atConvV_CO_0;
    Float_t lep_Mtrktrk_atConvV_CO_1;
    Float_t lep_Mtrktrk_atConvV_CO_2;
    Float_t lep_Mtrktrk_atConvV_CO_3;
    Float_t lep_RadiusCO_0;
    Float_t lep_RadiusCO_1;
    Float_t lep_RadiusCO_2;
    Float_t lep_RadiusCO_3;
    Float_t lep_RadiusCOX_0;
    Float_t lep_RadiusCOX_1;
    Float_t lep_RadiusCOX_2;
    Float_t lep_RadiusCOX_3;
    Float_t lep_RadiusCOY_0;
    Float_t lep_RadiusCOY_1;
    Float_t lep_RadiusCOY_2;
    Float_t lep_RadiusCOY_3;
    Int_t lep_truthParentPdgId_0;
    Int_t lep_truthParentPdgId_1;
    Int_t lep_truthParentPdgId_2;
    Int_t lep_truthParentPdgId_3;
    Int_t lep_truthParentOrigin_0;
    Int_t lep_truthParentOrigin_1;
    Int_t lep_truthParentOrigin_2;
    Int_t lep_truthParentOrigin_3;
    Char_t lep_isISR_FSR_Ph_0;
    Char_t lep_isISR_FSR_Ph_1;
    Char_t lep_isISR_FSR_Ph_2;
    Char_t lep_isISR_FSR_Ph_3;
    Int_t lep_nTrackParticles_0;
    Int_t lep_nTrackParticles_1;
    Int_t lep_nTrackParticles_2;
    Int_t lep_nTrackParticles_3;
    Char_t lep_isTruthMatched_0;
    Char_t lep_isTruthMatched_1;
    Char_t lep_isTruthMatched_2;
    Char_t lep_isTruthMatched_3;
    Int_t lep_truthParentType_0;
    Int_t lep_truthParentType_1;
    Int_t lep_truthParentType_2;
    Int_t lep_truthParentType_3;
    Int_t lep_truthPdgId_0;
    Int_t lep_truthPdgId_1;
    Int_t lep_truthPdgId_2;
    Int_t lep_truthPdgId_3;
    Int_t lep_truthStatus_0;
    Int_t lep_truthStatus_1;
    Int_t lep_truthStatus_2;
    Int_t lep_truthStatus_3;
    Int_t lep_truthParentStatus_0;
    Int_t lep_truthParentStatus_1;
    Int_t lep_truthParentStatus_2;
    Int_t lep_truthParentStatus_3;
    Int_t lep_plvWP_Loose_0;
    Int_t lep_plvWP_Loose_1;
    Int_t lep_plvWP_Loose_2;
    Int_t lep_plvWP_Loose_3;
    Int_t lep_plvWP_Tight_0;
    Int_t lep_plvWP_Tight_1;
    Int_t lep_plvWP_Tight_2;
    Int_t lep_plvWP_Tight_3;
    Float_t lep_truthPt_0;
    Float_t lep_truthPt_1;
    Float_t lep_truthPt_2;
    Float_t lep_truthPt_3;
    Float_t lep_truthEta_0;
    Float_t lep_truthEta_1;
    Float_t lep_truthEta_2;
    Float_t lep_truthEta_3;
    Float_t lep_truthPhi_0;
    Float_t lep_truthPhi_1;
    Float_t lep_truthPhi_2;
    Float_t lep_truthPhi_3;
    Float_t lep_truthM_0;
    Float_t lep_truthM_1;
    Float_t lep_truthM_2;
    Float_t lep_truthM_3;
    Float_t lep_truthE_0;
    Float_t lep_truthE_1;
    Float_t lep_truthE_2;
    Float_t lep_truthE_3;
    Float_t lep_truthRapidity_0;
    Float_t lep_truthRapidity_1;
    Float_t lep_truthRapidity_2;
    Float_t lep_truthRapidity_3;
    Int_t lep_nInnerPix_0;
    Int_t lep_nInnerPix_1;
    Int_t lep_nInnerPix_2;
    Int_t lep_nInnerPix_3;
    Int_t lep_firstEgMotherPdgId_0;
    Int_t lep_firstEgMotherPdgId_1;
    Int_t lep_firstEgMotherPdgId_2;
    Int_t lep_firstEgMotherPdgId_3;
    Int_t lep_firstEgMotherTruthType_0;
    Int_t lep_firstEgMotherTruthType_1;
    Int_t lep_firstEgMotherTruthType_2;
    Int_t lep_firstEgMotherTruthType_3;
    Int_t lep_firstEgMotherTruthOrigin_0;
    Int_t lep_firstEgMotherTruthOrigin_1;
    Int_t lep_firstEgMotherTruthOrigin_2;
    Int_t lep_firstEgMotherTruthOrigin_3;
    Float_t lep_promptLeptonIso_TagWeight_0;
    Float_t lep_promptLeptonIso_TagWeight_1;
    Float_t lep_promptLeptonIso_TagWeight_2;
    Float_t lep_promptLeptonIso_TagWeight_3;
    Float_t lep_promptLeptonVeto_TagWeight_0;
    Float_t lep_promptLeptonVeto_TagWeight_1;
    Float_t lep_promptLeptonVeto_TagWeight_2;
    Float_t lep_promptLeptonVeto_TagWeight_3;
    Float_t lep_promptLeptonImprovedVetoBARR_TagWeight_0;
    Float_t lep_promptLeptonImprovedVetoBARR_TagWeight_1;
    Float_t lep_promptLeptonImprovedVetoBARR_TagWeight_2;
    Float_t lep_promptLeptonImprovedVetoBARR_TagWeight_3;
    Float_t lep_promptLeptonImprovedVetoECAP_TagWeight_0;
    Float_t lep_promptLeptonImprovedVetoECAP_TagWeight_1;
    Float_t lep_promptLeptonImprovedVetoECAP_TagWeight_2;
    Float_t lep_promptLeptonImprovedVetoECAP_TagWeight_3;
    Float_t lep_SF_El_Reco_AT_0;
    Float_t lep_SF_El_Reco_AT_1;
    Float_t lep_SF_El_Reco_AT_2;
    Float_t lep_SF_El_Reco_AT_3;
    Float_t lep_SF_El_ID_LooseAndBLayerLH_AT_0;
    Float_t lep_SF_El_ID_LooseAndBLayerLH_AT_1;
    Float_t lep_SF_El_ID_LooseAndBLayerLH_AT_2;
    Float_t lep_SF_El_ID_LooseAndBLayerLH_AT_3;
    Float_t lep_SF_El_ID_TightLH_AT_0;
    Float_t lep_SF_El_ID_TightLH_AT_1;
    Float_t lep_SF_El_ID_TightLH_AT_2;
    Float_t lep_SF_El_ID_TightLH_AT_3;
    Float_t lep_SF_El_Iso_FCLoose_AT_0;
    Float_t lep_SF_El_Iso_FCLoose_AT_1;
    Float_t lep_SF_El_Iso_FCLoose_AT_2;
    Float_t lep_SF_El_Iso_FCLoose_AT_3;
    Float_t lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0;
    Float_t lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1;
    Float_t lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2;
    Float_t lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_3;
    Float_t lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_0;
    Float_t lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_1;
    Float_t lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_2;
    Float_t lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_3;
    Float_t lep_SF_El_FCLooseTightLHQMisID_0;
    Float_t lep_SF_El_FCLooseTightLHQMisID_1;
    Float_t lep_SF_El_FCLooseTightLHQMisID_2;
    Float_t lep_SF_El_FCLooseTightLHQMisID_3;
    Float_t lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_0;
    Float_t lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1;
    Float_t lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_2;
    Float_t lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_3;
    Float_t lep_SF_El_Iso_FCLoose_TightLH_0;
    Float_t lep_SF_El_Iso_FCLoose_TightLH_1;
    Float_t lep_SF_El_Iso_FCLoose_TightLH_2;
    Float_t lep_SF_El_Iso_FCLoose_TightLH_3;
    Float_t lep_SF_El_PLVTight_QmisID_0;
    Float_t lep_SF_El_PLVTight_QmisID_1;
    Float_t lep_SF_El_PLVTight_QmisID_2;
    Float_t lep_SF_El_PLVTight_QmisID_3;
    Float_t lep_SF_El_PLVTight_0;
    Float_t lep_SF_El_PLVTight_1;
    Float_t lep_SF_El_PLVTight_2;
    Float_t lep_SF_El_PLVTight_3;
    Float_t lep_SF_El_PLVLoose_0;
    Float_t lep_SF_El_PLVLoose_1;
    Float_t lep_SF_El_PLVLoose_2;
    Float_t lep_SF_El_PLVLoose_3;
    Float_t lep_SF_El_ID_MediumLH_0;
    Float_t lep_SF_El_ID_MediumLH_1;
    Float_t lep_SF_El_ID_MediumLH_2;
    Float_t lep_SF_El_ID_MediumLH_3;
    Float_t lep_SF_Mu_TTVA_AT_0;
    Float_t lep_SF_Mu_TTVA_AT_1;
    Float_t lep_SF_Mu_TTVA_AT_2;
    Float_t lep_SF_Mu_TTVA_AT_3;
    Float_t lep_SF_Mu_ID_Loose_AT_0;
    Float_t lep_SF_Mu_ID_Loose_AT_1;
    Float_t lep_SF_Mu_ID_Loose_AT_2;
    Float_t lep_SF_Mu_ID_Loose_AT_3;
    Float_t lep_SF_Mu_ID_Medium_AT_0;
    Float_t lep_SF_Mu_ID_Medium_AT_1;
    Float_t lep_SF_Mu_ID_Medium_AT_2;
    Float_t lep_SF_Mu_ID_Medium_AT_3;
    Float_t lep_SF_Mu_Iso_FCLoose_AT_0;
    Float_t lep_SF_Mu_Iso_FCLoose_AT_1;
    Float_t lep_SF_Mu_Iso_FCLoose_AT_2;
    Float_t lep_SF_Mu_Iso_FCLoose_AT_3;
    Float_t lep_SF_Mu_ID_Tight_0;
    Float_t lep_SF_Mu_ID_Tight_1;
    Float_t lep_SF_Mu_ID_Tight_2;
    Float_t lep_SF_Mu_ID_Tight_3;
    Float_t lep_SF_Mu_PLVTight_0;
    Float_t lep_SF_Mu_PLVTight_1;
    Float_t lep_SF_Mu_PLVTight_2;
    Float_t lep_SF_Mu_PLVTight_3;
    Float_t lep_SF_Mu_PLVLoose_0;
    Float_t lep_SF_Mu_PLVLoose_1;
    Float_t lep_SF_Mu_PLVLoose_2;
    Float_t lep_SF_Mu_PLVLoose_3;
    Float_t lep_SF_CombinedLoose_0;
    Float_t lep_SF_CombinedLoose_1;
    Float_t lep_SF_CombinedLoose_2;
    Float_t lep_SF_CombinedLoose_3;
    Float_t lep_SF_CombinedTight_0;
    Float_t lep_SF_CombinedTight_1;
    Float_t lep_SF_CombinedTight_2;
    Float_t lep_SF_CombinedTight_3;
    Float_t tau_E_0;
    Float_t tau_E_1;
    Int_t tau_numTrack_0;
    Int_t tau_numTrack_1;
    Float_t totalEventsWeighted;
    Float_t weight_mc;
    Float_t weight_pileup;
    std::vector<float> *mc_genWeights;
    Float_t bTagSF_weight_DL1r_70;
    Float_t bTagSF_weight_DL1r_77;
    Float_t bTagSF_weight_DL1r_85;
    Float_t jvtSF_customOR;
    Float_t lepSFObjLoose;
    Float_t lepSFObjTight;
    Float_t lepSFObjChannelCombined;
    Float_t custTrigSF_LooseID_FCLooseIso_SLTorDLT;
    Float_t custTrigSF_LooseID_FCLooseIso_SLT;
    Float_t custTrigSF_LooseID_FCLooseIso_DLT;
    Int_t custTrigMatch_LooseID_FCLooseIso_SLTorDLT;
    Int_t custTrigMatch_LooseID_FCLooseIso_SLT;
    Int_t custTrigMatch_LooseID_FCLooseIso_DLT;
    Float_t custTrigSF_TightElMediumMuID_FCLooseIso_SLTorDLT;
    Float_t custTrigSF_TightElMediumMuID_FCLooseIso_SLT;
    Float_t custTrigSF_TightElMediumMuID_FCLooseIso_DLT;
    Int_t custTrigMatch_TightElMediumMuID_FCLooseIso_SLTorDLT;
    Int_t custTrigMatch_TightElMediumMuID_FCLooseIso_SLT;
    Int_t custTrigMatch_TightElMediumMuID_FCLooseIso_DLT;
    ULong64_t eventNumber;
    UInt_t runNumber;
    UInt_t randomRunNumber;
    UInt_t mcChannelNumber;
    Float_t mu;
    Float_t met_met;
    Float_t met_phi;
    Int_t GlobalTrigDecision;
    Char_t GlobalTrigDecision_mu26_ivarmedium;
    Char_t GlobalTrigDecision_mu22_mu8noL1;
    Char_t GlobalTrigDecision_e60_lhmedium_nod0;
    Char_t GlobalTrigDecision_mu18_mu8noL1;
    Char_t GlobalTrigDecision_e26_lhtight_nod0_ivarloose;
    Char_t GlobalTrigDecision_mu20_iloose_L1MU15;
    Char_t GlobalTrigDecision_e24_lhmedium_L1EM20VH;
    Char_t GlobalTrigDecision_mu50;
    Char_t GlobalTrigDecision_e17_lhloose_nod0_mu14;
    Char_t GlobalTrigDecision_2e12_lhloose_L12EM10VH;
    Char_t GlobalTrigDecision_e120_lhloose;
    Char_t GlobalTrigDecision_e140_lhloose_nod0;
    Char_t GlobalTrigDecision_e60_lhmedium;
    Char_t GlobalTrigDecision_2e17_lhvloose_nod0;
    Char_t GlobalTrigDecision_e17_lhloose_mu14;
    Float_t custTrigSF_AT_default_preOR;
    Int_t RunYear;
    Double_t mc_xSection;
    Double_t mc_rawXSection;
    Double_t mc_kFactor;
    Int_t onelep_type;
    Int_t dilep_type;
    Int_t trilep_type;
    Int_t quadlep_type;
    Int_t total_charge;
    Int_t total_leptons;
    Char_t isQMisIDEvent;
    Char_t isFakeEvent;
    Char_t isLepFromPhEvent;
    Float_t Mll01;
    Float_t Mll02;
    Float_t Mll12;
    Float_t Mlll012;
    Float_t Ptll01;
    Float_t Ptll02;
    Float_t Ptll12;
    Float_t DRll01;
    Float_t DRll02;
    Float_t DRll12;
    Float_t DRl0jmin;
    Float_t DRl1jmin;
    Float_t DRl0j0;
    Float_t DRl0j1;
    Float_t DRl1j0;
    Float_t DRl1j1;
    Float_t DRl2j0;
    Float_t DRl2j1;
    Float_t DRj0j1;
    Float_t DRjjMax;
    Float_t mj0j1;
    Float_t ml0j0;
    Float_t ml0j1;
    Float_t ml1j0;
    Float_t ml1j1;
    Float_t ml2j0;
    Float_t ml2j1;
    Char_t matchDLTll01;
    Float_t best_Z_Mll;
    Float_t best_Z_other_MtLepMet;
    Float_t best_Z_other_Mll;
    Int_t nJets_OR;
    Int_t nTruthJets;
    Int_t nTruthJets_OR;
    Int_t nJets_OR_DL1r_85;
    Int_t nJets_OR_DL1r_77;
    Int_t nJets_OR_DL1r_70;
    Int_t nTaus_OR_Pt25;
    Int_t nTaus_OR_Pt25_RNN;
    Float_t HT;
    Float_t HT_lep;
    Float_t HT_jets;
    Float_t lead_jetPt;
    Float_t lead_jetEta;
    Float_t lead_jetPhi;
    Float_t lead_jetE;
    Float_t sublead_jetPt;
    Float_t sublead_jetEta;
    Float_t sublead_jetPhi;
    Float_t sublead_jetE;
    Int_t higgs1Mode;
    Int_t higgs2Mode;
    std::vector<float> *jet_pt;
    std::vector<float> *jet_eta;
    std::vector<float> *jet_phi;
    std::vector<float> *jet_e;

    // List of branches
    TBranch *b_lep_ID_0;   //!
    TBranch *b_lep_ID_1;   //!
    TBranch *b_lep_ID_2;   //!
    TBranch *b_lep_ID_3;   //!
    TBranch *b_lep_Index_0;   //!
    TBranch *b_lep_Index_1;   //!
    TBranch *b_lep_Index_2;   //!
    TBranch *b_lep_Index_3;   //!
    TBranch *b_lep_sigd0PV_0;   //!
    TBranch *b_lep_sigd0PV_1;   //!
    TBranch *b_lep_sigd0PV_2;   //!
    TBranch *b_lep_sigd0PV_3;   //!
    TBranch *b_lep_Z0SinTheta_0;   //!
    TBranch *b_lep_Z0SinTheta_1;   //!
    TBranch *b_lep_Z0SinTheta_2;   //!
    TBranch *b_lep_Z0SinTheta_3;   //!
    TBranch *b_lep_d0_0;   //!
    TBranch *b_lep_d0_1;   //!
    TBranch *b_lep_d0_2;   //!
    TBranch *b_lep_d0_3;   //!
    TBranch *b_lep_z0_0;   //!
    TBranch *b_lep_z0_1;   //!
    TBranch *b_lep_z0_2;   //!
    TBranch *b_lep_z0_3;   //!
    TBranch *b_lep_vz_0;   //!
    TBranch *b_lep_vz_1;   //!
    TBranch *b_lep_vz_2;   //!
    TBranch *b_lep_vz_3;   //!
    TBranch *b_lep_deltaz0_0;   //!
    TBranch *b_lep_deltaz0_1;   //!
    TBranch *b_lep_deltaz0_2;   //!
    TBranch *b_lep_deltaz0_3;   //!
    TBranch *b_lep_Pt_0;   //!
    TBranch *b_lep_Pt_1;   //!
    TBranch *b_lep_Pt_2;   //!
    TBranch *b_lep_Pt_3;   //!
    TBranch *b_lep_E_0;   //!
    TBranch *b_lep_E_1;   //!
    TBranch *b_lep_E_2;   //!
    TBranch *b_lep_E_3;   //!
    TBranch *b_lep_Eta_0;   //!
    TBranch *b_lep_Eta_1;   //!
    TBranch *b_lep_Eta_2;   //!
    TBranch *b_lep_Eta_3;   //!
    TBranch *b_lep_Phi_0;   //!
    TBranch *b_lep_Phi_1;   //!
    TBranch *b_lep_Phi_2;   //!
    TBranch *b_lep_Phi_3;   //!
    TBranch *b_lep_isTightLH_0;   //!
    TBranch *b_lep_isTightLH_1;   //!
    TBranch *b_lep_isTightLH_2;   //!
    TBranch *b_lep_isTightLH_3;   //!
    TBranch *b_lep_isMediumLH_0;   //!
    TBranch *b_lep_isMediumLH_1;   //!
    TBranch *b_lep_isMediumLH_2;   //!
    TBranch *b_lep_isMediumLH_3;   //!
    TBranch *b_lep_isLooseLH_0;   //!
    TBranch *b_lep_isLooseLH_1;   //!
    TBranch *b_lep_isLooseLH_2;   //!
    TBranch *b_lep_isLooseLH_3;   //!
    TBranch *b_lep_isLoose_0;   //!
    TBranch *b_lep_isLoose_1;   //!
    TBranch *b_lep_isLoose_2;   //!
    TBranch *b_lep_isLoose_3;   //!
    TBranch *b_lep_isTight_0;   //!
    TBranch *b_lep_isTight_1;   //!
    TBranch *b_lep_isTight_2;   //!
    TBranch *b_lep_isTight_3;   //!
    TBranch *b_lep_isMedium_0;   //!
    TBranch *b_lep_isMedium_1;   //!
    TBranch *b_lep_isMedium_2;   //!
    TBranch *b_lep_isMedium_3;   //!
    TBranch *b_lep_isolationLoose_0;   //!
    TBranch *b_lep_isolationLoose_1;   //!
    TBranch *b_lep_isolationLoose_2;   //!
    TBranch *b_lep_isolationLoose_3;   //!
    TBranch *b_lep_isolationGradient_0;   //!
    TBranch *b_lep_isolationGradient_1;   //!
    TBranch *b_lep_isolationGradient_2;   //!
    TBranch *b_lep_isolationGradient_3;   //!
    TBranch *b_lep_isolationGradientLoose_0;   //!
    TBranch *b_lep_isolationGradientLoose_1;   //!
    TBranch *b_lep_isolationGradientLoose_2;   //!
    TBranch *b_lep_isolationGradientLoose_3;   //!
    TBranch *b_lep_isolationTightTrackOnly_0;   //!
    TBranch *b_lep_isolationTightTrackOnly_1;   //!
    TBranch *b_lep_isolationTightTrackOnly_2;   //!
    TBranch *b_lep_isolationTightTrackOnly_3;   //!
    TBranch *b_lep_isolationFCLoose_0;   //!
    TBranch *b_lep_isolationFCLoose_1;   //!
    TBranch *b_lep_isolationFCLoose_2;   //!
    TBranch *b_lep_isolationFCLoose_3;   //!
    TBranch *b_lep_isolationFCTight_0;   //!
    TBranch *b_lep_isolationFCTight_1;   //!
    TBranch *b_lep_isolationFCTight_2;   //!
    TBranch *b_lep_isolationFCTight_3;   //!
    TBranch *b_lep_isolationPflowTight_0;   //!
    TBranch *b_lep_isolationPflowTight_1;   //!
    TBranch *b_lep_isolationPflowTight_2;   //!
    TBranch *b_lep_isolationPflowTight_3;   //!
    TBranch *b_lep_isolationPflowLoose_0;   //!
    TBranch *b_lep_isolationPflowLoose_1;   //!
    TBranch *b_lep_isolationPflowLoose_2;   //!
    TBranch *b_lep_isolationPflowLoose_3;   //!
    TBranch *b_lep_isPrompt_0;   //!
    TBranch *b_lep_isPrompt_1;   //!
    TBranch *b_lep_isPrompt_2;   //!
    TBranch *b_lep_isPrompt_3;   //!
    TBranch *b_lep_truthType_0;   //!
    TBranch *b_lep_truthType_1;   //!
    TBranch *b_lep_truthType_2;   //!
    TBranch *b_lep_truthType_3;   //!
    TBranch *b_lep_truthOrigin_0;   //!
    TBranch *b_lep_truthOrigin_1;   //!
    TBranch *b_lep_truthOrigin_2;   //!
    TBranch *b_lep_truthOrigin_3;   //!
    TBranch *b_lep_chargeIDBDTLoose_0;   //!
    TBranch *b_lep_chargeIDBDTLoose_1;   //!
    TBranch *b_lep_chargeIDBDTLoose_2;   //!
    TBranch *b_lep_chargeIDBDTLoose_3;   //!
    TBranch *b_lep_chargeIDBDTResult_0;   //!
    TBranch *b_lep_chargeIDBDTResult_1;   //!
    TBranch *b_lep_chargeIDBDTResult_2;   //!
    TBranch *b_lep_chargeIDBDTResult_3;   //!
    TBranch *b_tau_pt_0;   //!
    TBranch *b_tau_pt_1;   //!
    TBranch *b_tau_eta_0;   //!
    TBranch *b_tau_eta_1;   //!
    TBranch *b_tau_charge_0;   //!
    TBranch *b_tau_charge_1;   //!
    TBranch *b_tau_width_0;   //!
    TBranch *b_tau_width_1;   //!
    TBranch *b_tau_passJetIDRNNLoose_0;   //!
    TBranch *b_tau_passJetIDRNNLoose_1;   //!
    TBranch *b_tau_passJetIDRNNMed_0;   //!
    TBranch *b_tau_passJetIDRNNMed_1;   //!
    TBranch *b_tau_passEleOLR_0;   //!
    TBranch *b_tau_passEleOLR_1;   //!
    TBranch *b_tau_passEleBDT_0;   //!
    TBranch *b_tau_passEleBDT_1;   //!
    TBranch *b_tau_passEleBDTMed_0;   //!
    TBranch *b_tau_passEleBDTMed_1;   //!
    TBranch *b_tau_passMuonOLR_0;   //!
    TBranch *b_tau_passMuonOLR_1;   //!
    TBranch *b_lep_isFakeLep_0;   //!
    TBranch *b_lep_isFakeLep_1;   //!
    TBranch *b_lep_isFakeLep_2;   //!
    TBranch *b_lep_isFakeLep_3;   //!
    TBranch *b_lep_isTrigMatch_0;   //!
    TBranch *b_lep_isTrigMatch_1;   //!
    TBranch *b_lep_isTrigMatch_2;   //!
    TBranch *b_lep_isTrigMatch_3;   //!
    TBranch *b_lep_isTrigMatchDLT_0;   //!
    TBranch *b_lep_isTrigMatchDLT_1;   //!
    TBranch *b_lep_isTrigMatchDLT_2;   //!
    TBranch *b_lep_isTrigMatchDLT_3;   //!
    TBranch *b_lep_ambiguityType_0;   //!
    TBranch *b_lep_ambiguityType_1;   //!
    TBranch *b_lep_ambiguityType_2;   //!
    TBranch *b_lep_ambiguityType_3;   //!
    TBranch *b_lep_isQMisID_0;   //!
    TBranch *b_lep_isQMisID_1;   //!
    TBranch *b_lep_isQMisID_2;   //!
    TBranch *b_lep_isQMisID_3;   //!
    TBranch *b_lep_isConvPh_0;   //!
    TBranch *b_lep_isConvPh_1;   //!
    TBranch *b_lep_isConvPh_2;   //!
    TBranch *b_lep_isConvPh_3;   //!
    TBranch *b_lep_isExtConvPh_0;   //!
    TBranch *b_lep_isExtConvPh_1;   //!
    TBranch *b_lep_isExtConvPh_2;   //!
    TBranch *b_lep_isExtConvPh_3;   //!
    TBranch *b_lep_isIntConvPh_0;   //!
    TBranch *b_lep_isIntConvPh_1;   //!
    TBranch *b_lep_isIntConvPh_2;   //!
    TBranch *b_lep_isIntConvPh_3;   //!
    TBranch *b_lep_isBrems_0;   //!
    TBranch *b_lep_isBrems_1;   //!
    TBranch *b_lep_isBrems_2;   //!
    TBranch *b_lep_isBrems_3;   //!
    TBranch *b_lep_Mtrktrk_atPV_CO_0;   //!
    TBranch *b_lep_Mtrktrk_atPV_CO_1;   //!
    TBranch *b_lep_Mtrktrk_atPV_CO_2;   //!
    TBranch *b_lep_Mtrktrk_atPV_CO_3;   //!
    TBranch *b_lep_Mtrktrk_atConvV_CO_0;   //!
    TBranch *b_lep_Mtrktrk_atConvV_CO_1;   //!
    TBranch *b_lep_Mtrktrk_atConvV_CO_2;   //!
    TBranch *b_lep_Mtrktrk_atConvV_CO_3;   //!
    TBranch *b_lep_RadiusCO_0;   //!
    TBranch *b_lep_RadiusCO_1;   //!
    TBranch *b_lep_RadiusCO_2;   //!
    TBranch *b_lep_RadiusCO_3;   //!
    TBranch *b_lep_RadiusCOX_0;   //!
    TBranch *b_lep_RadiusCOX_1;   //!
    TBranch *b_lep_RadiusCOX_2;   //!
    TBranch *b_lep_RadiusCOX_3;   //!
    TBranch *b_lep_RadiusCOY_0;   //!
    TBranch *b_lep_RadiusCOY_1;   //!
    TBranch *b_lep_RadiusCOY_2;   //!
    TBranch *b_lep_RadiusCOY_3;   //!
    TBranch *b_lep_truthParentPdgId_0;   //!
    TBranch *b_lep_truthParentPdgId_1;   //!
    TBranch *b_lep_truthParentPdgId_2;   //!
    TBranch *b_lep_truthParentPdgId_3;   //!
    TBranch *b_lep_truthParentOrigin_0;   //!
    TBranch *b_lep_truthParentOrigin_1;   //!
    TBranch *b_lep_truthParentOrigin_2;   //!
    TBranch *b_lep_truthParentOrigin_3;   //!
    TBranch *b_lep_isISR_FSR_Ph_0;   //!
    TBranch *b_lep_isISR_FSR_Ph_1;   //!
    TBranch *b_lep_isISR_FSR_Ph_2;   //!
    TBranch *b_lep_isISR_FSR_Ph_3;   //!
    TBranch *b_lep_nTrackParticles_0;   //!
    TBranch *b_lep_nTrackParticles_1;   //!
    TBranch *b_lep_nTrackParticles_2;   //!
    TBranch *b_lep_nTrackParticles_3;   //!
    TBranch *b_lep_isTruthMatched_0;   //!
    TBranch *b_lep_isTruthMatched_1;   //!
    TBranch *b_lep_isTruthMatched_2;   //!
    TBranch *b_lep_isTruthMatched_3;   //!
    TBranch *b_lep_truthParentType_0;   //!
    TBranch *b_lep_truthParentType_1;   //!
    TBranch *b_lep_truthParentType_2;   //!
    TBranch *b_lep_truthParentType_3;   //!
    TBranch *b_lep_truthPdgId_0;   //!
    TBranch *b_lep_truthPdgId_1;   //!
    TBranch *b_lep_truthPdgId_2;   //!
    TBranch *b_lep_truthPdgId_3;   //!
    TBranch *b_lep_truthStatus_0;   //!
    TBranch *b_lep_truthStatus_1;   //!
    TBranch *b_lep_truthStatus_2;   //!
    TBranch *b_lep_truthStatus_3;   //!
    TBranch *b_lep_truthParentStatus_0;   //!
    TBranch *b_lep_truthParentStatus_1;   //!
    TBranch *b_lep_truthParentStatus_2;   //!
    TBranch *b_lep_truthParentStatus_3;   //!
    TBranch *b_lep_plvWP_Loose_0;   //!
    TBranch *b_lep_plvWP_Loose_1;   //!
    TBranch *b_lep_plvWP_Loose_2;   //!
    TBranch *b_lep_plvWP_Loose_3;   //!
    TBranch *b_lep_plvWP_Tight_0;   //!
    TBranch *b_lep_plvWP_Tight_1;   //!
    TBranch *b_lep_plvWP_Tight_2;   //!
    TBranch *b_lep_plvWP_Tight_3;   //!
    TBranch *b_lep_truthPt_0;   //!
    TBranch *b_lep_truthPt_1;   //!
    TBranch *b_lep_truthPt_2;   //!
    TBranch *b_lep_truthPt_3;   //!
    TBranch *b_lep_truthEta_0;   //!
    TBranch *b_lep_truthEta_1;   //!
    TBranch *b_lep_truthEta_2;   //!
    TBranch *b_lep_truthEta_3;   //!
    TBranch *b_lep_truthPhi_0;   //!
    TBranch *b_lep_truthPhi_1;   //!
    TBranch *b_lep_truthPhi_2;   //!
    TBranch *b_lep_truthPhi_3;   //!
    TBranch *b_lep_truthM_0;   //!
    TBranch *b_lep_truthM_1;   //!
    TBranch *b_lep_truthM_2;   //!
    TBranch *b_lep_truthM_3;   //!
    TBranch *b_lep_truthE_0;   //!
    TBranch *b_lep_truthE_1;   //!
    TBranch *b_lep_truthE_2;   //!
    TBranch *b_lep_truthE_3;   //!
    TBranch *b_lep_truthRapidity_0;   //!
    TBranch *b_lep_truthRapidity_1;   //!
    TBranch *b_lep_truthRapidity_2;   //!
    TBranch *b_lep_truthRapidity_3;   //!
    TBranch *b_lep_nInnerPix_0;   //!
    TBranch *b_lep_nInnerPix_1;   //!
    TBranch *b_lep_nInnerPix_2;   //!
    TBranch *b_lep_nInnerPix_3;   //!
    TBranch *b_lep_firstEgMotherPdgId_0;   //!
    TBranch *b_lep_firstEgMotherPdgId_1;   //!
    TBranch *b_lep_firstEgMotherPdgId_2;   //!
    TBranch *b_lep_firstEgMotherPdgId_3;   //!
    TBranch *b_lep_firstEgMotherTruthType_0;   //!
    TBranch *b_lep_firstEgMotherTruthType_1;   //!
    TBranch *b_lep_firstEgMotherTruthType_2;   //!
    TBranch *b_lep_firstEgMotherTruthType_3;   //!
    TBranch *b_lep_firstEgMotherTruthOrigin_0;   //!
    TBranch *b_lep_firstEgMotherTruthOrigin_1;   //!
    TBranch *b_lep_firstEgMotherTruthOrigin_2;   //!
    TBranch *b_lep_firstEgMotherTruthOrigin_3;   //!
    TBranch *b_lep_promptLeptonIso_TagWeight_0;   //!
    TBranch *b_lep_promptLeptonIso_TagWeight_1;   //!
    TBranch *b_lep_promptLeptonIso_TagWeight_2;   //!
    TBranch *b_lep_promptLeptonIso_TagWeight_3;   //!
    TBranch *b_lep_promptLeptonVeto_TagWeight_0;   //!
    TBranch *b_lep_promptLeptonVeto_TagWeight_1;   //!
    TBranch *b_lep_promptLeptonVeto_TagWeight_2;   //!
    TBranch *b_lep_promptLeptonVeto_TagWeight_3;   //!
    TBranch *b_lep_promptLeptonImprovedVetoBARR_TagWeight_0;   //!
    TBranch *b_lep_promptLeptonImprovedVetoBARR_TagWeight_1;   //!
    TBranch *b_lep_promptLeptonImprovedVetoBARR_TagWeight_2;   //!
    TBranch *b_lep_promptLeptonImprovedVetoBARR_TagWeight_3;   //!
    TBranch *b_lep_promptLeptonImprovedVetoECAP_TagWeight_0;   //!
    TBranch *b_lep_promptLeptonImprovedVetoECAP_TagWeight_1;   //!
    TBranch *b_lep_promptLeptonImprovedVetoECAP_TagWeight_2;   //!
    TBranch *b_lep_promptLeptonImprovedVetoECAP_TagWeight_3;   //!
    TBranch *b_lep_SF_El_Reco_AT_0;   //!
    TBranch *b_lep_SF_El_Reco_AT_1;   //!
    TBranch *b_lep_SF_El_Reco_AT_2;   //!
    TBranch *b_lep_SF_El_Reco_AT_3;   //!
    TBranch *b_lep_SF_El_ID_LooseAndBLayerLH_AT_0;   //!
    TBranch *b_lep_SF_El_ID_LooseAndBLayerLH_AT_1;   //!
    TBranch *b_lep_SF_El_ID_LooseAndBLayerLH_AT_2;   //!
    TBranch *b_lep_SF_El_ID_LooseAndBLayerLH_AT_3;   //!
    TBranch *b_lep_SF_El_ID_TightLH_AT_0;   //!
    TBranch *b_lep_SF_El_ID_TightLH_AT_1;   //!
    TBranch *b_lep_SF_El_ID_TightLH_AT_2;   //!
    TBranch *b_lep_SF_El_ID_TightLH_AT_3;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_AT_0;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_AT_1;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_AT_2;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_AT_3;   //!
    TBranch *b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0;   //!
    TBranch *b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1;   //!
    TBranch *b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2;   //!
    TBranch *b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_3;   //!
    TBranch *b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_0;   //!
    TBranch *b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_1;   //!
    TBranch *b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_2;   //!
    TBranch *b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_3;   //!
    TBranch *b_lep_SF_El_FCLooseTightLHQMisID_0;   //!
    TBranch *b_lep_SF_El_FCLooseTightLHQMisID_1;   //!
    TBranch *b_lep_SF_El_FCLooseTightLHQMisID_2;   //!
    TBranch *b_lep_SF_El_FCLooseTightLHQMisID_3;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_0;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_2;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_3;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_TightLH_0;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_TightLH_1;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_TightLH_2;   //!
    TBranch *b_lep_SF_El_Iso_FCLoose_TightLH_3;   //!
    TBranch *b_lep_SF_El_PLVTight_QmisID_0;   //!
    TBranch *b_lep_SF_El_PLVTight_QmisID_1;   //!
    TBranch *b_lep_SF_El_PLVTight_QmisID_2;   //!
    TBranch *b_lep_SF_El_PLVTight_QmisID_3;   //!
    TBranch *b_lep_SF_El_PLVTight_0;   //!
    TBranch *b_lep_SF_El_PLVTight_1;   //!
    TBranch *b_lep_SF_El_PLVTight_2;   //!
    TBranch *b_lep_SF_El_PLVTight_3;   //!
    TBranch *b_lep_SF_El_PLVLoose_0;   //!
    TBranch *b_lep_SF_El_PLVLoose_1;   //!
    TBranch *b_lep_SF_El_PLVLoose_2;   //!
    TBranch *b_lep_SF_El_PLVLoose_3;   //!
    TBranch *b_lep_SF_El_ID_MediumLH_0;   //!
    TBranch *b_lep_SF_El_ID_MediumLH_1;   //!
    TBranch *b_lep_SF_El_ID_MediumLH_2;   //!
    TBranch *b_lep_SF_El_ID_MediumLH_3;   //!
    TBranch *b_lep_SF_Mu_TTVA_AT_0;   //!
    TBranch *b_lep_SF_Mu_TTVA_AT_1;   //!
    TBranch *b_lep_SF_Mu_TTVA_AT_2;   //!
    TBranch *b_lep_SF_Mu_TTVA_AT_3;   //!
    TBranch *b_lep_SF_Mu_ID_Loose_AT_0;   //!
    TBranch *b_lep_SF_Mu_ID_Loose_AT_1;   //!
    TBranch *b_lep_SF_Mu_ID_Loose_AT_2;   //!
    TBranch *b_lep_SF_Mu_ID_Loose_AT_3;   //!
    TBranch *b_lep_SF_Mu_ID_Medium_AT_0;   //!
    TBranch *b_lep_SF_Mu_ID_Medium_AT_1;   //!
    TBranch *b_lep_SF_Mu_ID_Medium_AT_2;   //!
    TBranch *b_lep_SF_Mu_ID_Medium_AT_3;   //!
    TBranch *b_lep_SF_Mu_Iso_FCLoose_AT_0;   //!
    TBranch *b_lep_SF_Mu_Iso_FCLoose_AT_1;   //!
    TBranch *b_lep_SF_Mu_Iso_FCLoose_AT_2;   //!
    TBranch *b_lep_SF_Mu_Iso_FCLoose_AT_3;   //!
    TBranch *b_lep_SF_Mu_ID_Tight_0;   //!
    TBranch *b_lep_SF_Mu_ID_Tight_1;   //!
    TBranch *b_lep_SF_Mu_ID_Tight_2;   //!
    TBranch *b_lep_SF_Mu_ID_Tight_3;   //!
    TBranch *b_lep_SF_Mu_PLVTight_0;   //!
    TBranch *b_lep_SF_Mu_PLVTight_1;   //!
    TBranch *b_lep_SF_Mu_PLVTight_2;   //!
    TBranch *b_lep_SF_Mu_PLVTight_3;   //!
    TBranch *b_lep_SF_Mu_PLVLoose_0;   //!
    TBranch *b_lep_SF_Mu_PLVLoose_1;   //!
    TBranch *b_lep_SF_Mu_PLVLoose_2;   //!
    TBranch *b_lep_SF_Mu_PLVLoose_3;   //!
    TBranch *b_lep_SF_CombinedLoose_0;   //!
    TBranch *b_lep_SF_CombinedLoose_1;   //!
    TBranch *b_lep_SF_CombinedLoose_2;   //!
    TBranch *b_lep_SF_CombinedLoose_3;   //!
    TBranch *b_lep_SF_CombinedTight_0;   //!
    TBranch *b_lep_SF_CombinedTight_1;   //!
    TBranch *b_lep_SF_CombinedTight_2;   //!
    TBranch *b_lep_SF_CombinedTight_3;   //!
    TBranch *b_tau_E_0;   //!
    TBranch *b_tau_E_1;   //!
    TBranch *b_tau_numTrack_0;   //!
    TBranch *b_tau_numTrack_1;   //!
    TBranch *b_totalEventsWeighted;   //!
    TBranch *b_weight_mc;   //!
    TBranch *b_weight_pileup;   //!
    TBranch *b_mc_genWeights;   //!
    TBranch *b_bTagSF_weight_DL1r_70;   //!
    TBranch *b_bTagSF_weight_DL1r_77;   //!
    TBranch *b_bTagSF_weight_DL1r_85;   //!
    TBranch *b_jvtSF_customOR;   //!
    TBranch *b_lepSFObjLoose;   //!
    TBranch *b_lepSFObjTight;   //!
    TBranch *b_lepSFObjChannelCombined;   //!
    TBranch *b_custTrigSF_LooseID_FCLooseIso_SLTorDLT;   //!
    TBranch *b_custTrigSF_LooseID_FCLooseIso_SLT;   //!
    TBranch *b_custTrigSF_LooseID_FCLooseIso_DLT;   //!
    TBranch *b_custTrigMatch_LooseID_FCLooseIso_SLTorDLT;   //!
    TBranch *b_custTrigMatch_LooseID_FCLooseIso_SLT;   //!
    TBranch *b_custTrigMatch_LooseID_FCLooseIso_DLT;   //!
    TBranch *b_custTrigSF_TightElMediumMuID_FCLooseIso_SLTorDLT;   //!
    TBranch *b_custTrigSF_TightElMediumMuID_FCLooseIso_SLT;   //!
    TBranch *b_custTrigSF_TightElMediumMuID_FCLooseIso_DLT;   //!
    TBranch *b_custTrigMatch_TightElMediumMuID_FCLooseIso_SLTorDLT;   //!
    TBranch *b_custTrigMatch_TightElMediumMuID_FCLooseIso_SLT;   //!
    TBranch *b_custTrigMatch_TightElMediumMuID_FCLooseIso_DLT;   //!
    TBranch *b_eventNumber;   //!
    TBranch *b_runNumber;   //!
    TBranch *b_randomRunNumber;   //!
    TBranch *b_mcChannelNumber;   //!
    TBranch *b_mu;   //!
    TBranch *b_met_met;   //!
    TBranch *b_met_phi;   //!
    TBranch *b_GlobalTrigDecision;   //!
    TBranch *b_GlobalTrigDecision_mu26_ivarmedium;   //!
    TBranch *b_GlobalTrigDecision_mu22_mu8noL1;   //!
    TBranch *b_GlobalTrigDecision_e60_lhmedium_nod0;   //!
    TBranch *b_GlobalTrigDecision_mu18_mu8noL1;   //!
    TBranch *b_GlobalTrigDecision_e26_lhtight_nod0_ivarloose;   //!
    TBranch *b_GlobalTrigDecision_mu20_iloose_L1MU15;   //!
    TBranch *b_GlobalTrigDecision_e24_lhmedium_L1EM20VH;   //!
    TBranch *b_GlobalTrigDecision_mu50;   //!
    TBranch *b_GlobalTrigDecision_e17_lhloose_nod0_mu14;   //!
    TBranch *b_GlobalTrigDecision_2e12_lhloose_L12EM10VH;   //!
    TBranch *b_GlobalTrigDecision_e120_lhloose;   //!
    TBranch *b_GlobalTrigDecision_e140_lhloose_nod0;   //!
    TBranch *b_GlobalTrigDecision_e60_lhmedium;   //!
    TBranch *b_GlobalTrigDecision_2e17_lhvloose_nod0;   //!
    TBranch *b_GlobalTrigDecision_e17_lhloose_mu14;   //!
    TBranch *b_custTrigSF_AT_default_preOR;   //!
    TBranch *b_RunYear;   //!
    TBranch *b_mc_xSection;   //!
    TBranch *b_mc_rawXSection;   //!
    TBranch *b_mc_kFactor;   //!
    TBranch *b_onelep_type;   //!
    TBranch *b_dilep_type;   //!
    TBranch *b_trilep_type;   //!
    TBranch *b_quadlep_type;   //!
    TBranch *b_total_charge;   //!
    TBranch *b_total_leptons;   //!
    TBranch *b_isQMisIDEvent;   //!
    TBranch *b_isFakeEvent;   //!
    TBranch *b_isLepFromPhEvent;   //!
    TBranch *b_Mll01;   //!
    TBranch *b_Mll02;   //!
    TBranch *b_Mll12;   //!
    TBranch *b_Mlll012;   //!
    TBranch *b_Ptll01;   //!
    TBranch *b_Ptll02;   //!
    TBranch *b_Ptll12;   //!
    TBranch *b_DRll01;   //!
    TBranch *b_DRll02;   //!
    TBranch *b_DRll12;   //!
    TBranch *b_DRl0jmin;   //!
    TBranch *b_DRl1jmin;   //!
    TBranch *b_DRl0j0;   //!
    TBranch *b_DRl0j1;   //!
    TBranch *b_DRl1j0;   //!
    TBranch *b_DRl1j1;   //!
    TBranch *b_DRl2j0;   //!
    TBranch *b_DRl2j1;   //!
    TBranch *b_DRj0j1;   //!
    TBranch *b_DRjjMax;   //!
    TBranch *b_mj0j1;   //!
    TBranch *b_ml0j0;   //!
    TBranch *b_ml0j1;   //!
    TBranch *b_ml1j0;   //!
    TBranch *b_ml1j1;   //!
    TBranch *b_ml2j0;   //!
    TBranch *b_ml2j1;   //!
    TBranch *b_matchDLTll01;   //!
    TBranch *b_best_Z_Mll;   //!
    TBranch *b_best_Z_other_MtLepMet;   //!
    TBranch *b_best_Z_other_Mll;   //!
    TBranch *b_nJets_OR;   //!
    TBranch *b_nTruthJets;   //!
    TBranch *b_nTruthJets_OR;   //!
    TBranch *b_nJets_OR_DL1r_85;   //!
    TBranch *b_nJets_OR_DL1r_77;   //!
    TBranch *b_nJets_OR_DL1r_70;   //!
    TBranch *b_nTaus_OR_Pt25;   //!
    TBranch *b_nTaus_OR_Pt25_RNN;   //!
    TBranch *b_HT;   //!
    TBranch *b_HT_lep;   //!
    TBranch *b_HT_jets;   //!
    TBranch *b_lead_jetPt;   //!
    TBranch *b_lead_jetEta;   //!
    TBranch *b_lead_jetPhi;   //!
    TBranch *b_lead_jetE;   //!
    TBranch *b_sublead_jetPt;   //!
    TBranch *b_sublead_jetEta;   //!
    TBranch *b_sublead_jetPhi;   //!
    TBranch *b_sublead_jetE;   //!
    TBranch *b_higgs1Mode;   //!
    TBranch *b_higgs2Mode;   //!
    TBranch *b_jet_pt;   //!
    TBranch *b_jet_eta;   //!
    TBranch *b_jet_phi;   //!
    TBranch *b_jet_e;   //!

    nominal(TTree *tree = nullptr, bool is_data = false);

    virtual ~nominal();

    virtual Int_t Cut(Long64_t entry);

    virtual Int_t GetEntry(Long64_t entry);

    virtual Long64_t LoadTree(Long64_t entry);

    virtual void Init(TTree *tree, bool is_data);

    virtual void Loop();

    virtual Bool_t Notify();

    virtual void Show(Long64_t entry = -1);
};

#endif

#ifdef nominal_cxx

nominal::nominal(TTree *tree, bool is_data) : fChain(nullptr) {
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
    if (tree == nullptr) {
        TFile *f = (TFile *) gROOT->GetListOfFiles()->FindObject(
                "/lustre/collider/zhangyulei/ATLAS/fw1/trilep/v4/mc16a/450662.root");
        if (!f || !f->IsOpen()) {
            f = new TFile("/lustre/collider/zhangyulei/ATLAS/fw1/trilep/v4/mc16a/450662.root");
        }
        f->GetObject("nominal", tree);

    }
    Init(tree, is_data);
}

nominal::~nominal() {
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t nominal::GetEntry(Long64_t entry) {
// Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}

Long64_t nominal::LoadTree(Long64_t entry) {
// Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void nominal::Init(TTree *tree, bool is_data) {
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    // Set object pointer
    mc_genWeights = 0;
    jet_pt = 0;
    jet_eta = 0;
    jet_phi = 0;
    jet_e = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("lep_ID_0", &lep_ID_0, &b_lep_ID_0);
    fChain->SetBranchAddress("lep_ID_1", &lep_ID_1, &b_lep_ID_1);
    fChain->SetBranchAddress("lep_ID_2", &lep_ID_2, &b_lep_ID_2);
    fChain->SetBranchAddress("lep_ID_3", &lep_ID_3, &b_lep_ID_3);
    fChain->SetBranchAddress("lep_Index_0", &lep_Index_0, &b_lep_Index_0);
    fChain->SetBranchAddress("lep_Index_1", &lep_Index_1, &b_lep_Index_1);
    fChain->SetBranchAddress("lep_Index_2", &lep_Index_2, &b_lep_Index_2);
    fChain->SetBranchAddress("lep_Index_3", &lep_Index_3, &b_lep_Index_3);
    fChain->SetBranchAddress("lep_sigd0PV_0", &lep_sigd0PV_0, &b_lep_sigd0PV_0);
    fChain->SetBranchAddress("lep_sigd0PV_1", &lep_sigd0PV_1, &b_lep_sigd0PV_1);
    fChain->SetBranchAddress("lep_sigd0PV_2", &lep_sigd0PV_2, &b_lep_sigd0PV_2);
    fChain->SetBranchAddress("lep_sigd0PV_3", &lep_sigd0PV_3, &b_lep_sigd0PV_3);
    fChain->SetBranchAddress("lep_Z0SinTheta_0", &lep_Z0SinTheta_0, &b_lep_Z0SinTheta_0);
    fChain->SetBranchAddress("lep_Z0SinTheta_1", &lep_Z0SinTheta_1, &b_lep_Z0SinTheta_1);
    fChain->SetBranchAddress("lep_Z0SinTheta_2", &lep_Z0SinTheta_2, &b_lep_Z0SinTheta_2);
    fChain->SetBranchAddress("lep_Z0SinTheta_3", &lep_Z0SinTheta_3, &b_lep_Z0SinTheta_3);
    fChain->SetBranchAddress("lep_d0_0", &lep_d0_0, &b_lep_d0_0);
    fChain->SetBranchAddress("lep_d0_1", &lep_d0_1, &b_lep_d0_1);
    fChain->SetBranchAddress("lep_d0_2", &lep_d0_2, &b_lep_d0_2);
    fChain->SetBranchAddress("lep_d0_3", &lep_d0_3, &b_lep_d0_3);
    fChain->SetBranchAddress("lep_z0_0", &lep_z0_0, &b_lep_z0_0);
    fChain->SetBranchAddress("lep_z0_1", &lep_z0_1, &b_lep_z0_1);
    fChain->SetBranchAddress("lep_z0_2", &lep_z0_2, &b_lep_z0_2);
    fChain->SetBranchAddress("lep_z0_3", &lep_z0_3, &b_lep_z0_3);
    fChain->SetBranchAddress("lep_vz_0", &lep_vz_0, &b_lep_vz_0);
    fChain->SetBranchAddress("lep_vz_1", &lep_vz_1, &b_lep_vz_1);
    fChain->SetBranchAddress("lep_vz_2", &lep_vz_2, &b_lep_vz_2);
    fChain->SetBranchAddress("lep_vz_3", &lep_vz_3, &b_lep_vz_3);
    fChain->SetBranchAddress("lep_deltaz0_0", &lep_deltaz0_0, &b_lep_deltaz0_0);
    fChain->SetBranchAddress("lep_deltaz0_1", &lep_deltaz0_1, &b_lep_deltaz0_1);
    fChain->SetBranchAddress("lep_deltaz0_2", &lep_deltaz0_2, &b_lep_deltaz0_2);
    fChain->SetBranchAddress("lep_deltaz0_3", &lep_deltaz0_3, &b_lep_deltaz0_3);
    fChain->SetBranchAddress("lep_Pt_0", &lep_Pt_0, &b_lep_Pt_0);
    fChain->SetBranchAddress("lep_Pt_1", &lep_Pt_1, &b_lep_Pt_1);
    fChain->SetBranchAddress("lep_Pt_2", &lep_Pt_2, &b_lep_Pt_2);
    fChain->SetBranchAddress("lep_Pt_3", &lep_Pt_3, &b_lep_Pt_3);
    fChain->SetBranchAddress("lep_E_0", &lep_E_0, &b_lep_E_0);
    fChain->SetBranchAddress("lep_E_1", &lep_E_1, &b_lep_E_1);
    fChain->SetBranchAddress("lep_E_2", &lep_E_2, &b_lep_E_2);
    fChain->SetBranchAddress("lep_E_3", &lep_E_3, &b_lep_E_3);
    fChain->SetBranchAddress("lep_Eta_0", &lep_Eta_0, &b_lep_Eta_0);
    fChain->SetBranchAddress("lep_Eta_1", &lep_Eta_1, &b_lep_Eta_1);
    fChain->SetBranchAddress("lep_Eta_2", &lep_Eta_2, &b_lep_Eta_2);
    fChain->SetBranchAddress("lep_Eta_3", &lep_Eta_3, &b_lep_Eta_3);
    fChain->SetBranchAddress("lep_Phi_0", &lep_Phi_0, &b_lep_Phi_0);
    fChain->SetBranchAddress("lep_Phi_1", &lep_Phi_1, &b_lep_Phi_1);
    fChain->SetBranchAddress("lep_Phi_2", &lep_Phi_2, &b_lep_Phi_2);
    fChain->SetBranchAddress("lep_Phi_3", &lep_Phi_3, &b_lep_Phi_3);
    fChain->SetBranchAddress("lep_isTightLH_0", &lep_isTightLH_0, &b_lep_isTightLH_0);
    fChain->SetBranchAddress("lep_isTightLH_1", &lep_isTightLH_1, &b_lep_isTightLH_1);
    fChain->SetBranchAddress("lep_isTightLH_2", &lep_isTightLH_2, &b_lep_isTightLH_2);
    fChain->SetBranchAddress("lep_isTightLH_3", &lep_isTightLH_3, &b_lep_isTightLH_3);
    fChain->SetBranchAddress("lep_isMediumLH_0", &lep_isMediumLH_0, &b_lep_isMediumLH_0);
    fChain->SetBranchAddress("lep_isMediumLH_1", &lep_isMediumLH_1, &b_lep_isMediumLH_1);
    fChain->SetBranchAddress("lep_isMediumLH_2", &lep_isMediumLH_2, &b_lep_isMediumLH_2);
    fChain->SetBranchAddress("lep_isMediumLH_3", &lep_isMediumLH_3, &b_lep_isMediumLH_3);
    fChain->SetBranchAddress("lep_isLooseLH_0", &lep_isLooseLH_0, &b_lep_isLooseLH_0);
    fChain->SetBranchAddress("lep_isLooseLH_1", &lep_isLooseLH_1, &b_lep_isLooseLH_1);
    fChain->SetBranchAddress("lep_isLooseLH_2", &lep_isLooseLH_2, &b_lep_isLooseLH_2);
    fChain->SetBranchAddress("lep_isLooseLH_3", &lep_isLooseLH_3, &b_lep_isLooseLH_3);
    fChain->SetBranchAddress("lep_isLoose_0", &lep_isLoose_0, &b_lep_isLoose_0);
    fChain->SetBranchAddress("lep_isLoose_1", &lep_isLoose_1, &b_lep_isLoose_1);
    fChain->SetBranchAddress("lep_isLoose_2", &lep_isLoose_2, &b_lep_isLoose_2);
    fChain->SetBranchAddress("lep_isLoose_3", &lep_isLoose_3, &b_lep_isLoose_3);
    fChain->SetBranchAddress("lep_isTight_0", &lep_isTight_0, &b_lep_isTight_0);
    fChain->SetBranchAddress("lep_isTight_1", &lep_isTight_1, &b_lep_isTight_1);
    fChain->SetBranchAddress("lep_isTight_2", &lep_isTight_2, &b_lep_isTight_2);
    fChain->SetBranchAddress("lep_isTight_3", &lep_isTight_3, &b_lep_isTight_3);
    fChain->SetBranchAddress("lep_isMedium_0", &lep_isMedium_0, &b_lep_isMedium_0);
    fChain->SetBranchAddress("lep_isMedium_1", &lep_isMedium_1, &b_lep_isMedium_1);
    fChain->SetBranchAddress("lep_isMedium_2", &lep_isMedium_2, &b_lep_isMedium_2);
    fChain->SetBranchAddress("lep_isMedium_3", &lep_isMedium_3, &b_lep_isMedium_3);
    fChain->SetBranchAddress("lep_isolationLoose_0", &lep_isolationLoose_0, &b_lep_isolationLoose_0);
    fChain->SetBranchAddress("lep_isolationLoose_1", &lep_isolationLoose_1, &b_lep_isolationLoose_1);
    fChain->SetBranchAddress("lep_isolationLoose_2", &lep_isolationLoose_2, &b_lep_isolationLoose_2);
    fChain->SetBranchAddress("lep_isolationLoose_3", &lep_isolationLoose_3, &b_lep_isolationLoose_3);
    fChain->SetBranchAddress("lep_isolationGradient_0", &lep_isolationGradient_0, &b_lep_isolationGradient_0);
    fChain->SetBranchAddress("lep_isolationGradient_1", &lep_isolationGradient_1, &b_lep_isolationGradient_1);
    fChain->SetBranchAddress("lep_isolationGradient_2", &lep_isolationGradient_2, &b_lep_isolationGradient_2);
    fChain->SetBranchAddress("lep_isolationGradient_3", &lep_isolationGradient_3, &b_lep_isolationGradient_3);
    fChain->SetBranchAddress("lep_isolationGradientLoose_0", &lep_isolationGradientLoose_0,
                             &b_lep_isolationGradientLoose_0);
    fChain->SetBranchAddress("lep_isolationGradientLoose_1", &lep_isolationGradientLoose_1,
                             &b_lep_isolationGradientLoose_1);
    fChain->SetBranchAddress("lep_isolationGradientLoose_2", &lep_isolationGradientLoose_2,
                             &b_lep_isolationGradientLoose_2);
    fChain->SetBranchAddress("lep_isolationGradientLoose_3", &lep_isolationGradientLoose_3,
                             &b_lep_isolationGradientLoose_3);
    fChain->SetBranchAddress("lep_isolationTightTrackOnly_0", &lep_isolationTightTrackOnly_0,
                             &b_lep_isolationTightTrackOnly_0);
    fChain->SetBranchAddress("lep_isolationTightTrackOnly_1", &lep_isolationTightTrackOnly_1,
                             &b_lep_isolationTightTrackOnly_1);
    fChain->SetBranchAddress("lep_isolationTightTrackOnly_2", &lep_isolationTightTrackOnly_2,
                             &b_lep_isolationTightTrackOnly_2);
    fChain->SetBranchAddress("lep_isolationTightTrackOnly_3", &lep_isolationTightTrackOnly_3,
                             &b_lep_isolationTightTrackOnly_3);
    fChain->SetBranchAddress("lep_isolationFCLoose_0", &lep_isolationFCLoose_0, &b_lep_isolationFCLoose_0);
    fChain->SetBranchAddress("lep_isolationFCLoose_1", &lep_isolationFCLoose_1, &b_lep_isolationFCLoose_1);
    fChain->SetBranchAddress("lep_isolationFCLoose_2", &lep_isolationFCLoose_2, &b_lep_isolationFCLoose_2);
    fChain->SetBranchAddress("lep_isolationFCLoose_3", &lep_isolationFCLoose_3, &b_lep_isolationFCLoose_3);
    fChain->SetBranchAddress("lep_isolationFCTight_0", &lep_isolationFCTight_0, &b_lep_isolationFCTight_0);
    fChain->SetBranchAddress("lep_isolationFCTight_1", &lep_isolationFCTight_1, &b_lep_isolationFCTight_1);
    fChain->SetBranchAddress("lep_isolationFCTight_2", &lep_isolationFCTight_2, &b_lep_isolationFCTight_2);
    fChain->SetBranchAddress("lep_isolationFCTight_3", &lep_isolationFCTight_3, &b_lep_isolationFCTight_3);
    fChain->SetBranchAddress("lep_isolationPflowTight_0", &lep_isolationPflowTight_0, &b_lep_isolationPflowTight_0);
    fChain->SetBranchAddress("lep_isolationPflowTight_1", &lep_isolationPflowTight_1, &b_lep_isolationPflowTight_1);
    fChain->SetBranchAddress("lep_isolationPflowTight_2", &lep_isolationPflowTight_2, &b_lep_isolationPflowTight_2);
    fChain->SetBranchAddress("lep_isolationPflowTight_3", &lep_isolationPflowTight_3, &b_lep_isolationPflowTight_3);
    fChain->SetBranchAddress("lep_isolationPflowLoose_0", &lep_isolationPflowLoose_0, &b_lep_isolationPflowLoose_0);
    fChain->SetBranchAddress("lep_isolationPflowLoose_1", &lep_isolationPflowLoose_1, &b_lep_isolationPflowLoose_1);
    fChain->SetBranchAddress("lep_isolationPflowLoose_2", &lep_isolationPflowLoose_2, &b_lep_isolationPflowLoose_2);
    fChain->SetBranchAddress("lep_isolationPflowLoose_3", &lep_isolationPflowLoose_3, &b_lep_isolationPflowLoose_3);
    fChain->SetBranchAddress("lep_isPrompt_0", &lep_isPrompt_0, &b_lep_isPrompt_0);
    fChain->SetBranchAddress("lep_isPrompt_1", &lep_isPrompt_1, &b_lep_isPrompt_1);
    fChain->SetBranchAddress("lep_isPrompt_2", &lep_isPrompt_2, &b_lep_isPrompt_2);
    fChain->SetBranchAddress("lep_isPrompt_3", &lep_isPrompt_3, &b_lep_isPrompt_3);
    fChain->SetBranchAddress("lep_truthType_0", &lep_truthType_0, &b_lep_truthType_0);
    fChain->SetBranchAddress("lep_truthType_1", &lep_truthType_1, &b_lep_truthType_1);
    fChain->SetBranchAddress("lep_truthType_2", &lep_truthType_2, &b_lep_truthType_2);
    fChain->SetBranchAddress("lep_truthType_3", &lep_truthType_3, &b_lep_truthType_3);
    fChain->SetBranchAddress("lep_truthOrigin_0", &lep_truthOrigin_0, &b_lep_truthOrigin_0);
    fChain->SetBranchAddress("lep_truthOrigin_1", &lep_truthOrigin_1, &b_lep_truthOrigin_1);
    fChain->SetBranchAddress("lep_truthOrigin_2", &lep_truthOrigin_2, &b_lep_truthOrigin_2);
    fChain->SetBranchAddress("lep_truthOrigin_3", &lep_truthOrigin_3, &b_lep_truthOrigin_3);
    fChain->SetBranchAddress("lep_chargeIDBDTLoose_0", &lep_chargeIDBDTLoose_0, &b_lep_chargeIDBDTLoose_0);
    fChain->SetBranchAddress("lep_chargeIDBDTLoose_1", &lep_chargeIDBDTLoose_1, &b_lep_chargeIDBDTLoose_1);
    fChain->SetBranchAddress("lep_chargeIDBDTLoose_2", &lep_chargeIDBDTLoose_2, &b_lep_chargeIDBDTLoose_2);
    fChain->SetBranchAddress("lep_chargeIDBDTLoose_3", &lep_chargeIDBDTLoose_3, &b_lep_chargeIDBDTLoose_3);
    fChain->SetBranchAddress("lep_chargeIDBDTResult_0", &lep_chargeIDBDTResult_0, &b_lep_chargeIDBDTResult_0);
    fChain->SetBranchAddress("lep_chargeIDBDTResult_1", &lep_chargeIDBDTResult_1, &b_lep_chargeIDBDTResult_1);
    fChain->SetBranchAddress("lep_chargeIDBDTResult_2", &lep_chargeIDBDTResult_2, &b_lep_chargeIDBDTResult_2);
    fChain->SetBranchAddress("lep_chargeIDBDTResult_3", &lep_chargeIDBDTResult_3, &b_lep_chargeIDBDTResult_3);
    fChain->SetBranchAddress("tau_pt_0", &tau_pt_0, &b_tau_pt_0);
    fChain->SetBranchAddress("tau_pt_1", &tau_pt_1, &b_tau_pt_1);
    fChain->SetBranchAddress("tau_eta_0", &tau_eta_0, &b_tau_eta_0);
    fChain->SetBranchAddress("tau_eta_1", &tau_eta_1, &b_tau_eta_1);
    fChain->SetBranchAddress("tau_charge_0", &tau_charge_0, &b_tau_charge_0);
    fChain->SetBranchAddress("tau_charge_1", &tau_charge_1, &b_tau_charge_1);
    fChain->SetBranchAddress("tau_width_0", &tau_width_0, &b_tau_width_0);
    fChain->SetBranchAddress("tau_width_1", &tau_width_1, &b_tau_width_1);
    fChain->SetBranchAddress("tau_passJetIDRNNLoose_0", &tau_passJetIDRNNLoose_0, &b_tau_passJetIDRNNLoose_0);
    fChain->SetBranchAddress("tau_passJetIDRNNLoose_1", &tau_passJetIDRNNLoose_1, &b_tau_passJetIDRNNLoose_1);
    fChain->SetBranchAddress("tau_passJetIDRNNMed_0", &tau_passJetIDRNNMed_0, &b_tau_passJetIDRNNMed_0);
    fChain->SetBranchAddress("tau_passJetIDRNNMed_1", &tau_passJetIDRNNMed_1, &b_tau_passJetIDRNNMed_1);
    fChain->SetBranchAddress("tau_passEleOLR_0", &tau_passEleOLR_0, &b_tau_passEleOLR_0);
    fChain->SetBranchAddress("tau_passEleOLR_1", &tau_passEleOLR_1, &b_tau_passEleOLR_1);
    fChain->SetBranchAddress("tau_passEleBDT_0", &tau_passEleBDT_0, &b_tau_passEleBDT_0);
    fChain->SetBranchAddress("tau_passEleBDT_1", &tau_passEleBDT_1, &b_tau_passEleBDT_1);
    fChain->SetBranchAddress("tau_passEleBDTMed_0", &tau_passEleBDTMed_0, &b_tau_passEleBDTMed_0);
    fChain->SetBranchAddress("tau_passEleBDTMed_1", &tau_passEleBDTMed_1, &b_tau_passEleBDTMed_1);
    fChain->SetBranchAddress("tau_passMuonOLR_0", &tau_passMuonOLR_0, &b_tau_passMuonOLR_0);
    fChain->SetBranchAddress("tau_passMuonOLR_1", &tau_passMuonOLR_1, &b_tau_passMuonOLR_1);
    fChain->SetBranchAddress("lep_isFakeLep_0", &lep_isFakeLep_0, &b_lep_isFakeLep_0);
    fChain->SetBranchAddress("lep_isFakeLep_1", &lep_isFakeLep_1, &b_lep_isFakeLep_1);
    fChain->SetBranchAddress("lep_isFakeLep_2", &lep_isFakeLep_2, &b_lep_isFakeLep_2);
    fChain->SetBranchAddress("lep_isFakeLep_3", &lep_isFakeLep_3, &b_lep_isFakeLep_3);
    fChain->SetBranchAddress("lep_isTrigMatch_0", &lep_isTrigMatch_0, &b_lep_isTrigMatch_0);
    fChain->SetBranchAddress("lep_isTrigMatch_1", &lep_isTrigMatch_1, &b_lep_isTrigMatch_1);
    fChain->SetBranchAddress("lep_isTrigMatch_2", &lep_isTrigMatch_2, &b_lep_isTrigMatch_2);
    fChain->SetBranchAddress("lep_isTrigMatch_3", &lep_isTrigMatch_3, &b_lep_isTrigMatch_3);
    fChain->SetBranchAddress("lep_isTrigMatchDLT_0", &lep_isTrigMatchDLT_0, &b_lep_isTrigMatchDLT_0);
    fChain->SetBranchAddress("lep_isTrigMatchDLT_1", &lep_isTrigMatchDLT_1, &b_lep_isTrigMatchDLT_1);
    fChain->SetBranchAddress("lep_isTrigMatchDLT_2", &lep_isTrigMatchDLT_2, &b_lep_isTrigMatchDLT_2);
    fChain->SetBranchAddress("lep_isTrigMatchDLT_3", &lep_isTrigMatchDLT_3, &b_lep_isTrigMatchDLT_3);
    fChain->SetBranchAddress("lep_ambiguityType_0", &lep_ambiguityType_0, &b_lep_ambiguityType_0);
    fChain->SetBranchAddress("lep_ambiguityType_1", &lep_ambiguityType_1, &b_lep_ambiguityType_1);
    fChain->SetBranchAddress("lep_ambiguityType_2", &lep_ambiguityType_2, &b_lep_ambiguityType_2);
    fChain->SetBranchAddress("lep_ambiguityType_3", &lep_ambiguityType_3, &b_lep_ambiguityType_3);
    fChain->SetBranchAddress("lep_isQMisID_0", &lep_isQMisID_0, &b_lep_isQMisID_0);
    fChain->SetBranchAddress("lep_isQMisID_1", &lep_isQMisID_1, &b_lep_isQMisID_1);
    fChain->SetBranchAddress("lep_isQMisID_2", &lep_isQMisID_2, &b_lep_isQMisID_2);
    fChain->SetBranchAddress("lep_isQMisID_3", &lep_isQMisID_3, &b_lep_isQMisID_3);
    fChain->SetBranchAddress("lep_isConvPh_0", &lep_isConvPh_0, &b_lep_isConvPh_0);
    fChain->SetBranchAddress("lep_isConvPh_1", &lep_isConvPh_1, &b_lep_isConvPh_1);
    fChain->SetBranchAddress("lep_isConvPh_2", &lep_isConvPh_2, &b_lep_isConvPh_2);
    fChain->SetBranchAddress("lep_isConvPh_3", &lep_isConvPh_3, &b_lep_isConvPh_3);
    fChain->SetBranchAddress("lep_isExtConvPh_0", &lep_isExtConvPh_0, &b_lep_isExtConvPh_0);
    fChain->SetBranchAddress("lep_isExtConvPh_1", &lep_isExtConvPh_1, &b_lep_isExtConvPh_1);
    fChain->SetBranchAddress("lep_isExtConvPh_2", &lep_isExtConvPh_2, &b_lep_isExtConvPh_2);
    fChain->SetBranchAddress("lep_isExtConvPh_3", &lep_isExtConvPh_3, &b_lep_isExtConvPh_3);
    fChain->SetBranchAddress("lep_isIntConvPh_0", &lep_isIntConvPh_0, &b_lep_isIntConvPh_0);
    fChain->SetBranchAddress("lep_isIntConvPh_1", &lep_isIntConvPh_1, &b_lep_isIntConvPh_1);
    fChain->SetBranchAddress("lep_isIntConvPh_2", &lep_isIntConvPh_2, &b_lep_isIntConvPh_2);
    fChain->SetBranchAddress("lep_isIntConvPh_3", &lep_isIntConvPh_3, &b_lep_isIntConvPh_3);
    fChain->SetBranchAddress("lep_isBrems_0", &lep_isBrems_0, &b_lep_isBrems_0);
    fChain->SetBranchAddress("lep_isBrems_1", &lep_isBrems_1, &b_lep_isBrems_1);
    fChain->SetBranchAddress("lep_isBrems_2", &lep_isBrems_2, &b_lep_isBrems_2);
    fChain->SetBranchAddress("lep_isBrems_3", &lep_isBrems_3, &b_lep_isBrems_3);
    fChain->SetBranchAddress("lep_Mtrktrk_atPV_CO_0", &lep_Mtrktrk_atPV_CO_0, &b_lep_Mtrktrk_atPV_CO_0);
    fChain->SetBranchAddress("lep_Mtrktrk_atPV_CO_1", &lep_Mtrktrk_atPV_CO_1, &b_lep_Mtrktrk_atPV_CO_1);
    fChain->SetBranchAddress("lep_Mtrktrk_atPV_CO_2", &lep_Mtrktrk_atPV_CO_2, &b_lep_Mtrktrk_atPV_CO_2);
    fChain->SetBranchAddress("lep_Mtrktrk_atPV_CO_3", &lep_Mtrktrk_atPV_CO_3, &b_lep_Mtrktrk_atPV_CO_3);
    fChain->SetBranchAddress("lep_Mtrktrk_atConvV_CO_0", &lep_Mtrktrk_atConvV_CO_0, &b_lep_Mtrktrk_atConvV_CO_0);
    fChain->SetBranchAddress("lep_Mtrktrk_atConvV_CO_1", &lep_Mtrktrk_atConvV_CO_1, &b_lep_Mtrktrk_atConvV_CO_1);
    fChain->SetBranchAddress("lep_Mtrktrk_atConvV_CO_2", &lep_Mtrktrk_atConvV_CO_2, &b_lep_Mtrktrk_atConvV_CO_2);
    fChain->SetBranchAddress("lep_Mtrktrk_atConvV_CO_3", &lep_Mtrktrk_atConvV_CO_3, &b_lep_Mtrktrk_atConvV_CO_3);
    fChain->SetBranchAddress("lep_RadiusCO_0", &lep_RadiusCO_0, &b_lep_RadiusCO_0);
    fChain->SetBranchAddress("lep_RadiusCO_1", &lep_RadiusCO_1, &b_lep_RadiusCO_1);
    fChain->SetBranchAddress("lep_RadiusCO_2", &lep_RadiusCO_2, &b_lep_RadiusCO_2);
    fChain->SetBranchAddress("lep_RadiusCO_3", &lep_RadiusCO_3, &b_lep_RadiusCO_3);
    fChain->SetBranchAddress("lep_RadiusCOX_0", &lep_RadiusCOX_0, &b_lep_RadiusCOX_0);
    fChain->SetBranchAddress("lep_RadiusCOX_1", &lep_RadiusCOX_1, &b_lep_RadiusCOX_1);
    fChain->SetBranchAddress("lep_RadiusCOX_2", &lep_RadiusCOX_2, &b_lep_RadiusCOX_2);
    fChain->SetBranchAddress("lep_RadiusCOX_3", &lep_RadiusCOX_3, &b_lep_RadiusCOX_3);
    fChain->SetBranchAddress("lep_RadiusCOY_0", &lep_RadiusCOY_0, &b_lep_RadiusCOY_0);
    fChain->SetBranchAddress("lep_RadiusCOY_1", &lep_RadiusCOY_1, &b_lep_RadiusCOY_1);
    fChain->SetBranchAddress("lep_RadiusCOY_2", &lep_RadiusCOY_2, &b_lep_RadiusCOY_2);
    fChain->SetBranchAddress("lep_RadiusCOY_3", &lep_RadiusCOY_3, &b_lep_RadiusCOY_3);
    fChain->SetBranchAddress("lep_truthParentPdgId_0", &lep_truthParentPdgId_0, &b_lep_truthParentPdgId_0);
    fChain->SetBranchAddress("lep_truthParentPdgId_1", &lep_truthParentPdgId_1, &b_lep_truthParentPdgId_1);
    fChain->SetBranchAddress("lep_truthParentPdgId_2", &lep_truthParentPdgId_2, &b_lep_truthParentPdgId_2);
    fChain->SetBranchAddress("lep_truthParentPdgId_3", &lep_truthParentPdgId_3, &b_lep_truthParentPdgId_3);
    fChain->SetBranchAddress("lep_truthParentOrigin_0", &lep_truthParentOrigin_0, &b_lep_truthParentOrigin_0);
    fChain->SetBranchAddress("lep_truthParentOrigin_1", &lep_truthParentOrigin_1, &b_lep_truthParentOrigin_1);
    fChain->SetBranchAddress("lep_truthParentOrigin_2", &lep_truthParentOrigin_2, &b_lep_truthParentOrigin_2);
    fChain->SetBranchAddress("lep_truthParentOrigin_3", &lep_truthParentOrigin_3, &b_lep_truthParentOrigin_3);
    fChain->SetBranchAddress("lep_isISR_FSR_Ph_0", &lep_isISR_FSR_Ph_0, &b_lep_isISR_FSR_Ph_0);
    fChain->SetBranchAddress("lep_isISR_FSR_Ph_1", &lep_isISR_FSR_Ph_1, &b_lep_isISR_FSR_Ph_1);
    fChain->SetBranchAddress("lep_isISR_FSR_Ph_2", &lep_isISR_FSR_Ph_2, &b_lep_isISR_FSR_Ph_2);
    fChain->SetBranchAddress("lep_isISR_FSR_Ph_3", &lep_isISR_FSR_Ph_3, &b_lep_isISR_FSR_Ph_3);
    fChain->SetBranchAddress("lep_nTrackParticles_0", &lep_nTrackParticles_0, &b_lep_nTrackParticles_0);
    fChain->SetBranchAddress("lep_nTrackParticles_1", &lep_nTrackParticles_1, &b_lep_nTrackParticles_1);
    fChain->SetBranchAddress("lep_nTrackParticles_2", &lep_nTrackParticles_2, &b_lep_nTrackParticles_2);
    fChain->SetBranchAddress("lep_nTrackParticles_3", &lep_nTrackParticles_3, &b_lep_nTrackParticles_3);
    fChain->SetBranchAddress("lep_isTruthMatched_0", &lep_isTruthMatched_0, &b_lep_isTruthMatched_0);
    fChain->SetBranchAddress("lep_isTruthMatched_1", &lep_isTruthMatched_1, &b_lep_isTruthMatched_1);
    fChain->SetBranchAddress("lep_isTruthMatched_2", &lep_isTruthMatched_2, &b_lep_isTruthMatched_2);
    fChain->SetBranchAddress("lep_isTruthMatched_3", &lep_isTruthMatched_3, &b_lep_isTruthMatched_3);
    fChain->SetBranchAddress("lep_truthParentType_0", &lep_truthParentType_0, &b_lep_truthParentType_0);
    fChain->SetBranchAddress("lep_truthParentType_1", &lep_truthParentType_1, &b_lep_truthParentType_1);
    fChain->SetBranchAddress("lep_truthParentType_2", &lep_truthParentType_2, &b_lep_truthParentType_2);
    fChain->SetBranchAddress("lep_truthParentType_3", &lep_truthParentType_3, &b_lep_truthParentType_3);
    fChain->SetBranchAddress("lep_truthPdgId_0", &lep_truthPdgId_0, &b_lep_truthPdgId_0);
    fChain->SetBranchAddress("lep_truthPdgId_1", &lep_truthPdgId_1, &b_lep_truthPdgId_1);
    fChain->SetBranchAddress("lep_truthPdgId_2", &lep_truthPdgId_2, &b_lep_truthPdgId_2);
    fChain->SetBranchAddress("lep_truthPdgId_3", &lep_truthPdgId_3, &b_lep_truthPdgId_3);
    fChain->SetBranchAddress("lep_truthStatus_0", &lep_truthStatus_0, &b_lep_truthStatus_0);
    fChain->SetBranchAddress("lep_truthStatus_1", &lep_truthStatus_1, &b_lep_truthStatus_1);
    fChain->SetBranchAddress("lep_truthStatus_2", &lep_truthStatus_2, &b_lep_truthStatus_2);
    fChain->SetBranchAddress("lep_truthStatus_3", &lep_truthStatus_3, &b_lep_truthStatus_3);
    fChain->SetBranchAddress("lep_truthParentStatus_0", &lep_truthParentStatus_0, &b_lep_truthParentStatus_0);
    fChain->SetBranchAddress("lep_truthParentStatus_1", &lep_truthParentStatus_1, &b_lep_truthParentStatus_1);
    fChain->SetBranchAddress("lep_truthParentStatus_2", &lep_truthParentStatus_2, &b_lep_truthParentStatus_2);
    fChain->SetBranchAddress("lep_truthParentStatus_3", &lep_truthParentStatus_3, &b_lep_truthParentStatus_3);
    fChain->SetBranchAddress("lep_plvWP_Loose_0", &lep_plvWP_Loose_0, &b_lep_plvWP_Loose_0);
    fChain->SetBranchAddress("lep_plvWP_Loose_1", &lep_plvWP_Loose_1, &b_lep_plvWP_Loose_1);
    fChain->SetBranchAddress("lep_plvWP_Loose_2", &lep_plvWP_Loose_2, &b_lep_plvWP_Loose_2);
    fChain->SetBranchAddress("lep_plvWP_Loose_3", &lep_plvWP_Loose_3, &b_lep_plvWP_Loose_3);
    fChain->SetBranchAddress("lep_plvWP_Tight_0", &lep_plvWP_Tight_0, &b_lep_plvWP_Tight_0);
    fChain->SetBranchAddress("lep_plvWP_Tight_1", &lep_plvWP_Tight_1, &b_lep_plvWP_Tight_1);
    fChain->SetBranchAddress("lep_plvWP_Tight_2", &lep_plvWP_Tight_2, &b_lep_plvWP_Tight_2);
    fChain->SetBranchAddress("lep_plvWP_Tight_3", &lep_plvWP_Tight_3, &b_lep_plvWP_Tight_3);
    fChain->SetBranchAddress("lep_truthPt_0", &lep_truthPt_0, &b_lep_truthPt_0);
    fChain->SetBranchAddress("lep_truthPt_1", &lep_truthPt_1, &b_lep_truthPt_1);
    fChain->SetBranchAddress("lep_truthPt_2", &lep_truthPt_2, &b_lep_truthPt_2);
    fChain->SetBranchAddress("lep_truthPt_3", &lep_truthPt_3, &b_lep_truthPt_3);
    fChain->SetBranchAddress("lep_truthEta_0", &lep_truthEta_0, &b_lep_truthEta_0);
    fChain->SetBranchAddress("lep_truthEta_1", &lep_truthEta_1, &b_lep_truthEta_1);
    fChain->SetBranchAddress("lep_truthEta_2", &lep_truthEta_2, &b_lep_truthEta_2);
    fChain->SetBranchAddress("lep_truthEta_3", &lep_truthEta_3, &b_lep_truthEta_3);
    fChain->SetBranchAddress("lep_truthPhi_0", &lep_truthPhi_0, &b_lep_truthPhi_0);
    fChain->SetBranchAddress("lep_truthPhi_1", &lep_truthPhi_1, &b_lep_truthPhi_1);
    fChain->SetBranchAddress("lep_truthPhi_2", &lep_truthPhi_2, &b_lep_truthPhi_2);
    fChain->SetBranchAddress("lep_truthPhi_3", &lep_truthPhi_3, &b_lep_truthPhi_3);
    fChain->SetBranchAddress("lep_truthM_0", &lep_truthM_0, &b_lep_truthM_0);
    fChain->SetBranchAddress("lep_truthM_1", &lep_truthM_1, &b_lep_truthM_1);
    fChain->SetBranchAddress("lep_truthM_2", &lep_truthM_2, &b_lep_truthM_2);
    fChain->SetBranchAddress("lep_truthM_3", &lep_truthM_3, &b_lep_truthM_3);
    fChain->SetBranchAddress("lep_truthE_0", &lep_truthE_0, &b_lep_truthE_0);
    fChain->SetBranchAddress("lep_truthE_1", &lep_truthE_1, &b_lep_truthE_1);
    fChain->SetBranchAddress("lep_truthE_2", &lep_truthE_2, &b_lep_truthE_2);
    fChain->SetBranchAddress("lep_truthE_3", &lep_truthE_3, &b_lep_truthE_3);
    fChain->SetBranchAddress("lep_truthRapidity_0", &lep_truthRapidity_0, &b_lep_truthRapidity_0);
    fChain->SetBranchAddress("lep_truthRapidity_1", &lep_truthRapidity_1, &b_lep_truthRapidity_1);
    fChain->SetBranchAddress("lep_truthRapidity_2", &lep_truthRapidity_2, &b_lep_truthRapidity_2);
    fChain->SetBranchAddress("lep_truthRapidity_3", &lep_truthRapidity_3, &b_lep_truthRapidity_3);
//    fChain->SetBranchAddress("lep_ambiguityType_0", &lep_ambiguityType_0, &b_lep_ambiguityType_0);
//    fChain->SetBranchAddress("lep_ambiguityType_1", &lep_ambiguityType_1, &b_lep_ambiguityType_1);
//    fChain->SetBranchAddress("lep_ambiguityType_2", &lep_ambiguityType_2, &b_lep_ambiguityType_2);
//    fChain->SetBranchAddress("lep_ambiguityType_3", &lep_ambiguityType_3, &b_lep_ambiguityType_3);
    fChain->SetBranchAddress("lep_nInnerPix_0", &lep_nInnerPix_0, &b_lep_nInnerPix_0);
    fChain->SetBranchAddress("lep_nInnerPix_1", &lep_nInnerPix_1, &b_lep_nInnerPix_1);
    fChain->SetBranchAddress("lep_nInnerPix_2", &lep_nInnerPix_2, &b_lep_nInnerPix_2);
    fChain->SetBranchAddress("lep_nInnerPix_3", &lep_nInnerPix_3, &b_lep_nInnerPix_3);
    fChain->SetBranchAddress("lep_firstEgMotherPdgId_0", &lep_firstEgMotherPdgId_0, &b_lep_firstEgMotherPdgId_0);
    fChain->SetBranchAddress("lep_firstEgMotherPdgId_1", &lep_firstEgMotherPdgId_1, &b_lep_firstEgMotherPdgId_1);
    fChain->SetBranchAddress("lep_firstEgMotherPdgId_2", &lep_firstEgMotherPdgId_2, &b_lep_firstEgMotherPdgId_2);
    fChain->SetBranchAddress("lep_firstEgMotherPdgId_3", &lep_firstEgMotherPdgId_3, &b_lep_firstEgMotherPdgId_3);
    fChain->SetBranchAddress("lep_firstEgMotherTruthType_0", &lep_firstEgMotherTruthType_0,
                             &b_lep_firstEgMotherTruthType_0);
    fChain->SetBranchAddress("lep_firstEgMotherTruthType_1", &lep_firstEgMotherTruthType_1,
                             &b_lep_firstEgMotherTruthType_1);
    fChain->SetBranchAddress("lep_firstEgMotherTruthType_2", &lep_firstEgMotherTruthType_2,
                             &b_lep_firstEgMotherTruthType_2);
    fChain->SetBranchAddress("lep_firstEgMotherTruthType_3", &lep_firstEgMotherTruthType_3,
                             &b_lep_firstEgMotherTruthType_3);
    fChain->SetBranchAddress("lep_firstEgMotherTruthOrigin_0", &lep_firstEgMotherTruthOrigin_0,
                             &b_lep_firstEgMotherTruthOrigin_0);
    fChain->SetBranchAddress("lep_firstEgMotherTruthOrigin_1", &lep_firstEgMotherTruthOrigin_1,
                             &b_lep_firstEgMotherTruthOrigin_1);
    fChain->SetBranchAddress("lep_firstEgMotherTruthOrigin_2", &lep_firstEgMotherTruthOrigin_2,
                             &b_lep_firstEgMotherTruthOrigin_2);
    fChain->SetBranchAddress("lep_firstEgMotherTruthOrigin_3", &lep_firstEgMotherTruthOrigin_3,
                             &b_lep_firstEgMotherTruthOrigin_3);
    fChain->SetBranchAddress("lep_promptLeptonIso_TagWeight_0", &lep_promptLeptonIso_TagWeight_0,
                             &b_lep_promptLeptonIso_TagWeight_0);
    fChain->SetBranchAddress("lep_promptLeptonIso_TagWeight_1", &lep_promptLeptonIso_TagWeight_1,
                             &b_lep_promptLeptonIso_TagWeight_1);
    fChain->SetBranchAddress("lep_promptLeptonIso_TagWeight_2", &lep_promptLeptonIso_TagWeight_2,
                             &b_lep_promptLeptonIso_TagWeight_2);
    fChain->SetBranchAddress("lep_promptLeptonIso_TagWeight_3", &lep_promptLeptonIso_TagWeight_3,
                             &b_lep_promptLeptonIso_TagWeight_3);
    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_0", &lep_promptLeptonVeto_TagWeight_0,
                             &b_lep_promptLeptonVeto_TagWeight_0);
    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_1", &lep_promptLeptonVeto_TagWeight_1,
                             &b_lep_promptLeptonVeto_TagWeight_1);
    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_2", &lep_promptLeptonVeto_TagWeight_2,
                             &b_lep_promptLeptonVeto_TagWeight_2);
    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_3", &lep_promptLeptonVeto_TagWeight_3,
                             &b_lep_promptLeptonVeto_TagWeight_3);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoBARR_TagWeight_0",
                             &lep_promptLeptonImprovedVetoBARR_TagWeight_0,
                             &b_lep_promptLeptonImprovedVetoBARR_TagWeight_0);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoBARR_TagWeight_1",
                             &lep_promptLeptonImprovedVetoBARR_TagWeight_1,
                             &b_lep_promptLeptonImprovedVetoBARR_TagWeight_1);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoBARR_TagWeight_2",
                             &lep_promptLeptonImprovedVetoBARR_TagWeight_2,
                             &b_lep_promptLeptonImprovedVetoBARR_TagWeight_2);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoBARR_TagWeight_3",
                             &lep_promptLeptonImprovedVetoBARR_TagWeight_3,
                             &b_lep_promptLeptonImprovedVetoBARR_TagWeight_3);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoECAP_TagWeight_0",
                             &lep_promptLeptonImprovedVetoECAP_TagWeight_0,
                             &b_lep_promptLeptonImprovedVetoECAP_TagWeight_0);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoECAP_TagWeight_1",
                             &lep_promptLeptonImprovedVetoECAP_TagWeight_1,
                             &b_lep_promptLeptonImprovedVetoECAP_TagWeight_1);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoECAP_TagWeight_2",
                             &lep_promptLeptonImprovedVetoECAP_TagWeight_2,
                             &b_lep_promptLeptonImprovedVetoECAP_TagWeight_2);
    fChain->SetBranchAddress("lep_promptLeptonImprovedVetoECAP_TagWeight_3",
                             &lep_promptLeptonImprovedVetoECAP_TagWeight_3,
                             &b_lep_promptLeptonImprovedVetoECAP_TagWeight_3);
    fChain->SetBranchAddress("lep_SF_El_Reco_AT_0", &lep_SF_El_Reco_AT_0, &b_lep_SF_El_Reco_AT_0);
    fChain->SetBranchAddress("lep_SF_El_Reco_AT_1", &lep_SF_El_Reco_AT_1, &b_lep_SF_El_Reco_AT_1);
    fChain->SetBranchAddress("lep_SF_El_Reco_AT_2", &lep_SF_El_Reco_AT_2, &b_lep_SF_El_Reco_AT_2);
    fChain->SetBranchAddress("lep_SF_El_Reco_AT_3", &lep_SF_El_Reco_AT_3, &b_lep_SF_El_Reco_AT_3);
    fChain->SetBranchAddress("lep_SF_El_ID_LooseAndBLayerLH_AT_0", &lep_SF_El_ID_LooseAndBLayerLH_AT_0,
                             &b_lep_SF_El_ID_LooseAndBLayerLH_AT_0);
    fChain->SetBranchAddress("lep_SF_El_ID_LooseAndBLayerLH_AT_1", &lep_SF_El_ID_LooseAndBLayerLH_AT_1,
                             &b_lep_SF_El_ID_LooseAndBLayerLH_AT_1);
    fChain->SetBranchAddress("lep_SF_El_ID_LooseAndBLayerLH_AT_2", &lep_SF_El_ID_LooseAndBLayerLH_AT_2,
                             &b_lep_SF_El_ID_LooseAndBLayerLH_AT_2);
    fChain->SetBranchAddress("lep_SF_El_ID_LooseAndBLayerLH_AT_3", &lep_SF_El_ID_LooseAndBLayerLH_AT_3,
                             &b_lep_SF_El_ID_LooseAndBLayerLH_AT_3);
    fChain->SetBranchAddress("lep_SF_El_ID_TightLH_AT_0", &lep_SF_El_ID_TightLH_AT_0, &b_lep_SF_El_ID_TightLH_AT_0);
    fChain->SetBranchAddress("lep_SF_El_ID_TightLH_AT_1", &lep_SF_El_ID_TightLH_AT_1, &b_lep_SF_El_ID_TightLH_AT_1);
    fChain->SetBranchAddress("lep_SF_El_ID_TightLH_AT_2", &lep_SF_El_ID_TightLH_AT_2, &b_lep_SF_El_ID_TightLH_AT_2);
    fChain->SetBranchAddress("lep_SF_El_ID_TightLH_AT_3", &lep_SF_El_ID_TightLH_AT_3, &b_lep_SF_El_ID_TightLH_AT_3);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_AT_0", &lep_SF_El_Iso_FCLoose_AT_0, &b_lep_SF_El_Iso_FCLoose_AT_0);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_AT_1", &lep_SF_El_Iso_FCLoose_AT_1, &b_lep_SF_El_Iso_FCLoose_AT_1);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_AT_2", &lep_SF_El_Iso_FCLoose_AT_2, &b_lep_SF_El_Iso_FCLoose_AT_2);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_AT_3", &lep_SF_El_Iso_FCLoose_AT_3, &b_lep_SF_El_Iso_FCLoose_AT_3);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0",
                             &lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0,
                             &b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1",
                             &lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1,
                             &b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2",
                             &lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2,
                             &b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_3",
                             &lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_3,
                             &b_lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_3);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_0", &lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_0,
                             &b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_0);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_1", &lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_1,
                             &b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_1);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_2", &lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_2,
                             &b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_2);
    fChain->SetBranchAddress("lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_3", &lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_3,
                             &b_lep_SF_El_ChargeMisID_TightLH_FCLoose_AT_3);
    fChain->SetBranchAddress("lep_SF_El_FCLooseTightLHQMisID_0", &lep_SF_El_FCLooseTightLHQMisID_0,
                             &b_lep_SF_El_FCLooseTightLHQMisID_0);
    fChain->SetBranchAddress("lep_SF_El_FCLooseTightLHQMisID_1", &lep_SF_El_FCLooseTightLHQMisID_1,
                             &b_lep_SF_El_FCLooseTightLHQMisID_1);
    fChain->SetBranchAddress("lep_SF_El_FCLooseTightLHQMisID_2", &lep_SF_El_FCLooseTightLHQMisID_2,
                             &b_lep_SF_El_FCLooseTightLHQMisID_2);
    fChain->SetBranchAddress("lep_SF_El_FCLooseTightLHQMisID_3", &lep_SF_El_FCLooseTightLHQMisID_3,
                             &b_lep_SF_El_FCLooseTightLHQMisID_3);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_0", &lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_0,
                             &b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_0);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1", &lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1,
                             &b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_2", &lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_2,
                             &b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_2);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_3", &lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_3,
                             &b_lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_3);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_TightLH_0", &lep_SF_El_Iso_FCLoose_TightLH_0,
                             &b_lep_SF_El_Iso_FCLoose_TightLH_0);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_TightLH_1", &lep_SF_El_Iso_FCLoose_TightLH_1,
                             &b_lep_SF_El_Iso_FCLoose_TightLH_1);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_TightLH_2", &lep_SF_El_Iso_FCLoose_TightLH_2,
                             &b_lep_SF_El_Iso_FCLoose_TightLH_2);
    fChain->SetBranchAddress("lep_SF_El_Iso_FCLoose_TightLH_3", &lep_SF_El_Iso_FCLoose_TightLH_3,
                             &b_lep_SF_El_Iso_FCLoose_TightLH_3);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_QmisID_0", &lep_SF_El_PLVTight_QmisID_0,
                             &b_lep_SF_El_PLVTight_QmisID_0);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_QmisID_1", &lep_SF_El_PLVTight_QmisID_1,
                             &b_lep_SF_El_PLVTight_QmisID_1);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_QmisID_2", &lep_SF_El_PLVTight_QmisID_2,
                             &b_lep_SF_El_PLVTight_QmisID_2);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_QmisID_3", &lep_SF_El_PLVTight_QmisID_3,
                             &b_lep_SF_El_PLVTight_QmisID_3);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_0", &lep_SF_El_PLVTight_0, &b_lep_SF_El_PLVTight_0);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_1", &lep_SF_El_PLVTight_1, &b_lep_SF_El_PLVTight_1);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_2", &lep_SF_El_PLVTight_2, &b_lep_SF_El_PLVTight_2);
    fChain->SetBranchAddress("lep_SF_El_PLVTight_3", &lep_SF_El_PLVTight_3, &b_lep_SF_El_PLVTight_3);
    fChain->SetBranchAddress("lep_SF_El_PLVLoose_0", &lep_SF_El_PLVLoose_0, &b_lep_SF_El_PLVLoose_0);
    fChain->SetBranchAddress("lep_SF_El_PLVLoose_1", &lep_SF_El_PLVLoose_1, &b_lep_SF_El_PLVLoose_1);
    fChain->SetBranchAddress("lep_SF_El_PLVLoose_2", &lep_SF_El_PLVLoose_2, &b_lep_SF_El_PLVLoose_2);
    fChain->SetBranchAddress("lep_SF_El_PLVLoose_3", &lep_SF_El_PLVLoose_3, &b_lep_SF_El_PLVLoose_3);
    fChain->SetBranchAddress("lep_SF_El_ID_MediumLH_0", &lep_SF_El_ID_MediumLH_0, &b_lep_SF_El_ID_MediumLH_0);
    fChain->SetBranchAddress("lep_SF_El_ID_MediumLH_1", &lep_SF_El_ID_MediumLH_1, &b_lep_SF_El_ID_MediumLH_1);
    fChain->SetBranchAddress("lep_SF_El_ID_MediumLH_2", &lep_SF_El_ID_MediumLH_2, &b_lep_SF_El_ID_MediumLH_2);
    fChain->SetBranchAddress("lep_SF_El_ID_MediumLH_3", &lep_SF_El_ID_MediumLH_3, &b_lep_SF_El_ID_MediumLH_3);
    fChain->SetBranchAddress("lep_SF_Mu_TTVA_AT_0", &lep_SF_Mu_TTVA_AT_0, &b_lep_SF_Mu_TTVA_AT_0);
    fChain->SetBranchAddress("lep_SF_Mu_TTVA_AT_1", &lep_SF_Mu_TTVA_AT_1, &b_lep_SF_Mu_TTVA_AT_1);
    fChain->SetBranchAddress("lep_SF_Mu_TTVA_AT_2", &lep_SF_Mu_TTVA_AT_2, &b_lep_SF_Mu_TTVA_AT_2);
    fChain->SetBranchAddress("lep_SF_Mu_TTVA_AT_3", &lep_SF_Mu_TTVA_AT_3, &b_lep_SF_Mu_TTVA_AT_3);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Loose_AT_0", &lep_SF_Mu_ID_Loose_AT_0, &b_lep_SF_Mu_ID_Loose_AT_0);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Loose_AT_1", &lep_SF_Mu_ID_Loose_AT_1, &b_lep_SF_Mu_ID_Loose_AT_1);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Loose_AT_2", &lep_SF_Mu_ID_Loose_AT_2, &b_lep_SF_Mu_ID_Loose_AT_2);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Loose_AT_3", &lep_SF_Mu_ID_Loose_AT_3, &b_lep_SF_Mu_ID_Loose_AT_3);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Medium_AT_0", &lep_SF_Mu_ID_Medium_AT_0, &b_lep_SF_Mu_ID_Medium_AT_0);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Medium_AT_1", &lep_SF_Mu_ID_Medium_AT_1, &b_lep_SF_Mu_ID_Medium_AT_1);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Medium_AT_2", &lep_SF_Mu_ID_Medium_AT_2, &b_lep_SF_Mu_ID_Medium_AT_2);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Medium_AT_3", &lep_SF_Mu_ID_Medium_AT_3, &b_lep_SF_Mu_ID_Medium_AT_3);
    fChain->SetBranchAddress("lep_SF_Mu_Iso_FCLoose_AT_0", &lep_SF_Mu_Iso_FCLoose_AT_0, &b_lep_SF_Mu_Iso_FCLoose_AT_0);
    fChain->SetBranchAddress("lep_SF_Mu_Iso_FCLoose_AT_1", &lep_SF_Mu_Iso_FCLoose_AT_1, &b_lep_SF_Mu_Iso_FCLoose_AT_1);
    fChain->SetBranchAddress("lep_SF_Mu_Iso_FCLoose_AT_2", &lep_SF_Mu_Iso_FCLoose_AT_2, &b_lep_SF_Mu_Iso_FCLoose_AT_2);
    fChain->SetBranchAddress("lep_SF_Mu_Iso_FCLoose_AT_3", &lep_SF_Mu_Iso_FCLoose_AT_3, &b_lep_SF_Mu_Iso_FCLoose_AT_3);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Tight_0", &lep_SF_Mu_ID_Tight_0, &b_lep_SF_Mu_ID_Tight_0);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Tight_1", &lep_SF_Mu_ID_Tight_1, &b_lep_SF_Mu_ID_Tight_1);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Tight_2", &lep_SF_Mu_ID_Tight_2, &b_lep_SF_Mu_ID_Tight_2);
    fChain->SetBranchAddress("lep_SF_Mu_ID_Tight_3", &lep_SF_Mu_ID_Tight_3, &b_lep_SF_Mu_ID_Tight_3);
    fChain->SetBranchAddress("lep_SF_Mu_PLVTight_0", &lep_SF_Mu_PLVTight_0, &b_lep_SF_Mu_PLVTight_0);
    fChain->SetBranchAddress("lep_SF_Mu_PLVTight_1", &lep_SF_Mu_PLVTight_1, &b_lep_SF_Mu_PLVTight_1);
    fChain->SetBranchAddress("lep_SF_Mu_PLVTight_2", &lep_SF_Mu_PLVTight_2, &b_lep_SF_Mu_PLVTight_2);
    fChain->SetBranchAddress("lep_SF_Mu_PLVTight_3", &lep_SF_Mu_PLVTight_3, &b_lep_SF_Mu_PLVTight_3);
    fChain->SetBranchAddress("lep_SF_Mu_PLVLoose_0", &lep_SF_Mu_PLVLoose_0, &b_lep_SF_Mu_PLVLoose_0);
    fChain->SetBranchAddress("lep_SF_Mu_PLVLoose_1", &lep_SF_Mu_PLVLoose_1, &b_lep_SF_Mu_PLVLoose_1);
    fChain->SetBranchAddress("lep_SF_Mu_PLVLoose_2", &lep_SF_Mu_PLVLoose_2, &b_lep_SF_Mu_PLVLoose_2);
    fChain->SetBranchAddress("lep_SF_Mu_PLVLoose_3", &lep_SF_Mu_PLVLoose_3, &b_lep_SF_Mu_PLVLoose_3);
    fChain->SetBranchAddress("lep_SF_CombinedLoose_0", &lep_SF_CombinedLoose_0, &b_lep_SF_CombinedLoose_0);
    fChain->SetBranchAddress("lep_SF_CombinedLoose_1", &lep_SF_CombinedLoose_1, &b_lep_SF_CombinedLoose_1);
    fChain->SetBranchAddress("lep_SF_CombinedLoose_2", &lep_SF_CombinedLoose_2, &b_lep_SF_CombinedLoose_2);
    fChain->SetBranchAddress("lep_SF_CombinedLoose_3", &lep_SF_CombinedLoose_3, &b_lep_SF_CombinedLoose_3);
    fChain->SetBranchAddress("lep_SF_CombinedTight_0", &lep_SF_CombinedTight_0, &b_lep_SF_CombinedTight_0);
    fChain->SetBranchAddress("lep_SF_CombinedTight_1", &lep_SF_CombinedTight_1, &b_lep_SF_CombinedTight_1);
    fChain->SetBranchAddress("lep_SF_CombinedTight_2", &lep_SF_CombinedTight_2, &b_lep_SF_CombinedTight_2);
    fChain->SetBranchAddress("lep_SF_CombinedTight_3", &lep_SF_CombinedTight_3, &b_lep_SF_CombinedTight_3);
//    fChain->SetBranchAddress("tau_pt_0", &tau_pt_0, &b_tau_pt_0);
//    fChain->SetBranchAddress("tau_pt_1", &tau_pt_1, &b_tau_pt_1);
    fChain->SetBranchAddress("tau_E_0", &tau_E_0, &b_tau_E_0);
    fChain->SetBranchAddress("tau_E_1", &tau_E_1, &b_tau_E_1);
    fChain->SetBranchAddress("tau_numTrack_0", &tau_numTrack_0, &b_tau_numTrack_0);
    fChain->SetBranchAddress("tau_numTrack_1", &tau_numTrack_1, &b_tau_numTrack_1);
//    fChain->SetBranchAddress("tau_passEleOLR_0", &tau_passEleOLR_0, &b_tau_passEleOLR_0);
//    fChain->SetBranchAddress("tau_passEleOLR_1", &tau_passEleOLR_1, &b_tau_passEleOLR_1);
//    fChain->SetBranchAddress("tau_passMuonOLR_0", &tau_passMuonOLR_0, &b_tau_passMuonOLR_0);
//    fChain->SetBranchAddress("tau_passMuonOLR_1", &tau_passMuonOLR_1, &b_tau_passMuonOLR_1);
//    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_0", &lep_promptLeptonVeto_TagWeight_0, &b_lep_promptLeptonVeto_TagWeight_0);
//    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_1", &lep_promptLeptonVeto_TagWeight_1, &b_lep_promptLeptonVeto_TagWeight_1);
//    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_2", &lep_promptLeptonVeto_TagWeight_2, &b_lep_promptLeptonVeto_TagWeight_2);
//    fChain->SetBranchAddress("lep_promptLeptonVeto_TagWeight_3", &lep_promptLeptonVeto_TagWeight_3, &b_lep_promptLeptonVeto_TagWeight_3);
    if (!is_data) {
        fChain->SetBranchAddress("totalEventsWeighted", &totalEventsWeighted, &b_totalEventsWeighted);
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
        fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
        fChain->SetBranchAddress("mc_genWeights", &mc_genWeights, &b_mc_genWeights);
        fChain->SetBranchAddress("bTagSF_weight_DL1r_70", &bTagSF_weight_DL1r_70, &b_bTagSF_weight_DL1r_70);
        fChain->SetBranchAddress("bTagSF_weight_DL1r_77", &bTagSF_weight_DL1r_77, &b_bTagSF_weight_DL1r_77);
        fChain->SetBranchAddress("bTagSF_weight_DL1r_85", &bTagSF_weight_DL1r_85, &b_bTagSF_weight_DL1r_85);
        fChain->SetBranchAddress("jvtSF_customOR", &jvtSF_customOR, &b_jvtSF_customOR);
    }
    fChain->SetBranchAddress("lepSFObjLoose", &lepSFObjLoose, &b_lepSFObjLoose);
    fChain->SetBranchAddress("lepSFObjTight", &lepSFObjTight, &b_lepSFObjTight);
    fChain->SetBranchAddress("lepSFObjChannelCombined", &lepSFObjChannelCombined, &b_lepSFObjChannelCombined);
    fChain->SetBranchAddress("custTrigSF_LooseID_FCLooseIso_SLTorDLT", &custTrigSF_LooseID_FCLooseIso_SLTorDLT,
                             &b_custTrigSF_LooseID_FCLooseIso_SLTorDLT);
    fChain->SetBranchAddress("custTrigSF_LooseID_FCLooseIso_SLT", &custTrigSF_LooseID_FCLooseIso_SLT,
                             &b_custTrigSF_LooseID_FCLooseIso_SLT);
    fChain->SetBranchAddress("custTrigSF_LooseID_FCLooseIso_DLT", &custTrigSF_LooseID_FCLooseIso_DLT,
                             &b_custTrigSF_LooseID_FCLooseIso_DLT);
    fChain->SetBranchAddress("custTrigMatch_LooseID_FCLooseIso_SLTorDLT", &custTrigMatch_LooseID_FCLooseIso_SLTorDLT,
                             &b_custTrigMatch_LooseID_FCLooseIso_SLTorDLT);
    fChain->SetBranchAddress("custTrigMatch_LooseID_FCLooseIso_SLT", &custTrigMatch_LooseID_FCLooseIso_SLT,
                             &b_custTrigMatch_LooseID_FCLooseIso_SLT);
    fChain->SetBranchAddress("custTrigMatch_LooseID_FCLooseIso_DLT", &custTrigMatch_LooseID_FCLooseIso_DLT,
                             &b_custTrigMatch_LooseID_FCLooseIso_DLT);
    fChain->SetBranchAddress("custTrigSF_TightElMediumMuID_FCLooseIso_SLTorDLT",
                             &custTrigSF_TightElMediumMuID_FCLooseIso_SLTorDLT,
                             &b_custTrigSF_TightElMediumMuID_FCLooseIso_SLTorDLT);
    fChain->SetBranchAddress("custTrigSF_TightElMediumMuID_FCLooseIso_SLT",
                             &custTrigSF_TightElMediumMuID_FCLooseIso_SLT,
                             &b_custTrigSF_TightElMediumMuID_FCLooseIso_SLT);
    fChain->SetBranchAddress("custTrigSF_TightElMediumMuID_FCLooseIso_DLT",
                             &custTrigSF_TightElMediumMuID_FCLooseIso_DLT,
                             &b_custTrigSF_TightElMediumMuID_FCLooseIso_DLT);
    fChain->SetBranchAddress("custTrigMatch_TightElMediumMuID_FCLooseIso_SLTorDLT",
                             &custTrigMatch_TightElMediumMuID_FCLooseIso_SLTorDLT,
                             &b_custTrigMatch_TightElMediumMuID_FCLooseIso_SLTorDLT);
    fChain->SetBranchAddress("custTrigMatch_TightElMediumMuID_FCLooseIso_SLT",
                             &custTrigMatch_TightElMediumMuID_FCLooseIso_SLT,
                             &b_custTrigMatch_TightElMediumMuID_FCLooseIso_SLT);
    fChain->SetBranchAddress("custTrigMatch_TightElMediumMuID_FCLooseIso_DLT",
                             &custTrigMatch_TightElMediumMuID_FCLooseIso_DLT,
                             &b_custTrigMatch_TightElMediumMuID_FCLooseIso_DLT);
    fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
    fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
    if (!is_data) {
        fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
    }
    fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
    fChain->SetBranchAddress("mu", &mu, &b_mu);
    fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
    fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
    fChain->SetBranchAddress("GlobalTrigDecision", &GlobalTrigDecision, &b_GlobalTrigDecision);
    fChain->SetBranchAddress("GlobalTrigDecision_mu26_ivarmedium", &GlobalTrigDecision_mu26_ivarmedium,
                             &b_GlobalTrigDecision_mu26_ivarmedium);
    fChain->SetBranchAddress("GlobalTrigDecision_mu22_mu8noL1", &GlobalTrigDecision_mu22_mu8noL1,
                             &b_GlobalTrigDecision_mu22_mu8noL1);
    fChain->SetBranchAddress("GlobalTrigDecision_e60_lhmedium_nod0", &GlobalTrigDecision_e60_lhmedium_nod0,
                             &b_GlobalTrigDecision_e60_lhmedium_nod0);
    fChain->SetBranchAddress("GlobalTrigDecision_mu18_mu8noL1", &GlobalTrigDecision_mu18_mu8noL1,
                             &b_GlobalTrigDecision_mu18_mu8noL1);
    fChain->SetBranchAddress("GlobalTrigDecision_e26_lhtight_nod0_ivarloose",
                             &GlobalTrigDecision_e26_lhtight_nod0_ivarloose,
                             &b_GlobalTrigDecision_e26_lhtight_nod0_ivarloose);
    fChain->SetBranchAddress("GlobalTrigDecision_mu20_iloose_L1MU15", &GlobalTrigDecision_mu20_iloose_L1MU15,
                             &b_GlobalTrigDecision_mu20_iloose_L1MU15);
    fChain->SetBranchAddress("GlobalTrigDecision_e24_lhmedium_L1EM20VH", &GlobalTrigDecision_e24_lhmedium_L1EM20VH,
                             &b_GlobalTrigDecision_e24_lhmedium_L1EM20VH);
    fChain->SetBranchAddress("GlobalTrigDecision_mu50", &GlobalTrigDecision_mu50, &b_GlobalTrigDecision_mu50);
    fChain->SetBranchAddress("GlobalTrigDecision_e17_lhloose_nod0_mu14", &GlobalTrigDecision_e17_lhloose_nod0_mu14,
                             &b_GlobalTrigDecision_e17_lhloose_nod0_mu14);
    fChain->SetBranchAddress("GlobalTrigDecision_2e12_lhloose_L12EM10VH", &GlobalTrigDecision_2e12_lhloose_L12EM10VH,
                             &b_GlobalTrigDecision_2e12_lhloose_L12EM10VH);
    fChain->SetBranchAddress("GlobalTrigDecision_e120_lhloose", &GlobalTrigDecision_e120_lhloose,
                             &b_GlobalTrigDecision_e120_lhloose);
    fChain->SetBranchAddress("GlobalTrigDecision_e140_lhloose_nod0", &GlobalTrigDecision_e140_lhloose_nod0,
                             &b_GlobalTrigDecision_e140_lhloose_nod0);
    fChain->SetBranchAddress("GlobalTrigDecision_e60_lhmedium", &GlobalTrigDecision_e60_lhmedium,
                             &b_GlobalTrigDecision_e60_lhmedium);
    fChain->SetBranchAddress("GlobalTrigDecision_2e17_lhvloose_nod0", &GlobalTrigDecision_2e17_lhvloose_nod0,
                             &b_GlobalTrigDecision_2e17_lhvloose_nod0);
    fChain->SetBranchAddress("GlobalTrigDecision_e17_lhloose_mu14", &GlobalTrigDecision_e17_lhloose_mu14,
                             &b_GlobalTrigDecision_e17_lhloose_mu14);
    fChain->SetBranchAddress("custTrigSF_AT_default_preOR", &custTrigSF_AT_default_preOR,
                             &b_custTrigSF_AT_default_preOR);
    fChain->SetBranchAddress("RunYear", &RunYear, &b_RunYear);
    fChain->SetBranchAddress("mc_xSection", &mc_xSection, &b_mc_xSection);
    fChain->SetBranchAddress("mc_rawXSection", &mc_rawXSection, &b_mc_rawXSection);
    fChain->SetBranchAddress("mc_kFactor", &mc_kFactor, &b_mc_kFactor);
    fChain->SetBranchAddress("onelep_type", &onelep_type, &b_onelep_type);
    fChain->SetBranchAddress("dilep_type", &dilep_type, &b_dilep_type);
    fChain->SetBranchAddress("trilep_type", &trilep_type, &b_trilep_type);
    fChain->SetBranchAddress("quadlep_type", &quadlep_type, &b_quadlep_type);
    fChain->SetBranchAddress("total_charge", &total_charge, &b_total_charge);
    fChain->SetBranchAddress("total_leptons", &total_leptons, &b_total_leptons);
    fChain->SetBranchAddress("isQMisIDEvent", &isQMisIDEvent, &b_isQMisIDEvent);
    fChain->SetBranchAddress("isFakeEvent", &isFakeEvent, &b_isFakeEvent);
    fChain->SetBranchAddress("isLepFromPhEvent", &isLepFromPhEvent, &b_isLepFromPhEvent);
    fChain->SetBranchAddress("Mll01", &Mll01, &b_Mll01);
    fChain->SetBranchAddress("Mll02", &Mll02, &b_Mll02);
    fChain->SetBranchAddress("Mll12", &Mll12, &b_Mll12);
    fChain->SetBranchAddress("Mlll012", &Mlll012, &b_Mlll012);
    fChain->SetBranchAddress("Ptll01", &Ptll01, &b_Ptll01);
    fChain->SetBranchAddress("Ptll02", &Ptll02, &b_Ptll02);
    fChain->SetBranchAddress("Ptll12", &Ptll12, &b_Ptll12);
    fChain->SetBranchAddress("DRll01", &DRll01, &b_DRll01);
    fChain->SetBranchAddress("DRll02", &DRll02, &b_DRll02);
    fChain->SetBranchAddress("DRll12", &DRll12, &b_DRll12);
    fChain->SetBranchAddress("DRl0jmin", &DRl0jmin, &b_DRl0jmin);
    fChain->SetBranchAddress("DRl1jmin", &DRl1jmin, &b_DRl1jmin);
    fChain->SetBranchAddress("DRl0j0", &DRl0j0, &b_DRl0j0);
    fChain->SetBranchAddress("DRl0j1", &DRl0j1, &b_DRl0j1);
    fChain->SetBranchAddress("DRl1j0", &DRl1j0, &b_DRl1j0);
    fChain->SetBranchAddress("DRl1j1", &DRl1j1, &b_DRl1j1);
    fChain->SetBranchAddress("DRl2j0", &DRl2j0, &b_DRl2j0);
    fChain->SetBranchAddress("DRl2j1", &DRl2j1, &b_DRl2j1);
    fChain->SetBranchAddress("DRj0j1", &DRj0j1, &b_DRj0j1);
    fChain->SetBranchAddress("DRjjMax", &DRjjMax, &b_DRjjMax);
    fChain->SetBranchAddress("mj0j1", &mj0j1, &b_mj0j1);
    fChain->SetBranchAddress("ml0j0", &ml0j0, &b_ml0j0);
    fChain->SetBranchAddress("ml0j1", &ml0j1, &b_ml0j1);
    fChain->SetBranchAddress("ml1j0", &ml1j0, &b_ml1j0);
    fChain->SetBranchAddress("ml1j1", &ml1j1, &b_ml1j1);
    fChain->SetBranchAddress("ml2j0", &ml2j0, &b_ml2j0);
    fChain->SetBranchAddress("ml2j1", &ml2j1, &b_ml2j1);
    fChain->SetBranchAddress("matchDLTll01", &matchDLTll01, &b_matchDLTll01);
    fChain->SetBranchAddress("best_Z_Mll", &best_Z_Mll, &b_best_Z_Mll);
    fChain->SetBranchAddress("best_Z_other_MtLepMet", &best_Z_other_MtLepMet, &b_best_Z_other_MtLepMet);
    fChain->SetBranchAddress("best_Z_other_Mll", &best_Z_other_Mll, &b_best_Z_other_Mll);
    fChain->SetBranchAddress("nJets_OR", &nJets_OR, &b_nJets_OR);
    fChain->SetBranchAddress("nTruthJets", &nTruthJets, &b_nTruthJets);
    fChain->SetBranchAddress("nTruthJets_OR", &nTruthJets_OR, &b_nTruthJets_OR);
    fChain->SetBranchAddress("nJets_OR_DL1r_85", &nJets_OR_DL1r_85, &b_nJets_OR_DL1r_85);
    fChain->SetBranchAddress("nJets_OR_DL1r_77", &nJets_OR_DL1r_77, &b_nJets_OR_DL1r_77);
    fChain->SetBranchAddress("nJets_OR_DL1r_70", &nJets_OR_DL1r_70, &b_nJets_OR_DL1r_70);
    fChain->SetBranchAddress("nTaus_OR_Pt25", &nTaus_OR_Pt25, &b_nTaus_OR_Pt25);
    fChain->SetBranchAddress("nTaus_OR_Pt25_RNN", &nTaus_OR_Pt25_RNN, &b_nTaus_OR_Pt25_RNN);
    fChain->SetBranchAddress("HT", &HT, &b_HT);
    fChain->SetBranchAddress("HT_lep", &HT_lep, &b_HT_lep);
    fChain->SetBranchAddress("HT_jets", &HT_jets, &b_HT_jets);
    fChain->SetBranchAddress("lead_jetPt", &lead_jetPt, &b_lead_jetPt);
    fChain->SetBranchAddress("lead_jetEta", &lead_jetEta, &b_lead_jetEta);
    fChain->SetBranchAddress("lead_jetPhi", &lead_jetPhi, &b_lead_jetPhi);
    fChain->SetBranchAddress("lead_jetE", &lead_jetE, &b_lead_jetE);
    fChain->SetBranchAddress("sublead_jetPt", &sublead_jetPt, &b_sublead_jetPt);
    fChain->SetBranchAddress("sublead_jetEta", &sublead_jetEta, &b_sublead_jetEta);
    fChain->SetBranchAddress("sublead_jetPhi", &sublead_jetPhi, &b_sublead_jetPhi);
    fChain->SetBranchAddress("sublead_jetE", &sublead_jetE, &b_sublead_jetE);
    fChain->SetBranchAddress("higgs1Mode", &higgs1Mode, &b_higgs1Mode);
    fChain->SetBranchAddress("higgs2Mode", &higgs2Mode, &b_higgs2Mode);
    fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
    fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
    fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
    fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
    Notify();
}

Bool_t nominal::Notify() {
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.

    return kTRUE;
}

void nominal::Show(Long64_t entry) {
// Print contents of entry.
// If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}

Int_t nominal::Cut(Long64_t entry) {
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
    return 1;
}

#endif // #ifdef nominal_cxx
