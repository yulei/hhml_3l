//
// Created by Zhang Yulei on 7/6/21.
//

#ifndef HHML_3L_NTUPLE_ANALYSIS_H
#define HHML_3L_NTUPLE_ANALYSIS_H

#include "nominal.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TChain.h"

#include <map>
#include <vector>

using std::map, std::vector, std::pair, std::tuple;

struct Class_Region {
    bool external_conversion = false;
    bool internal_conversion = false;
    bool hf_e = false;
    bool hf_m = false;
    bool lf_e = false;
};

struct Sample_Truth_Type {
    bool prompt = false;
    bool external_conversion = false;
    bool internal_conversion = false;
    bool hf_e = false;
    bool hf_m = false;
    bool lf_e = false;
    bool lf_m = false;
    bool QMisID = false;
    bool others = false;
};

enum class Sample_Type {
    None, DATA, PROMPT, MCFAKE, FAKE_DATA, FAKE_MC
};

enum class WorkPoint {
    None, Loose, Tight
};

enum class Isolation_Type {
    PLV, FCLoose
};

class nTuple_Analysis {
public:

    nTuple_Analysis();

    ~nTuple_Analysis();

    void read_tree(const vector<TString>& dir, const TString & tree_name, const TString &Sample_Name, Sample_Type st, double sw);

    [[nodiscard]] nominal *getTree() const {
        return tl;
    }

    void setTree(nominal *Tree) {
        tl = Tree;
    }

    void Book_Tree(const TString &file_name, const TString &tree_name);

    void Save_Tree();

    void Loop_Tree();

    bool quality_cut(int id, WorkPoint wp_id, WorkPoint wp_iso, Isolation_Type it = Isolation_Type::PLV);

    void convert_wp();

    void convert_original_branch();

    bool chargeIDBDT_cut();

    bool dz_cut();

    bool cut_flow(const vector<int>& skipped = vector<int>(), bool fill_hist = false);

    tuple<bool, double> fake_analysis(bool lq1, bool lq2);

    Class_Region template_fit_region(bool syst = false);

    [[nodiscard]] tuple<bool, double> template_fit_apply() const;

    Sample_Truth_Type truth_region_analysis();

    bool anti_tight_cut(bool lq1, bool lq2);

    double GetHiggsness();

private:
    nominal *tl{};
    TString sample_name;
    Sample_Type sample_type{Sample_Type::None};

    TFile *f_out{};
    TTree *t_out{};
    TChain* tc{};

    int verbose = 1;

    float unit_conv = 1e-3; // convert MeV to GeV

public:
    // newly defined variables
    TH1F *cutflow{};
    TH1F *cutflow_w{};

    vector<TString> cut_name;

    ULong64_t EvtNum{};
    Int_t Higgs1Mode{};
    Int_t Higgs2Mode{};
    Int_t FlavorCategory{};
    Int_t FakeCategory{};
    Int_t totalCharge{};
    Int_t nJets{};
    Int_t nbJets{};
    Int_t SampleType{}; // 0: Data; 1: Prompt; 2: MC Fakes

    double sample_weight{};
    Bool_t Evt_SR{};
    Bool_t Evt_Fake{};
    Bool_t Evt_WZ_CR{};

    // For Template Fit
    Bool_t Evt_TF_ExtConv_e{};
    Bool_t Evt_TF_IntConv_e{};
    Bool_t Evt_TF_HF_e{};
    Bool_t Evt_TF_HF_m{};
    Bool_t Evt_TF_LF_e{};
    Bool_t Evt_FF_CR_A{};
    Bool_t Evt_FF_CR_B{};
    Bool_t Evt_FF_CR_C{};
    Bool_t Evt_FF_CR_D{};
    // For truth origin classification
    Bool_t Class_ExtConv{};
    Bool_t Class_IntConv{};
    Bool_t Class_HF{};
    Bool_t Class_HF_m{};
    Bool_t Class_LF{};
    Bool_t Class_LF_m{};
    Bool_t Class_Others{};
    Bool_t Class_Prompt{};
    Bool_t Class_QMISID{};
    // For template fit systematics
    Bool_t Evt_TF_ExtConv_e_Syst{};
    Bool_t Evt_TF_IntConv_e_Syst{};
    Bool_t Evt_TF_HF_e_Syst{};
    Bool_t Evt_TF_HF_m_Syst{};
    Bool_t Evt_TF_LF_e_Syst{};

    Bool_t Evt_ID_Loose{};
    Bool_t Evt_ISO_Loose{};
    Bool_t Evt_ID_Tight{};
    Bool_t Evt_ISO_Tight{};
    Bool_t Evt_ChargeIDBDT{};
    Bool_t Evt_AuthorCut{};
    Bool_t Evt_Tight{};
    Bool_t Evt_AntiTight{};
    Bool_t Evt_Prompt{};
    Int_t Fake_Lep_idx{};
    Double_t _weight{};
    Double_t _weight_new{};
    Double_t MET_ET{};
    Double_t m_l1l2{};
    Double_t m_l1l3{};
    Double_t m_l2l3{};
    Double_t m_l1j{};
    Double_t m_l2j{};
    Double_t m_l3j{};
    Double_t m_l3jj{};
    Double_t m_lll{};
    Double_t m_llljj{};
    Double_t dR_l1l2{};
    Double_t dR_l1l3{};
    Double_t dR_l2l3{};
    Double_t dR_l1j{};
    Double_t dR_l2j{};
    Double_t dR_l3j{};

    Double_t m_l1j1{};
    Double_t m_l2j1{};
    Double_t m_l3j1{};
    Double_t m_l1j2{};
    Double_t m_l2j2{};
    Double_t m_l3j2{};

    Double_t dR_l1j1{};
    Double_t dR_l2j1{};
    Double_t dR_l3j1{};
    Double_t dR_l1j2{};
    Double_t dR_l2j2{};
    Double_t dR_l3j2{};

    Bool_t ZMassVeto{};
    Bool_t ZJetsVeto{};
    Bool_t MlllCut{};
    Bool_t loMassVeto{};

    double weight_l[3]{};
    double weight_fake{};
    double weight_chargeIDBDT{};

    Double_t lep_ID[3]{};
    Double_t lep_Pt[3]{};
    Double_t lep_Eta[3]{};
    Double_t lep_E[3]{};
    Double_t lep_Phi[3]{};
    Char_t lep_isPrompt[3]{};
    Int_t lep_truthType[3]{};
    Int_t lep_truthOrigin[3]{};
    Int_t lep_truthPDG[3]{};
    Int_t lep_truthParentPDG[3]{};
    Int_t lep_truthParentType[3]{};
    Int_t lep_truthParentOrigin[3]{};

    // jets
    Double_t leadJetPt{};
    Double_t leadJetEta{};
    Double_t leadJetPhi{};
    Double_t leadJetE{};
    Double_t subleadJetPt{};
    Double_t subleadJetEta{};
    Double_t subleadJetPhi{};
    Double_t subleadJetE{};

    // Overall variables
    Double_t best_Z_Mll{};
    Double_t HT{};
    Double_t HT_lep{};
    Double_t HT_jets{};
    Double_t best_Z_other_MtLepMet{};
    Double_t best_Z_other_Mll{};

    Double_t Higgsness{};

    // Systematics weight
    Bool_t Evt_SR_TF_HF_e{};
    Bool_t Evt_SR_TF_HF_m{};
    Bool_t Evt_SR_TF_ExtConv{};
    // fake factor errors for muon
    Double_t weight_FF_muStat_Bin1U{};
    Double_t weight_FF_muStat_Bin1D{};
    Double_t weight_FF_muStat_Bin2U{};
    Double_t weight_FF_muStat_Bin2D{};
    // fake factor errors for electron
    Double_t weight_FF_elStat_Bin1U{};
    Double_t weight_FF_elStat_Bin1D{};
    Double_t weight_FF_elStat_Bin2U{};
    Double_t weight_FF_elStat_Bin2D{};


    // For other usage
    double Z_Mass = 91.1876; // GeV
//    double Z_Mass = 91.2; // GeV

    // Structure (id, [loose, tight])
    map<int, vector<Char_t> > lep_id;
    map<int, map<Isolation_Type, vector<Int_t>>> lep_iso;
    map<int, vector<Float_t> > lep_id_weight;
    map<int, map<Isolation_Type, vector<Float_t>>> lep_iso_weight;
};

#endif // HHML_3L_NTUPLE_ANALYSIS_H
