//
// Created by Zhang Yulei on 7/6/21.
//

#include <Control.h>

#include "Control.h"

// Required by Singleton
Control *dControl = nullptr;

// Get Instance Class
Control *Control::CreateInstance() {
    if (dControl == nullptr)
        dControl = new Control();

    return dControl;
}

bool Control::ReadYAML(const TString &file_in) {
    auto Node = YAML::LoadFile(file_in.Data());

    auto s_node = Node["Samples"];
    auto g_node = Node["Groups"];
    auto c_node = Node["Cuts"];
    auto f_node = Node["FF"];
    auto t_node = Node["TF"];

    out_dir = Node["out_dir"].IsDefined() ? Node["out_dir"].as<std::string>() : ".";

    for (auto s: s_node) {
        auto s_t = s["type"].as<std::string>();
        Sample_Type st;
        if (s_t == "None") st = Sample_Type::None;
        if (s_t == "data") st = Sample_Type::DATA;
        if (s_t == "prompt") st = Sample_Type::PROMPT;
        if (s_t == "mcfake") st = Sample_Type::MCFAKE;
        samples.insert(pair(s["name"].as<std::string>(), st));
    }

    for (auto g: g_node) {
        map<TString, Sample_Type> tmp;
        for (auto comp: g["comp"]) {
            auto str = comp.as<std::string>();
            tmp.emplace(pair(str, samples.at(str)));
        }
        groups.emplace_back(tmp);
    }

    { // WZ normalization
        auto wz_node = Node["WZ_Scale"];
        for (auto nf: wz_node) nf_WZ.push_back(nf.as<double>());
    }

    for (auto lep: {"electron", "muon"}) { // FF node
        if (!f_node[lep].IsDefined()) continue;

        pT_range_1.insert({lep, {}});
        for (auto pt: f_node[lep]["pt_range_1"])
            pT_range_1.at(lep).push_back(pt.as<double>());
        pT_range_1.at(lep).push_back(DBL_MAX);

        ff_1.insert({lep, {}});
        for (auto ff: f_node[lep]["FF_1"]) ff_1.at(lep).push_back(ff.as<double>());
        assert(pT_range_1.at(lep).size() == ff_1.at(lep).size());

        pT_range_2.insert({lep, {}});
        for (auto pt: f_node[lep]["pt_range_2"])
            pT_range_2.at(lep).push_back(pt.as<double>());
        pT_range_2.at(lep).push_back(DBL_MAX);

        ff_2.insert({lep, {}});
        for (auto ff: f_node[lep]["FF_2"]) ff_2.at(lep).push_back(ff.as<double>());
        assert(pT_range_2.at(lep).size() == ff_2.at(lep).size());

        // add fake factor uncertainties
        ff_1_err.insert({lep, {}});
        if (auto ffn = f_node[lep]["FF_1_err"]; ffn.IsDefined()) {
            for (auto ff: ffn) ff_1_err.at(lep).push_back(ff.as<double>());
            assert(pT_range_1.at(lep).size() == ff_1_err.at(lep).size());
        }
        ff_2_err.insert({lep, {}});
        if (auto ffn = f_node[lep]["FF_2_err"]; ffn.IsDefined()) {
            for (auto ff: ffn) ff_2_err.at(lep).push_back(ff.as<double>());
            assert(pT_range_2.at(lep).size() == ff_2_err.at(lep).size());
        }
    }

    { // TF node
        NF_HF_e = t_node["NF_HF_e"].IsDefined() ? t_node["NF_HF_e"].as<double>() : 1.0;
        NF_MatConv_e = t_node["NF_MatConv_e"].IsDefined() ? t_node["NF_MatConv_e"].as<double>() : 1.0;
        NF_HF_m = (t_node["NF_HF_e"].IsDefined()) ? t_node["NF_HF_e"].as<double>() : 1.0;
    }

    { // Cuts Node
        N_bJets = c_node["N_bJets"].as<int>();
        N_Jets = c_node["N_Jets"].as<int>();

        ZMassVeto = c_node["ZMassVeto"].as<bool>();
        M_l_Cut = c_node["M_l_Cut"].as<bool>();
        Evt_Tight = c_node["Evt_Tight"].as<bool>();

        for (auto lq: c_node["lep_quality_0"]) lep_quality_0.push_back(lq.as<int>());
        for (auto lq: c_node["lep_quality_1"]) lep_quality_1.push_back(lq.as<int>());
        for (auto lq: c_node["lep_quality_2"]) lep_quality_2.push_back(lq.as<int>());
    }

    { // Higgsness
        if_Higgsness = Node["if_Higgsness"].IsDefined() && Node["if_Higgsness"].as<bool>();
    }


    return true;
}

Control::Control() = default;
