//
// Created by Zhang Yulei on 7/6/21.
//

#include "nTuple_Analysis.h"
#include "TArrow.h"
#include "TLorentzVector.h"
#include <iostream>

#include "Control.h"

#include "HiggsFcn.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnMigrad.h"
#include "Minuit2/MnMinos.h"
#include "Minuit2/MnContours.h"
#include "Minuit2/MnPlot.h"
#include "Minuit2/MnSimplex.h"

using std::cout, std::endl, std::cerr;

nTuple_Analysis::nTuple_Analysis() {
    // Initialize cut name
    cut_name.emplace_back("Trilep Type");
    cut_name.emplace_back("TotalCharge");
    cut_name.emplace_back("Trigger");
    cut_name.emplace_back("LepIDLoose");
    cut_name.emplace_back("LepIsoLoose");
    cut_name.emplace_back("TauVeto");
    cut_name.emplace_back("chargeID");
    cut_name.emplace_back("AuthorCut");
    cut_name.emplace_back("Extra pT");
    cut_name.emplace_back("nbJets");
    cut_name.emplace_back("LowMassVeto");
    cut_name.emplace_back("nJets");
    cut_name.emplace_back("ZMassVeto");
    cut_name.emplace_back("M_l Cut");
    cut_name.emplace_back("PLV_Tight");

    // Initialize Histogram
    cutflow = new TH1F("CutFlowHist", "CutFlowHist", 21, -1., 20.);
    cutflow_w = new TH1F("CutFlowHist_w", "CutFlowHist_w", 21, -1., 20.);
}

void nTuple_Analysis::Book_Tree(const TString &file_name, const TString &tree_name) {

    // Open a new root file on the disk
    f_out = new TFile(file_name, "RECREATE");

    t_out = new TTree(tree_name, "Analyzed 3-lepton");

    t_out->Branch("EvtNum", &EvtNum, "EvtNum/l");
    t_out->Branch("weight_old", &_weight, "weight_old/D");
    t_out->Branch("weight", &_weight_new, "weight/D");
    t_out->Branch("Evt_SR", &Evt_SR, "Evt_SR/O");
    t_out->Branch("Evt_Fake", &Evt_Fake, "Evt_Fake/O");
    t_out->Branch("Evt_WZ_CR", &Evt_WZ_CR, "Evt_WZ_CR/O");
    t_out->Branch("Evt_TF_ExtConv_e", &Evt_TF_ExtConv_e, "Evt_TF_ExtConv_e/O");
    t_out->Branch("Evt_TF_IntConv_e", &Evt_TF_IntConv_e, "Evt_TF_IntConv_e/O");
    t_out->Branch("Evt_TF_HF_e", &Evt_TF_HF_e, "Evt_TF_HF_e/O");
    t_out->Branch("Evt_TF_HF_m", &Evt_TF_HF_m, "Evt_TF_HF_m/O");
    t_out->Branch("Evt_TF_LF_e", &Evt_TF_LF_e, "Evt_TF_LF_e/O");
    t_out->Branch("Evt_TF_ExtConv_e_Syst", &Evt_TF_ExtConv_e_Syst, "Evt_TF_ExtConv_e_Syst/O");
    t_out->Branch("Evt_TF_IntConv_e_Syst", &Evt_TF_IntConv_e_Syst, "Evt_TF_IntConv_e_Syst/O");
    t_out->Branch("Evt_TF_HF_e_Syst", &Evt_TF_HF_e_Syst, "Evt_TF_HF_e_Syst/O");
    t_out->Branch("Evt_TF_HF_m_Syst", &Evt_TF_HF_m_Syst, "Evt_TF_HF_m_Syst/O");
    t_out->Branch("Evt_TF_LF_e_Syst", &Evt_TF_LF_e_Syst, "Evt_TF_LF_e_Syst/O");
    t_out->Branch("Evt_FF_CR_A", &Evt_FF_CR_A, "Evt_FF_CR_A/O");
    t_out->Branch("Evt_FF_CR_B", &Evt_FF_CR_B, "Evt_FF_CR_B/O");
    t_out->Branch("Evt_FF_CR_C", &Evt_FF_CR_C, "Evt_FF_CR_C/O");
    t_out->Branch("Evt_FF_CR_D", &Evt_FF_CR_D, "Evt_FF_CR_D/O");
    t_out->Branch("Class_Prompt", &Class_Prompt, "Class_Prompt/O");
    t_out->Branch("Class_ExtConv", &Class_ExtConv, "Class_ExtConv/O");
    t_out->Branch("Class_IntConv", &Class_IntConv, "Class_IntConv/O");
    t_out->Branch("Class_HF", &Class_HF, "Class_HF/O");
    t_out->Branch("Class_HF_m", &Class_HF_m, "Class_HF_m/O");
    t_out->Branch("Class_LF", &Class_LF, "Class_LF/O");
    t_out->Branch("Class_LF_m", &Class_LF_m, "Class_LF_m/O");
    t_out->Branch("Class_Others", &Class_Others, "Class_Others/O");
    t_out->Branch("Class_QMISID", &Class_QMISID, "Class_QMISID/O");

    t_out->Branch("Evt_Tight", &Evt_Tight, "Evt_Tight/O");
    t_out->Branch("Evt_AntiTight", &Evt_AntiTight, "Evt_AntiTight/O");
    t_out->Branch("Evt_Prompt", &Evt_Prompt, "Evt_Prompt/O");
    t_out->Branch("Higgs1Mode", &Higgs1Mode, "Higgs1Mode/I");
    t_out->Branch("Higgs2Mode", &Higgs2Mode, "Higgs2Mode/I");
    t_out->Branch("FlavorCategory", &FlavorCategory, "FlavorCategory/I");
    t_out->Branch("FakeCategory", &FakeCategory, "FakeCategory/I");
    t_out->Branch("SampleType", &SampleType, "SampleType/I");
    t_out->Branch("totalCharge", &totalCharge, "totalCharge/I");
    t_out->Branch("nJets", &nJets, "nJets/I");
    t_out->Branch("nbJets", &nbJets, "nbJets/I");
    t_out->Branch("Fake_Lep_idx", &Fake_Lep_idx, "Fake_Lep_idx/I");
    t_out->Branch("MET_ET", &MET_ET, "MET_ET/D");
    t_out->Branch("m_l1l2", &m_l1l2, "m_l1l2/D");
    t_out->Branch("m_l1l3", &m_l1l3, "m_l1l3/D");
    t_out->Branch("m_l2l3", &m_l2l3, "m_l2l3/D");
    t_out->Branch("m_l1j1", &m_l1j1, "m_l1j1/D");
    t_out->Branch("m_l2j1", &m_l2j1, "m_l2j1/D");
    t_out->Branch("m_l3j1", &m_l3j1, "m_l3j1/D");
    t_out->Branch("m_l1j2", &m_l1j2, "m_l1j2/D");
    t_out->Branch("m_l2j2", &m_l2j2, "m_l2j2/D");
    t_out->Branch("m_l3j2", &m_l3j2, "m_l3j2/D");
    t_out->Branch("m_l1j", &m_l1j, "m_l1j/D");
    t_out->Branch("m_l2j", &m_l2j, "m_l2j/D");
    t_out->Branch("m_l3j", &m_l3j, "m_l3j/D");
    t_out->Branch("m_l3jj", &m_l3jj, "m_l3jj/D");
    t_out->Branch("m_lll", &m_lll, "m_lll/D");
    t_out->Branch("m_llljj", &m_llljj, "m_llljj/D");
    t_out->Branch("dR_l1l2", &dR_l1l2, "dR_l1l2/D");
    t_out->Branch("dR_l1l3", &dR_l1l3, "dR_l1l3/D");
    t_out->Branch("dR_l2l3", &dR_l2l3, "dR_l2l3/D");
    t_out->Branch("dR_l1j1", &dR_l1j1, "dR_l1j1/D");
    t_out->Branch("dR_l2j1", &dR_l2j1, "dR_l2j1/D");
    t_out->Branch("dR_l3j1", &dR_l3j1, "dR_l3j1/D");
    t_out->Branch("dR_l1j2", &dR_l1j2, "dR_l1j2/D");
    t_out->Branch("dR_l2j2", &dR_l2j2, "dR_l2j2/D");
    t_out->Branch("dR_l3j2", &dR_l3j2, "dR_l3j2/D");
    t_out->Branch("dR_l1j", &dR_l1j, "dR_l1j/D");
    t_out->Branch("dR_l2j", &dR_l2j, "dR_l2j/D");
    t_out->Branch("dR_l3j", &dR_l3j, "dR_l3j/D");
    t_out->Branch("ZMassVeto", &ZMassVeto, "ZMassVeto/O");
    t_out->Branch("ZJetsVeto", &ZJetsVeto, "ZJetsVeto/O");
    t_out->Branch("MlllCut", &MlllCut, "MlllCut/O");
    t_out->Branch("loMassVeto", &loMassVeto, "loMassVeto/O");

//    t_out->Branch("lep_ID", &lep_ID, "lep_ID[3]/D");
//    t_out->Branch("lep_E", &lep_E, "lep_E[3]/D");
//    t_out->Branch("lep_Pt", &lep_Pt, "lep_Pt[3]/D");
//    t_out->Branch("lep_Eta", &lep_Eta, "lep_Eta[3]/D");
//    t_out->Branch("lep_Phi", &lep_Phi, "lep_Phi[3]/D");
    t_out->Branch("lep_ID_0", &lep_ID[0], "lep_ID_0/D");
    t_out->Branch("lep_E_0", &lep_E[0], "lep_E_0/D");
    t_out->Branch("lep_Pt_0", &lep_Pt[0], "lep_Pt_0/D");
    t_out->Branch("lep_Eta_0", &lep_Eta[0], "lep_Eta_0/D");
    t_out->Branch("lep_Phi_0", &lep_Phi[0], "lep_Phi_0/D");
    t_out->Branch("lep_ID_1", &lep_ID[1], "lep_ID_1/D");
    t_out->Branch("lep_E_1", &lep_E[1], "lep_E_1/D");
    t_out->Branch("lep_Pt_1", &lep_Pt[1], "lep_Pt_1/D");
    t_out->Branch("lep_Eta_1", &lep_Eta[1], "lep_Eta_1/D");
    t_out->Branch("lep_Phi_1", &lep_Phi[1], "lep_Phi_1/D");
    t_out->Branch("lep_ID_2", &lep_ID[2], "lep_ID_2/D");
    t_out->Branch("lep_E_2", &lep_E[2], "lep_E_2/D");
    t_out->Branch("lep_Pt_2", &lep_Pt[2], "lep_Pt_2/D");
    t_out->Branch("lep_Eta_2", &lep_Eta[2], "lep_Eta_2/D");
    t_out->Branch("lep_Phi_2", &lep_Phi[2], "lep_Phi_2/D");

    t_out->Branch("lep_isPrompt", &lep_isPrompt, "lep_isPrompt[3]/B");
    t_out->Branch("lep_truthType", &lep_truthType, "lep_truthType[3]/I");
    t_out->Branch("lep_truthOrigin", &lep_truthOrigin, "lep_truthOrigin[3]/I");
    t_out->Branch("lep_truthPDG", &lep_truthPDG, "lep_truthPDG[3]/I");
    t_out->Branch("lep_truthParentPDG", &lep_truthParentPDG, "lep_truthParentPDG[3]/I");
    t_out->Branch("lep_truthParentType", &lep_truthParentType, "lep_truthParentType[3]/I");
    t_out->Branch("lep_truthParentOrigin", &lep_truthParentOrigin, "lep_truthParentOrigin[3]/I");

    t_out->Branch("leadJetPt", &leadJetPt, "leadJetPt/D");
    t_out->Branch("leadJetEta", &leadJetEta, "leadJetEta/D");
    t_out->Branch("leadJetPhi", &leadJetPhi, "leadJetPhi/D");
    t_out->Branch("leadJetE", &leadJetE, "leadJetE/D");
    t_out->Branch("subleadJetPt", &subleadJetPt, "subleadJetPt/D");
    t_out->Branch("subleadJetEta", &subleadJetEta, "subleadJetEta/D");
    t_out->Branch("subleadJetPhi", &subleadJetPhi, "subleadJetPhi/D");
    t_out->Branch("subleadJetE", &subleadJetE, "subleadJetE/D");

    t_out->Branch("Higgsness", &Higgsness, "Higgsness/D");

    // Original Branch
    t_out->Branch("best_Z_Mll", &best_Z_Mll, "best_Z_Mll/D");
    t_out->Branch("HT", &HT, "HT/D");
    t_out->Branch("HT_lep", &HT_lep, "HT_lep/D");
    t_out->Branch("HT_jets", &HT_jets, "HT_jets/D");
    t_out->Branch("best_Z_other_MtLepMet", &best_Z_other_MtLepMet, "best_Z_other_MtLepMet/D");
    t_out->Branch("best_Z_other_Mll", &best_Z_other_Mll, "best_Z_other_Mll/D");

    // systematics
    t_out->Branch("Evt_SR_TF_HF_e", &Evt_SR_TF_HF_e, "Evt_SR_TF_HF_e/O");
    t_out->Branch("Evt_SR_TF_HF_m", &Evt_SR_TF_HF_m, "Evt_SR_TF_HF_m/O");
    t_out->Branch("Evt_SR_TF_ExtConv", &Evt_SR_TF_ExtConv, "Evt_SR_TF_ExtConv/O");

    // systematics weight
    t_out->Branch("weight_FF_elStat_Bin1U", &weight_FF_elStat_Bin1U, "weight_FF_elStat_Bin1U/D");
    t_out->Branch("weight_FF_elStat_Bin1D", &weight_FF_elStat_Bin1D, "weight_FF_elStat_Bin1D/D");
    t_out->Branch("weight_FF_elStat_Bin2U", &weight_FF_elStat_Bin2U, "weight_FF_elStat_Bin2U/D");
    t_out->Branch("weight_FF_elStat_Bin2D", &weight_FF_elStat_Bin2D, "weight_FF_elStat_Bin2D/D");
    t_out->Branch("weight_FF_muStat_Bin1U", &weight_FF_muStat_Bin1U, "weight_FF_muStat_Bin1U/D");
    t_out->Branch("weight_FF_muStat_Bin1D", &weight_FF_muStat_Bin1D, "weight_FF_muStat_Bin1D/D");
    t_out->Branch("weight_FF_muStat_Bin2U", &weight_FF_muStat_Bin2U, "weight_FF_muStat_Bin2U/D");
    t_out->Branch("weight_FF_muStat_Bin2D", &weight_FF_muStat_Bin2D, "weight_FF_muStat_Bin2D/D");
}

void nTuple_Analysis::Loop_Tree() {
    Long64_t nentries = tl->fChain->GetEntries();

    cout << "[Loop Tree]==> Start looping event for " << sample_name << " : " << nentries << endl;

    for (Long64_t i = 0; i < nentries; ++i) {
//    for (Long64_t i = 0; i < 20; ++i) {

        // Start Looping!
        tl->GetEntry(i);

        if (verbose > 0) {
            if (i % (nentries / 20) == 0) {
                printf("-- %s: %4.2f\n", sample_name.Data(), (float) i / (float) nentries);
                fflush(stdout);
            }
        }

        switch (sample_type) {
            case Sample_Type::None:
                SampleType = -1;
                break;
            case Sample_Type::DATA:
                SampleType = 0;
                break;
            case Sample_Type::PROMPT:
                SampleType = 1;
                break;
            case Sample_Type::MCFAKE:
                SampleType = 2;
                break;
            case Sample_Type::FAKE_DATA:
                SampleType = 3;
                break;
            case Sample_Type::FAKE_MC:
                SampleType = 4;
                break;
        }

        // global weight calculation
        for (auto &wl: weight_l) wl = 1.;
        if (sample_type == Sample_Type::DATA) _weight = 1.;
        else {
            auto RunYear = tl->RunYear;
            _weight = (36074.6 * (RunYear == 2015 || RunYear == 2016) + 43813.7 * (RunYear == 2017) +
                       58450.1 * (RunYear == 2018))
                      * tl->weight_pileup
                      * tl->jvtSF_customOR // jvt
                      * tl->weight_mc
                      * tl->mc_xSection
                      * tl->bTagSF_weight_DL1r_77
                      * tl->custTrigSF_LooseID_FCLooseIso_SLTorDLT
                      * tl->mc_kFactor / tl->totalEventsWeighted;
//                      * tl->mc_kFactor / 3114.08;

            _weight_new = _weight;
        }

        // weight conversion
        convert_wp();

        /*
         * Some Basic Conversion
         */
        convert_original_branch();

        /*
         * Flavor Categorization
         */
        int nElectron = 0;
        int nMuon = 0;
        {
            for (auto id: lep_ID) {
                if (abs(id) == 11) nElectron++;
                if (abs(id) == 13) nMuon++;
            }
            if (nElectron + nMuon != 3) continue;
            if (nElectron == 3) FlavorCategory = 1; // eee
            if (nElectron == 2 && abs(lep_ID[0]) == 11 && abs(lep_ID[1]) == 11) FlavorCategory = 2; // eem
            if (nElectron == 2 && abs(lep_ID[0]) == 11 && abs(lep_ID[1]) == 13) FlavorCategory = 3; // eme
            if (nElectron == 1 && abs(lep_ID[0]) == 11) FlavorCategory = 4; // emm
            if (nElectron == 2 && abs(lep_ID[0]) == 13) FlavorCategory = 5; // mee
            if (nElectron == 1 && abs(lep_ID[0]) == 13 && abs(lep_ID[1]) == 11) FlavorCategory = 6; // mem
            if (nElectron == 1 && abs(lep_ID[0]) == 13 && abs(lep_ID[1]) == 13) FlavorCategory = 7; // mme
            if (nElectron == 0) FlavorCategory = 8; // mmm
            /*  SFOS0 = 4 && 5;
             *  SFSO1 = 2 && 3 && 6 && 7;
             *  SFSO2 = 1 && 8;
             *  */
        }

        /*
         * Kinematics
         */
        {
            for (auto &l: lep_Pt) l *= unit_conv;
            for (auto &l: lep_E) l *= unit_conv;

            TLorentzVector l1;
            l1.SetPtEtaPhiE(lep_Pt[0], lep_Eta[0], lep_Phi[0], lep_E[0]);
            TLorentzVector l2;
            l2.SetPtEtaPhiE(lep_Pt[1], lep_Eta[1], lep_Phi[1], lep_E[1]);
            TLorentzVector l3;
            l3.SetPtEtaPhiE(lep_Pt[2], lep_Eta[2], lep_Phi[2], lep_E[2]);
            TLorentzVector j1;
            j1.SetPtEtaPhiE(leadJetPt, leadJetEta, leadJetPhi, leadJetE);
            TLorentzVector j2;
            j2.SetPtEtaPhiE(subleadJetPt, subleadJetEta, subleadJetPhi, subleadJetE);

            m_l1l2 = (l1 + l2).M();
            m_l1l3 = (l1 + l3).M();
            m_l2l3 = (l2 + l3).M();
            m_l1j = (l1.DeltaR(j1) <= l1.DeltaR(j2)) ? (l1 + j1).M() : (l1 + j2).M();
            m_l2j = (l2.DeltaR(j1) <= l2.DeltaR(j2)) ? (l2 + j1).M() : (l2 + j2).M();
            m_l3j = (l3.DeltaR(j1) <= l3.DeltaR(j2)) ? (l3 + j1).M() : (l3 + j2).M();
            m_l3jj = (l3 + j1 + j2).M();
            m_lll = (l1 + l2 + l3).M();
            m_llljj = std::min(m_l1l2 + m_l3jj, m_l1l3 + (l2 + j1 + j2).M());
            dR_l1l2 = l1.DeltaR(l2);
            dR_l1l3 = l1.DeltaR(l3);
            dR_l2l3 = l2.DeltaR(l3);
            dR_l1j = (l1.DeltaR(j1) <= l1.DeltaR(j2)) ? l1.DeltaR(j1) : l1.DeltaR(j2);
            dR_l2j = (l2.DeltaR(j1) <= l2.DeltaR(j2)) ? l2.DeltaR(j1) : l2.DeltaR(j2);
            dR_l3j = (l3.DeltaR(j1) <= l3.DeltaR(j2)) ? l3.DeltaR(j1) : l3.DeltaR(j2);

            m_l1j1 = (l1 + j1).M();
            m_l2j1 = (l2 + j1).M();
            m_l3j1 = (l3 + j1).M();
            m_l1j2 = (l1 + j2).M();
            m_l2j2 = (l2 + j2).M();
            m_l3j2 = (l3 + j2).M();

            dR_l1j1 = l1.DeltaR(j1);
            dR_l2j1 = l2.DeltaR(j1);
            dR_l3j1 = l3.DeltaR(j1);
            dR_l1j2 = l1.DeltaR(j2);
            dR_l2j2 = l2.DeltaR(j2);
            dR_l3j2 = l3.DeltaR(j2);
        }

        // Electron Quality
        Evt_ChargeIDBDT = chargeIDBDT_cut();
        Evt_AuthorCut = (((abs(lep_ID[0]) == 11 && tl->lep_ambiguityType_0 == 0) || abs(lep_ID[0]) == 13)
                         && ((abs(lep_ID[1]) == 11 && tl->lep_ambiguityType_1 == 0) || abs(lep_ID[1]) == 13)
                         && ((abs(lep_ID[2]) == 11 && tl->lep_ambiguityType_2 == 0) || abs(lep_ID[2]) == 13));

        // Event Quality
        Evt_ID_Loose = quality_cut(0, WorkPoint::Loose, WorkPoint::None) &&
                       quality_cut(1, WorkPoint::Loose, WorkPoint::None) &&
                       quality_cut(2, WorkPoint::Loose, WorkPoint::None);

        Evt_ISO_Loose = quality_cut(0, WorkPoint::None, WorkPoint::Loose, Isolation_Type::PLV) &&
                        quality_cut(1, WorkPoint::None, WorkPoint::Loose, Isolation_Type::PLV) &&
                        quality_cut(2, WorkPoint::None, WorkPoint::Loose, Isolation_Type::PLV);

        Evt_ID_Tight = quality_cut(0, WorkPoint::Loose, WorkPoint::None) &&
                       quality_cut(1, WorkPoint::Tight, WorkPoint::None) &&
                       quality_cut(2, WorkPoint::Tight, WorkPoint::None);

        Evt_ISO_Tight = quality_cut(0, WorkPoint::None, WorkPoint::Loose, Isolation_Type::PLV) &&
                        quality_cut(1, WorkPoint::None, WorkPoint::Tight, Isolation_Type::PLV) &&
                        quality_cut(2, WorkPoint::None, WorkPoint::Tight, Isolation_Type::PLV);

        // PLV Cut Selection
        bool plv_0 = quality_cut(0, static_cast<WorkPoint>(dControl->lep_quality_0.at(0)),
                                 static_cast<WorkPoint>(dControl->lep_quality_0.at(1)),Isolation_Type::PLV);
        bool plv_1 = quality_cut(1, static_cast<WorkPoint>(dControl->lep_quality_1.at(0)),
                                 static_cast<WorkPoint>(dControl->lep_quality_1.at(1)),Isolation_Type::PLV);
        bool plv_2 = quality_cut(2, static_cast<WorkPoint>(dControl->lep_quality_2.at(0)),
                                 static_cast<WorkPoint>(dControl->lep_quality_2.at(1)),Isolation_Type::PLV);
        Evt_Tight = plv_0 && plv_1 && plv_2;

        // Truth Prompt Check
        Evt_Prompt = tl->lep_isPrompt_0 && tl->lep_isPrompt_1 && tl->lep_isPrompt_2;

        // Low Mass Veto
        loMassVeto = true;
        if (FlavorCategory == 2 || FlavorCategory == 7) loMassVeto = (m_l1l2 > 12.);
        if (FlavorCategory == 3 || FlavorCategory == 6) loMassVeto = (m_l1l3 > 12.);
        if (FlavorCategory == 1 || FlavorCategory == 8) loMassVeto = ((m_l1l2 > 12.) && (m_l1l3 > 12.));

        // Z Mass Veto
        MlllCut = (fabs(m_lll - Z_Mass) >= 10);
        ZMassVeto = true;
        ZJetsVeto = false;
        double Zm_1 = fabs(m_l1l2 - Z_Mass);
        double Zm_2 = fabs(m_l1l3 - Z_Mass);
        if (FlavorCategory == 2 || FlavorCategory == 7) {
            ZMassVeto = (Zm_1 >= 10.);
            ZJetsVeto = !ZMassVeto && !plv_2;
        }
        if (FlavorCategory == 3 || FlavorCategory == 6) {
            ZMassVeto = (Zm_2 >= 10.);
            ZJetsVeto = !ZMassVeto && !plv_1;
        }
        if (FlavorCategory == 1 || FlavorCategory == 8) {
            ZMassVeto = ((Zm_1 >= 10.) && (Zm_2 >= 10.));
            if ((Zm_1 <= 10.) && (Zm_2 <= 10.))
                ZJetsVeto = (Zm_1 < Zm_2) ? !plv_2 : !plv_1;
            else
                ZJetsVeto = ((Zm_2 <= 10.) && !plv_1) || ((Zm_1 <= 10.) && !plv_2);
        }
//        ZMassVeto =     ((((abs(tl->lep_ID_0) == 11) && (abs(tl->lep_ID_1) == 11) && (abs(tl->Mll01 - 91200) > 10000)) ||
//                          ((abs(tl->lep_ID_0) == 13) && (abs(tl->lep_ID_1) == 13) && (abs(tl->Mll01 - 91200) > 10000))) ||
//                         (((abs(tl->lep_ID_0) == 11) && (abs(tl->lep_ID_1) == 13)) || ((abs(tl->lep_ID_0) == 13) && (abs(tl->lep_ID_1) == 11))))
//
//                        &&
//
//                        ((((abs(tl->lep_ID_0) == 11) && (abs(tl->lep_ID_2) == 11) && (abs(tl->Mll02 - 91200) > 10000)) ||
//                          ((abs(tl->lep_ID_0) == 13) && (abs(tl->lep_ID_2) == 13) && (abs(tl->Mll02 - 91200) > 10000))) ||
//                         (((abs(tl->lep_ID_0) == 11) && (abs(tl->lep_ID_2) == 13)) || ((abs(tl->lep_ID_0) == 13) && (abs(tl->lep_ID_2) == 11))));

        // Calculate WZ Control Region
        Evt_WZ_CR = cut_flow(vector<int>({11, 12})) && ZMassVeto == 0 && MET_ET > 30 && nJets >= 0;

        // Classify Sample Fake Type
        auto cr = truth_region_analysis();
        Class_HF = cr.hf_e;
        Class_HF_m = cr.hf_m;
        Class_ExtConv = cr.external_conversion;
        Class_IntConv = cr.internal_conversion;
        Class_Others = cr.others;
        Class_LF = cr.lf_e;
        Class_LF_m = cr.lf_m;
        Class_QMISID = cr.QMisID;
        Class_Prompt = cr.prompt;

        // Calculate Fake-Related
        auto weight_FF = 1.0;
        std::tie(Evt_AntiTight, weight_FF) = fake_analysis(plv_1, plv_2);
        bool if_FF = Evt_FF_CR_B || Evt_FF_CR_C || Evt_FF_CR_D;
        if_FF = false;

        // Calculate Template Fit
        auto weight_TF = 1.0;
        auto if_TF = false;
        auto tf_region = template_fit_region(false);
        Evt_TF_IntConv_e = tf_region.internal_conversion;
        Evt_TF_ExtConv_e = tf_region.external_conversion;
        Evt_TF_HF_e = tf_region.hf_e;
        Evt_TF_LF_e = tf_region.lf_e;
        Evt_TF_HF_m = tf_region.hf_m;
        // calculate template fit systematics region
        auto tf_region_syst = template_fit_region(true);
        Evt_TF_IntConv_e_Syst = tf_region_syst.internal_conversion;
        Evt_TF_ExtConv_e_Syst = tf_region_syst.external_conversion;
        Evt_TF_HF_e_Syst = tf_region_syst.hf_e;
        Evt_TF_LF_e_Syst = tf_region_syst.lf_e;
        Evt_TF_HF_m_Syst = tf_region_syst.hf_m;

        Evt_SR_TF_HF_e = cut_flow(vector<int>({4, 14}), false) && !Evt_Tight && (FlavorCategory == 1 || FlavorCategory == 5);
        Evt_SR_TF_HF_m = cut_flow(vector<int>({4, 14}), false) && !Evt_Tight && (FlavorCategory == 4 || FlavorCategory == 8);
        Evt_SR_TF_ExtConv = cut_flow(vector<int>({7, 14}), false) && !Evt_AuthorCut && (!Evt_Tight && Evt_ID_Tight);

        std::tie(Evt_Fake, weight_TF) = template_fit_apply();
        if_TF = (Evt_TF_IntConv_e || Evt_TF_ExtConv_e || Evt_TF_HF_e || Evt_TF_LF_e || Evt_TF_HF_m)
                || (Evt_TF_ExtConv_e_Syst || Evt_TF_HF_e_Syst || Evt_TF_HF_m_Syst)
                || (Evt_SR_TF_HF_e || Evt_SR_TF_HF_m || Evt_SR_TF_ExtConv);

        // Updating Selection
        //if (sample_type == Sample_Type::FAKE_DATA || sample_type == Sample_Type::FAKE_MC) Evt_Tight = 1;
        { // new weight calculation
            if (sample_type == Sample_Type::DATA) _weight_new = 1.;
            else {
                _weight_new *=
                        tl->lep_SF_El_Reco_AT_0 * tl->lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0 *
                        tl->lep_SF_El_ID_LooseAndBLayerLH_AT_0 * tl->lep_SF_El_PLVLoose_0 *
                        tl->lep_SF_Mu_ID_Loose_AT_0 * tl->lep_SF_Mu_TTVA_AT_0 * tl->lep_SF_Mu_PLVLoose_0 *

                        tl->lep_SF_El_Reco_AT_1 * tl->lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1 *
                        tl->lep_SF_El_ID_TightLH_AT_1 * tl->lep_SF_El_PLVTight_1 *
                        tl->lep_SF_Mu_ID_Medium_AT_1 * tl->lep_SF_Mu_TTVA_AT_1 * tl->lep_SF_Mu_PLVTight_1 *

                        tl->lep_SF_El_Reco_AT_2 * tl->lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2 *
                        tl->lep_SF_El_ID_TightLH_AT_2 * tl->lep_SF_El_PLVTight_2 *
                        tl->lep_SF_Mu_ID_Medium_AT_2 * tl->lep_SF_Mu_TTVA_AT_2 * tl->lep_SF_Mu_PLVTight_2;
            }
        }
        {// Updating weight
            weight_fake = (if_FF) ? weight_FF : weight_TF;

            if (sample_type == Sample_Type::DATA) _weight = 1.;
            else if (sample_type == Sample_Type::FAKE_DATA)
                _weight = (if_FF) ? weight_fake : 1.;
            else {
                // WZ_normalization
                if (sample_name.Contains("WZ")) {
                    for (int i_size = 0; i_size < dControl->nf_WZ.size(); ++i_size) {
                        if (nJets == i_size) {
                            _weight_new *= dControl->nf_WZ.at(i_size);
                            _weight *= dControl->nf_WZ.at(i_size);
                        }
                    }
                    if (nJets >= dControl->nf_WZ.size()) {
                        _weight_new *= dControl->nf_WZ.at(dControl->nf_WZ.size() - 1);
                        _weight *= dControl->nf_WZ.at(dControl->nf_WZ.size() - 1);
                    }
                }
                _weight *= weight_l[0] * weight_l[1] * weight_l[2] * weight_chargeIDBDT;
                if (sample_type == Sample_Type::FAKE_MC) {
                    weight_FF_muStat_Bin1U *= (if_FF) ? -1 * _weight : 1;
                    weight_FF_muStat_Bin1D *= (if_FF) ? -1 * _weight : 1;
                    weight_FF_muStat_Bin2U *= (if_FF) ? -1 * _weight : 1;
                    weight_FF_muStat_Bin2D *= (if_FF) ? -1 * _weight : 1;
                    _weight *= (if_FF) ? -weight_fake : weight_fake;
                }
            }
        }
        // Update Event Type
        Evt_SR = cut_flow(vector<int>(), true);
        Evt_Fake = if_FF || if_TF;

        if (Evt_SR || Evt_Fake || Evt_WZ_CR) {
//            EvtNum++;
            if (dControl->if_Higgsness) Higgsness = GetHiggsness();

            if (sample_type == Sample_Type::FAKE_MC || sample_type == Sample_Type::FAKE_DATA) {
                if (Evt_SR || Evt_FF_CR_B) t_out->Fill();
            } else
                t_out->Fill();
        }
    }
}

bool nTuple_Analysis::quality_cut(int id, WorkPoint wp_id, WorkPoint wp_iso, Isolation_Type it) {
    auto re = lep_id.at(id).at(static_cast<unsigned>(wp_id)) && lep_iso.at(id).at(it).at(static_cast<unsigned>(wp_iso));

    if (re) {
        weight_l[id] = lep_id_weight.at(id).at(static_cast<unsigned>(wp_id)) *
                       lep_iso_weight.at(id).at(it).at(static_cast<unsigned>(wp_iso));
    }

    return re;
}

void nTuple_Analysis::convert_wp() {
    // Structure (id, [loose, tight])
    lep_id.clear();
    lep_id_weight.clear();
    lep_iso.clear();
    lep_iso_weight.clear();

    // Isolation
    lep_iso.insert(pair(0, map<Isolation_Type, vector<Int_t>>(
            {pair(Isolation_Type::PLV, vector<Int_t>({1, tl->lep_plvWP_Loose_0, tl->lep_plvWP_Tight_0})),
             pair(Isolation_Type::FCLoose,
                  vector<Int_t>({1, tl->lep_isolationFCLoose_0, tl->lep_isolationFCTight_0}))})));
    lep_iso.insert(pair(1, map<Isolation_Type, vector<Int_t>>(
            {pair(Isolation_Type::PLV, vector<Int_t>({1, tl->lep_plvWP_Loose_1, tl->lep_plvWP_Tight_1})),
             pair(Isolation_Type::FCLoose,
                  vector<Int_t>({1, tl->lep_isolationFCLoose_1, tl->lep_isolationFCTight_1}))})));
    lep_iso.insert(pair(2, map<Isolation_Type, vector<Int_t>>(
            {pair(Isolation_Type::PLV, vector<Int_t>({1, tl->lep_plvWP_Loose_2, tl->lep_plvWP_Tight_2})),
             pair(Isolation_Type::FCLoose,
                  vector<Int_t>({1, tl->lep_isolationFCLoose_2, tl->lep_isolationFCTight_2}))})));

    float lep_w_l, lep_w_t;
    if (abs(tl->lep_ID_0) == 11) {
        lep_id.insert(pair(0, vector<Char_t>({1, tl->lep_isLooseLH_0, tl->lep_isTightLH_0})));
        lep_w_l = tl->lep_SF_El_Reco_AT_0 * tl->lep_SF_El_ID_LooseAndBLayerLH_AT_0;
        lep_w_t = tl->lep_SF_El_Reco_AT_0 * tl->lep_SF_El_ID_TightLH_AT_0;
        lep_id_weight.insert(pair(0, vector<Float_t>({1, lep_w_l, lep_w_t})));
        lep_iso_weight.insert(pair(0, map<Isolation_Type, vector<Float_t>>(
                {pair(Isolation_Type::PLV,
                      vector<Float_t>({1, tl->lep_SF_El_PLVLoose_0, tl->lep_SF_El_PLVTight_0})),
                 pair(Isolation_Type::FCLoose,
                      vector<Float_t>(
                              {1, tl->lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_0,
                               tl->lep_SF_El_Iso_FCLoose_TightLH_0}))})));
    } else if (abs(tl->lep_ID_0) == 13) {
//        lep_id.insert(pair(0, vector<Char_t>({1, tl->lep_isLoose_0, tl->lep_isMedium_0})));
//        lep_w_l = tl->lep_SF_Mu_TTVA_AT_0 * tl->lep_SF_Mu_ID_Loose_AT_0;
        lep_id.insert(pair(0, vector<Char_t>({1, tl->lep_isLoose_0, tl->lep_isMedium_0})));
        lep_w_l = tl->lep_SF_Mu_TTVA_AT_0 * tl->lep_SF_Mu_ID_Loose_AT_0;
        lep_w_t = tl->lep_SF_Mu_TTVA_AT_0 * tl->lep_SF_Mu_ID_Medium_AT_0;
        lep_id_weight.insert(pair(0, vector<Float_t>({1, lep_w_l, lep_w_t})));
        lep_iso_weight.insert(pair(0, map<Isolation_Type, vector<Float_t>>(
                {pair(Isolation_Type::PLV,
                      vector<Float_t>({1, tl->lep_SF_Mu_PLVLoose_0, tl->lep_SF_Mu_PLVTight_0})),
                 pair(Isolation_Type::FCLoose,
                      vector<Float_t>({1, tl->lep_SF_Mu_Iso_FCLoose_AT_0, tl->lep_SF_Mu_Iso_FCLoose_AT_0}))})));
    }

    if (abs(tl->lep_ID_1) == 11) {
        lep_id.insert(pair(1, vector<Char_t>({1, tl->lep_isLooseLH_1, tl->lep_isTightLH_1})));
        lep_w_l = tl->lep_SF_El_Reco_AT_1 * tl->lep_SF_El_ID_LooseAndBLayerLH_AT_1;
        lep_w_t = tl->lep_SF_El_Reco_AT_1 * tl->lep_SF_El_ID_TightLH_AT_1;
        lep_id_weight.insert(pair(1, vector<Float_t>({1, lep_w_l, lep_w_t})));
        lep_iso_weight.insert(pair(1, map<Isolation_Type, vector<Float_t>>(
                {pair(Isolation_Type::PLV,
                      vector<Float_t>({1, tl->lep_SF_El_PLVLoose_1, tl->lep_SF_El_PLVTight_1})),
                 pair(Isolation_Type::FCLoose,
                      vector<Float_t>(
                              {1, tl->lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1,
                               tl->lep_SF_El_Iso_FCLoose_TightLH_1}))})));
    } else if (abs(tl->lep_ID_1) == 13) {
//        lep_id.insert(pair(1, vector<Char_t>({1, tl->lep_isLoose_1, tl->lep_isMedium_1})));
//        lep_w_l = tl->lep_SF_Mu_TTVA_AT_1 * tl->lep_SF_Mu_ID_Loose_AT_1;
        lep_id.insert(pair(1, vector<Char_t>({1, tl->lep_isLoose_1, tl->lep_isMedium_1})));
        lep_w_l = tl->lep_SF_Mu_TTVA_AT_1 * tl->lep_SF_Mu_ID_Loose_AT_1;
        lep_w_t = tl->lep_SF_Mu_TTVA_AT_1 * tl->lep_SF_Mu_ID_Medium_AT_1;
        lep_id_weight.insert(pair(1, vector<Float_t>({1, lep_w_l, lep_w_t})));
        lep_iso_weight.insert(pair(1, map<Isolation_Type, vector<Float_t>>(
                {pair(Isolation_Type::PLV,
                      vector<Float_t>({1, tl->lep_SF_Mu_PLVLoose_1, tl->lep_SF_Mu_PLVTight_1})),
                 pair(Isolation_Type::FCLoose,
                      vector<Float_t>({1, tl->lep_SF_Mu_Iso_FCLoose_AT_1, tl->lep_SF_Mu_Iso_FCLoose_AT_1}))})));
    }

    if (abs(tl->lep_ID_2) == 11) {
        lep_id.insert(pair(2, vector<Char_t>({1, tl->lep_isLooseLH_2, tl->lep_isTightLH_2})));
        lep_w_l = tl->lep_SF_El_Reco_AT_2 * tl->lep_SF_El_ID_LooseAndBLayerLH_AT_2;
        lep_w_t = tl->lep_SF_El_Reco_AT_2 * tl->lep_SF_El_ID_TightLH_AT_2;
        lep_id_weight.insert(pair(2, vector<Float_t>({1, lep_w_l, lep_w_t})));
        lep_iso_weight.insert(pair(2, map<Isolation_Type, vector<Float_t>>(
                {pair(Isolation_Type::PLV,
                      vector<Float_t>({1, tl->lep_SF_El_PLVLoose_2, tl->lep_SF_El_PLVTight_2})),
                 pair(Isolation_Type::FCLoose,
                      vector<Float_t>(
                              {1, tl->lep_SF_El_Iso_FCLoose_LooseAndBLayerLH_1,
                               tl->lep_SF_El_Iso_FCLoose_TightLH_2}))})));
    } else if (abs(tl->lep_ID_2) == 13) {
//        lep_id.insert(pair(2, vector<Char_t>({1, tl->lep_isLoose_2, tl->lep_isMedium_2})));
//        lep_w_l = tl->lep_SF_Mu_TTVA_AT_2 * tl->lep_SF_Mu_ID_Loose_AT_2;
        lep_id.insert(pair(2, vector<Char_t>({1, tl->lep_isLoose_2, tl->lep_isMedium_2})));
        lep_w_l = tl->lep_SF_Mu_TTVA_AT_2 * tl->lep_SF_Mu_ID_Loose_AT_2;
        lep_w_t = tl->lep_SF_Mu_TTVA_AT_2 * tl->lep_SF_Mu_ID_Medium_AT_2;
        lep_id_weight.insert(pair(2, vector<Float_t>({1, lep_w_l, lep_w_t})));
        lep_iso_weight.insert(pair(2, map<Isolation_Type, vector<Float_t>>(
                {pair(Isolation_Type::PLV,
                      vector<Float_t>({1, tl->lep_SF_Mu_PLVLoose_2, tl->lep_SF_Mu_PLVTight_2})),
                 pair(Isolation_Type::FCLoose,
                      vector<Float_t>({1, tl->lep_SF_Mu_Iso_FCLoose_AT_2, tl->lep_SF_Mu_Iso_FCLoose_AT_2}))})));
    }

}

void nTuple_Analysis::convert_original_branch() {
    totalCharge = tl->total_charge;
    nJets = tl->nJets_OR;
    nbJets = tl->nJets_OR_DL1r_77;
    MET_ET = tl->met_met * unit_conv; // convert to GeV
    Higgs1Mode = tl->higgs1Mode;
    Higgs2Mode = tl->higgs2Mode;
    EvtNum = tl->eventNumber;

    leadJetPt = tl->lead_jetPt * unit_conv;
    leadJetEta = tl->lead_jetEta;
    leadJetPhi = tl->lead_jetPhi;
    leadJetE = tl->lead_jetE * unit_conv;
    subleadJetPt = tl->sublead_jetPt * unit_conv;
    subleadJetEta = tl->sublead_jetEta;
    subleadJetPhi = tl->sublead_jetPhi;
    subleadJetE = tl->sublead_jetE * unit_conv;

    best_Z_Mll = tl->best_Z_Mll * unit_conv;
    HT = tl->HT * unit_conv;
    HT_lep = tl->HT_lep * unit_conv;
    HT_jets = tl->HT_jets * unit_conv;
    best_Z_other_MtLepMet = tl->best_Z_other_MtLepMet * unit_conv;
    best_Z_other_Mll = tl->best_Z_other_Mll * unit_conv;

    // Lep 0 
    lep_ID[0] = tl->lep_ID_0;
    lep_E[0] = tl->lep_E_0;
    lep_Pt[0] = tl->lep_Pt_0;
    lep_Eta[0] = tl->lep_Eta_0;
    lep_Phi[0] = tl->lep_Phi_0;
    lep_isPrompt[0] = tl->lep_isPrompt_0;
    lep_truthType[0] = tl->lep_truthType_0;
    lep_truthOrigin[0] = tl->lep_truthOrigin_0;
    lep_truthPDG[0] = tl->lep_truthPdgId_0;
    lep_truthParentPDG[0] = tl->lep_truthParentPdgId_0;
    lep_truthParentOrigin[0] = tl->lep_truthParentOrigin_0;
    lep_truthParentType[0] = tl->lep_truthParentType_0;
    // Lep 1
    lep_ID[1] = tl->lep_ID_1;
    lep_E[1] = tl->lep_E_1;
    lep_Pt[1] = tl->lep_Pt_1;
    lep_Eta[1] = tl->lep_Eta_1;
    lep_Phi[1] = tl->lep_Phi_1;
    lep_isPrompt[1] = tl->lep_isPrompt_1;
    lep_truthType[1] = tl->lep_truthType_1;
    lep_truthOrigin[1] = tl->lep_truthOrigin_1;
    lep_truthPDG[1] = tl->lep_truthPdgId_1;
    lep_truthParentPDG[1] = tl->lep_truthParentPdgId_1;
    lep_truthParentOrigin[1] = tl->lep_truthParentOrigin_1;
    lep_truthParentType[1] = tl->lep_truthParentType_1;
    // Lep 2
    lep_ID[2] = tl->lep_ID_2;
    lep_E[2] = tl->lep_E_2;
    lep_Pt[2] = tl->lep_Pt_2;
    lep_Eta[2] = tl->lep_Eta_2;
    lep_Phi[2] = tl->lep_Phi_2;
    lep_isPrompt[2] = tl->lep_isPrompt_2;
    lep_truthType[2] = tl->lep_truthType_2;
    lep_truthOrigin[2] = tl->lep_truthOrigin_2;
    lep_truthPDG[1] = tl->lep_truthPdgId_1;
    lep_truthParentPDG[1] = tl->lep_truthParentPdgId_1;
    lep_truthParentOrigin[1] = tl->lep_truthParentOrigin_1;
    lep_truthParentType[1] = tl->lep_truthParentType_1;
}

bool nTuple_Analysis::chargeIDBDT_cut() {
    Bool_t result
            = ((abs(tl->lep_ID_0) == 11) ? tl->lep_chargeIDBDTLoose_0 : 1)
              && ((abs(tl->lep_ID_1) == 11) ? tl->lep_chargeIDBDTLoose_1 : 1)
              && ((abs(tl->lep_ID_2) == 11) ? tl->lep_chargeIDBDTLoose_2 : 1);

    weight_chargeIDBDT = 1.;
    if (result) {
        weight_chargeIDBDT =
                ((abs(tl->lep_ID_0) == 11) ? tl->lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_0 : 1)
                * ((abs(tl->lep_ID_1) == 11) ? tl->lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_1 : 1)
                * ((abs(tl->lep_ID_2) == 11) ? tl->lep_SF_El_ChargeMisID_LooseAndBLayerLH_FCLoose_AT_2 : 1);
    }

    return result;
}

bool nTuple_Analysis::cut_flow(const vector<int> &skipped, bool fill_hist) {

    std::vector<bool> cuts;
    // 0. 3-lepton type
    cuts.push_back(tl->total_leptons == 3);
    // 1. total charge
    cuts.push_back((abs(tl->total_charge) == 1));
    // 2. Trigger
    cuts.push_back((tl->GlobalTrigDecision && tl->custTrigMatch_LooseID_FCLooseIso_SLTorDLT));
    // 3. Lepton ID Loose
    cuts.push_back(Evt_ID_Loose);
    // 4. Lepton Isolation Loose
    cuts.push_back(Evt_ISO_Loose);  //&& dz_cut()
//     cuts.push_back(true);
    // 5. Tau veto
    cuts.push_back((tl->nTaus_OR_Pt25_RNN == 0));
    // 6. Charge ID BDT Loose
    cuts.push_back(Evt_ChargeIDBDT);
    // 7. Author Cut
    cuts.push_back(Evt_AuthorCut);
    // 8. Extra pT Cut
    cuts.push_back((lep_Pt[0] >= 10 && lep_Pt[1] >= 15 && lep_Pt[2] >= 15));
    // 9. b Jets veto (Nbjets < 0)
    cuts.push_back(nbJets <= dControl->N_bJets);
    // 10. Low mass veto
    cuts.push_back(loMassVeto == 1);
    // 11. # of jets (nJets >= 1)
    cuts.push_back(nJets >= dControl->N_Jets);
    // 12. Z-mass veto
    cuts.push_back(!dControl->ZMassVeto || (ZMassVeto == 1));
    // 13. m_l cut
    cuts.push_back(!dControl->M_l_Cut || MlllCut);
    // 14. Lepton Tight Quality cut
    cuts.push_back(!dControl->Evt_Tight || Evt_Tight);

//    // 0. 3-lepton type
//    cuts.push_back(tl->total_leptons == 3);
//    // 1. Trigger
//    cuts.push_back((tl->GlobalTrigDecision && tl->custTrigMatch_LooseID_FCLooseIso_SLTorDLT));
//    // 2. total charge
//    cuts.push_back((abs(tl->total_charge) == 1));
//    // 3. Lepton Tight Quality cut
//    cuts.push_back(Evt_ID_Tight);
//    // 3.5 Lepton Tight Quality cut
//    cuts.push_back(Evt_ISO_Tight);
//    // 4. Tau veto
//    cuts.push_back((tl->nTaus_OR_Pt25_RNN == 0));
//    // 5. Charge ID BDT Loose
//    cuts.push_back(Evt_ChargeIDBDT);
//    // 6. Author Cut
//    cuts.push_back(Evt_AuthorCut);
//    // 7. Extra pT Cut
//    cuts.push_back((lep_Pt[0] >= 10 && lep_Pt[1] >= 15 && lep_Pt[2] >= 15));
//    // 8. b Jets veto (Nbjets < 0)
//    cuts.push_back(nbJets <= dControl->N_bJets);
//    // 9. Low mass veto
//    cuts.push_back(loMassVeto == 1);
//    // 10. # of jets (nJets >= 1)
//    cuts.push_back(nJets >= dControl->N_Jets);
//    // 11. Z-mass veto
//    cuts.push_back(!dControl->ZMassVeto || (ZMassVeto == 1));
//    // 12. m_l cut
//    cuts.push_back(!dControl->M_l_Cut || MlllCut);


    // Fill Histogram and calculate cutflow results
    bool result = true;
    if (fill_hist) {
        for (unsigned i = 0; i < cuts.size(); i++) {
            result &= cuts.at(i);
            if (result) {
                cutflow->Fill(cut_name.at(i), 1);
                cutflow_w->Fill(cut_name.at(i), _weight_new);
            }
        }
    }

    // Calculate Skipped Cut
    result = true;
    for (unsigned i = 0; i < cuts.size(); i++) {
        if (std::find(skipped.begin(), skipped.end(), i) != skipped.end()) continue;
        result &= cuts.at(i);
    }

    return result;
}

tuple<bool, double> nTuple_Analysis::fake_analysis(bool lq1, bool lq2) {
    bool anti_tight = anti_tight_cut(lq1, lq2);
    bool result = anti_tight && cut_flow(vector<int>({4, 14}));
    double weight = 1.;

    /*
     * Fake Factor application
     */
    Evt_FF_CR_A = cut_flow();
    Evt_FF_CR_B = result;
//    Evt_FF_CR_C = cut_flow(vector<int>({12, 13})) && !ZMassVeto && MET_ET < 30.;
    Evt_FF_CR_C = false;
    if (FlavorCategory == 1 || FlavorCategory == 5)
        Evt_FF_CR_C = cut_flow(vector<int>({12, 13})) && !ZMassVeto &&
                      MET_ET < 30.;
    if (FlavorCategory == 4 || FlavorCategory == 8) Evt_FF_CR_C = cut_flow(vector<int>({9})) && (nbJets >= 1);

    auto categorize_ff = [&](bool electron) {
        // Classify region for Fake Factor
//        Evt_FF_CR_D = cut_flow(vector<int>({4, 12, 13, 14})) && ZJetsVeto && MET_ET < 30.;
        Evt_FF_CR_D = electron ? cut_flow(vector<int>({4, 12, 13, 14})) && ZJetsVeto && MET_ET < 30. :
                      cut_flow(vector<int>({4, 9, 14})) && (nbJets >= 1);

        if ((sample_type == Sample_Type::FAKE_DATA || sample_type == Sample_Type::FAKE_MC) && Evt_FF_CR_B) {
            TString lep = electron ? "electron" : "muon";
            if (dControl->ff_1.count(lep) == 0) return;
            // Calculated Fake Factor (DD)
            vector<vector<double>> ff = {dControl->ff_1.at(lep), dControl->ff_2.at(lep)};
            vector<vector<double>> pT_range = {dControl->pT_range_1.at(lep), dControl->pT_range_2.at(lep)};
            vector<vector<double>> ff_err = {dControl->ff_1_err.at(lep), dControl->ff_2_err.at(lep)};
            vector<vector<double *>> weight_err_mu = {{&weight_FF_muStat_Bin1U, &weight_FF_muStat_Bin2U},
                                                      {&weight_FF_muStat_Bin1D, &weight_FF_muStat_Bin2D}};
            vector<vector<double *>> weight_err_el = {{&weight_FF_elStat_Bin1U, &weight_FF_elStat_Bin2U},
                                                      {&weight_FF_elStat_Bin1D, &weight_FF_elStat_Bin2D}};
            std::map<TString, vector<vector<double *>>> weight_err = {
                    {"electron", weight_err_el},
                    {"muon",     weight_err_mu}
            };

            for (const auto &bins: weight_err_mu) {
                for (auto values: bins) *values = 1.0;
            }
            for (const auto &bins: weight_err_el) {
                for (auto values: bins) *values = 1.0;
            }

            // Apply FF to anti-tight muon event
            for (unsigned i = 0; i < pT_range.at(Fake_Lep_idx - 1).size(); ++i) {
                if (lep_Pt[Fake_Lep_idx] < pT_range.at(Fake_Lep_idx - 1).at(i)) {
                    weight = ff.at(Fake_Lep_idx - 1).at(i);
                    *weight_err.at(lep).at(0).at(i) = weight + ff_err.at(Fake_Lep_idx - 1).at(i);
                    *weight_err.at(lep).at(1).at(i) = weight - ff_err.at(Fake_Lep_idx - 1).at(i);
                    break;
                }
            }
        }
    };

    if (anti_tight && abs(lep_ID[Fake_Lep_idx]) == 13) categorize_ff(false); // muon
    if (anti_tight && abs(lep_ID[Fake_Lep_idx]) == 11) categorize_ff(true); // electron

    return std::make_tuple(result, weight);
}

bool nTuple_Analysis::anti_tight_cut(bool lep_quality1, bool lep_quality2) {
    bool result = false;
    Fake_Lep_idx = -1;
    // lee
    if (FlavorCategory == 1 || FlavorCategory == 5) {
        if (lep_quality1 && !lep_quality2) {
            FakeCategory = 1;
            result = true;
            Fake_Lep_idx = 2;
        }
        if (!lep_quality1 && lep_quality2) {
            FakeCategory = 1;
            result = true;
            Fake_Lep_idx = 1;
        }
    }
        // lmumu
    else if (FlavorCategory == 4 || FlavorCategory == 8) {
        if (lep_quality1 && !lep_quality2) {
            FakeCategory = 2;
            result = true;
            Fake_Lep_idx = 2;
        }
        if (!lep_quality1 && lep_quality2) {
            FakeCategory = 2;
            result = true;
            Fake_Lep_idx = 1;
        }
    }
        // lemu && lmue
        // 3: e anti-lep_quality
        // 4: mu anti-lep_quality
    else {
        if (lep_quality1 && !lep_quality2) {
            Fake_Lep_idx = 2;
            if (FlavorCategory == 3 || FlavorCategory == 7) {
                FakeCategory = 3;
                result = true;
            }
            if (FlavorCategory == 2 || FlavorCategory == 6) {
                FakeCategory = 4;
                result = true;
            }
        }
        if (!lep_quality1 && lep_quality2) {
            Fake_Lep_idx = 1;
            if (FlavorCategory == 3 || FlavorCategory == 7) {
                FakeCategory = 4;
                result = true;
            }
            if (FlavorCategory == 2 || FlavorCategory == 6) {
                FakeCategory = 3;
                result = true;
            }
        }
    }

    return result;
}

void nTuple_Analysis::read_tree(const vector<TString> &dir, const TString &tree_name,
                                const TString &Sample_Name, Sample_Type st, double sw) {
    sample_type = st;
    sample_name = Sample_Name;
    sample_weight = sw;

    tc = new TChain(tree_name);
    for (const auto &d: dir) {
        tc->AddFile(d);
    }

    bool is_data = false;
    if (sample_type == Sample_Type::DATA || sample_type == Sample_Type::FAKE_DATA)
        is_data = true;

    setTree(new nominal(dynamic_cast<TTree *>(tc), is_data));
}

void nTuple_Analysis::Save_Tree() {
    if (f_out) {
        f_out->cd();
        t_out->Write("", TObject::kOverwrite);
        cutflow->Write("", TObject::kOverwrite);
        cutflow_w->Write("", TObject::kOverwrite);
        f_out->Close();
    }

}

nTuple_Analysis::~nTuple_Analysis() {

    delete cutflow;
    delete cutflow_w;

    delete f_out;
    delete tc;
    delete tl;

}

tuple<bool, double> nTuple_Analysis::template_fit_apply() const {

    if (Class_HF || Class_LF || Class_Others) return {true, dControl->NF_HF_e};
    if (Class_ExtConv || Class_IntConv) return {true, dControl->NF_MatConv_e};

    if (Class_HF_m || Class_LF_m) return {true, dControl->NF_HF_m};

    return {false, 1.};
}

Class_Region nTuple_Analysis::template_fit_region(bool syst) {
    Class_Region region;

    auto two_electron = abs(lep_ID[1]) == 11 && abs(lep_ID[2]) == 11;
    auto two_muon = abs(lep_ID[1]) == 13 && abs(lep_ID[2]) == 13;
    { // Internal/Material Conversion:

        // everything but author cut (ambiguity cut)
        auto preselection = syst ? cut_flow(vector<int>({7, 11, 12, 13, 14})) && !(MlllCut) && !Evt_AuthorCut && (!Evt_Tight && Evt_ID_Tight)
                                 : cut_flow(vector<int>({11, 12, 13})) && !(MlllCut);

        auto lep_Mtrktrk_atConvV_CO_1 = (tl->lep_Mtrktrk_atConvV_CO_1 < 0.1 && tl->lep_Mtrktrk_atConvV_CO_1 > 0);
        auto lep_Mtrktrk_atConvV_CO_2 = (tl->lep_Mtrktrk_atConvV_CO_2 < 0.1 && tl->lep_Mtrktrk_atConvV_CO_2 > 0);

        auto lep_IntConv_atPV_CO_1 = (tl->lep_Mtrktrk_atPV_CO_1 < 0.1 && tl->lep_Mtrktrk_atPV_CO_1 > 0);
        auto lep_IntConv_atPV_CO_2 = (tl->lep_Mtrktrk_atPV_CO_2 < 0.1 && tl->lep_Mtrktrk_atPV_CO_2 > 0);

        auto material_con_1 =
                (abs(lep_ID[1]) == 11 && (tl->lep_RadiusCO_1 > 20 && lep_Mtrktrk_atConvV_CO_1)) || abs(lep_ID[1]) == 13;
        auto material_con_2 =
                (abs(lep_ID[2]) == 11 && (tl->lep_RadiusCO_2 > 20 && lep_Mtrktrk_atConvV_CO_2)) || abs(lep_ID[2]) == 13;

        auto ele_1 = (abs(tl->lep_ID_1) == 13) ||
                     (abs(tl->lep_ID_1) == 11 &&
                      (!(lep_IntConv_atPV_CO_1 && !(tl->lep_RadiusCO_1 > 20 && lep_Mtrktrk_atConvV_CO_1)) &&
                       !(tl->lep_RadiusCO_1 > 20 && lep_Mtrktrk_atConvV_CO_1)));
        auto ele_2 = (abs(tl->lep_ID_2) == 13) ||
                     (abs(tl->lep_ID_2) == 11 &&
                      (!(lep_IntConv_atPV_CO_2 && !(tl->lep_RadiusCO_2 > 20 && lep_Mtrktrk_atConvV_CO_2)) &&
                       !(tl->lep_RadiusCO_2 > 20 && lep_Mtrktrk_atConvV_CO_2)));

        auto internal_con_1 = abs(lep_ID[1]) == 11 && (lep_IntConv_atPV_CO_1);
        auto internal_con_2 = abs(lep_ID[2]) == 11 && (lep_IntConv_atPV_CO_2);

        region.external_conversion = preselection && (material_con_1 && material_con_2) && !(ele_1 & ele_2);

        region.internal_conversion =
                preselection && (internal_con_1 ^ internal_con_2) && !(material_con_1 || material_con_2);
    }

    { // Heavy Flavor e
        // everything but requires bJets
        auto preselection = syst ? cut_flow(vector<int>({4, 9, 11, 14})) && (nbJets >= 2) && (nJets >= 2) &&
                                   !Evt_ID_Tight
                                 : cut_flow(vector<int>({4, 9, 11, 14})) && (nbJets >= 2) && (nJets >= 2) &&
                                   Evt_ID_Tight;

        region.hf_e = preselection && two_electron;
        region.hf_m = preselection && two_muon;
    }

    { // Light Flavor e
        auto preselection =
                cut_flow(vector<int>({11, 13})) && (nJets >= 1) && !MlllCut;

        region.lf_e = preselection && two_electron && !region.hf_e;
    }

    return region;
}

Sample_Truth_Type nTuple_Analysis::truth_region_analysis() {
    Sample_Truth_Type sample;

    auto brem_election_1 = (tl->lep_truthParentPdgId_1 == (int) tl->lep_ID_1 && tl->lep_truthParentType_1 == 2);
    auto brem_election_2 = (tl->lep_truthParentPdgId_2 == (int) tl->lep_ID_2 && tl->lep_truthParentType_2 == 2);
    auto prompt_e_1 = (abs(tl->lep_ID_1) == 11 && tl->lep_truthOrigin_1 == 5 && brem_election_1);
    auto prompt_e_2 = (abs(tl->lep_ID_2) == 11 && tl->lep_truthOrigin_2 == 5 && brem_election_2);
    auto prompt_m_1 = (abs(tl->lep_ID_1) == 13 && tl->lep_truthOrigin_1 == 0);
    auto prompt_m_2 = (abs(tl->lep_ID_2) == 13 && tl->lep_truthOrigin_2 == 0);

    auto prompt_1 = (tl->lep_isPrompt_1 || prompt_m_1 || prompt_e_1);
    auto prompt_2 = (tl->lep_isPrompt_2 || prompt_m_2 || prompt_e_2);

    {
        // prompt lepton:
        sample.prompt = prompt_1 && prompt_2;

        // Miss charge
        sample.QMisID = !((tl->lep_isQMisID_0 == 0) && (tl->lep_isQMisID_1 == 0) && (tl->lep_isQMisID_2 == 0));
        if (sample.QMisID || sample.prompt) return sample;
    }

    if (sample.QMisID || sample.prompt) cerr << "Strange!!" << endl;
    // Conversion
    auto conv = (tl->lep_truthOrigin_1 == 5 && !brem_election_1) ||
                (tl->lep_truthOrigin_2 == 5 && !brem_election_2);

    auto qed = (tl->lep_truthParentType_1 == 21 && tl->lep_truthParentOrigin_1 == 0) ||
               (tl->lep_truthParentType_2 == 21 && tl->lep_truthParentOrigin_2 == 0);

//    sample.external_conversion = s_conv && !s_qed;
    sample.external_conversion = conv && !qed;
    sample.internal_conversion = conv && qed;

//    if (s_conv != conv) cerr << "Conv Strange!!" << endl;
//    if (s_qed != qed) cerr << "QED Strange!!" << endl;

    { // Heavy-Flavor Electron
        auto hf_1 = (tl->lep_truthOrigin_1 >= 25 && tl->lep_truthOrigin_1 <= 29)
                    || tl->lep_truthOrigin_1 == 32
                    || tl->lep_truthOrigin_1 == 33;
        auto hf_2 = (tl->lep_truthOrigin_2 >= 25 && tl->lep_truthOrigin_2 <= 29)
                    || tl->lep_truthOrigin_2 == 32
                    || tl->lep_truthOrigin_2 == 33;

        // classified as hadron, originated from unknown source
//        auto had_1 = tl->lep_truthType_1 == 17;
//        auto had_2 = tl->lep_truthType_2 == 17;

        sample.hf_e = ((hf_1 && abs(tl->lep_ID_1) == 11) || (hf_2 && abs(tl->lep_ID_2) == 11)) &&
                      (!(prompt_e_1 || tl->lep_isPrompt_1) && abs(tl->lep_ID_1) == 11 ||
                       !(prompt_e_2 || tl->lep_isPrompt_2) && abs(tl->lep_ID_2) == 11) &&
                      !conv;
        sample.hf_m = ((hf_1 && abs(tl->lep_ID_1) == 13) || (hf_2 && abs(tl->lep_ID_2) == 13)) &&
                      (!(prompt_m_1 || tl->lep_isPrompt_1) && abs(tl->lep_ID_1) == 13 ||
                       !(prompt_m_2 || tl->lep_isPrompt_2) && abs(tl->lep_ID_2) == 13) &&
                      !conv;

        // LF
//        auto light_1 = tl->lep_truthOrigin_1 == 23 || tl->lep_truthOrigin_1 == 24
//                       || tl->lep_truthOrigin_1 == 30 || tl->lep_truthOrigin_1 == 31
//                       || tl->lep_truthOrigin_1 == 34 || tl->lep_truthOrigin_1 == 35
//                       || tl->lep_truthOrigin_1 == 45;
//        auto light_2 = tl->lep_truthOrigin_2 == 23 || tl->lep_truthOrigin_2 == 24
//                       || tl->lep_truthOrigin_2 == 30 || tl->lep_truthOrigin_2 == 31
//                       || tl->lep_truthOrigin_2 == 34 || tl->lep_truthOrigin_2 == 35
//                       || tl->lep_truthOrigin_2 == 45;

        sample.lf_e = !((hf_1 && abs(tl->lep_ID_1) == 11) || (hf_2 && abs(tl->lep_ID_2) == 11)) &&
                      (!(prompt_e_1 || tl->lep_isPrompt_1) && abs(tl->lep_ID_1) == 11 ||
                       !(prompt_e_2 || tl->lep_isPrompt_2) && abs(tl->lep_ID_2) == 11) &&
                      !conv;
        sample.lf_m = !((hf_1 && abs(tl->lep_ID_1) == 13) || (hf_2 && abs(tl->lep_ID_2) == 13)) &&
                      (!(prompt_m_1 || tl->lep_isPrompt_1) && abs(tl->lep_ID_1) == 13 ||
                       !(prompt_m_2 || tl->lep_isPrompt_2) && abs(tl->lep_ID_2) == 13) &&
                      !conv;

        sample.others = !conv && (!(prompt_e_1 || tl->lep_isPrompt_1) && abs(tl->lep_ID_1) == 11 ||
                                  !(prompt_e_2 || tl->lep_isPrompt_2) && abs(tl->lep_ID_2) == 11);
    }

    { // Others
        auto only_prompt_muon_1 = abs(lep_ID[1]) != 13 || prompt_1;
        auto only_prompt_muon_2 = abs(lep_ID[2]) != 13 || prompt_2;

//        sample.others = only_prompt_muon_1 && only_prompt_muon_2 && non_prompt_e;

    }

    return sample;
}

double nTuple_Analysis::GetHiggsness() {
    double met_phi = tl->met_phi;
    std::vector<double> pos;
    pos.push_back(MET_ET);
    pos.push_back(met_phi);
    for (int lep = 0; lep < 3; lep++) {
        pos.push_back(lep_Pt[lep]);
        pos.push_back(lep_Eta[lep]);
        pos.push_back(lep_Phi[lep]);
        pos.push_back(lep_E[lep]);
    }
    pos.push_back(leadJetPt);
    pos.push_back(leadJetEta);
    pos.push_back(leadJetPhi);
    pos.push_back(leadJetE);
    pos.push_back(subleadJetPt);
    pos.push_back(subleadJetEta);
    pos.push_back(subleadJetPhi);
    pos.push_back(subleadJetE);

    std::vector<double> meas;
    meas.push_back(265.);
    meas.push_back(125.);
    meas.push_back(125.);
    meas.push_back(75.);
    meas.push_back(80.379);
    meas.push_back(39.);
    meas.push_back(3.1415);
    //meas.push_back(40.);
    //meas.push_back(80.);

    std::vector<double> var;
    var.push_back(10.);
    var.push_back(3.);
    var.push_back(5.);
    var.push_back(5.);
    var.push_back(5.);
    var.push_back(5.);
    var.push_back(5.);
    //var.push_back(1.);
    //var.push_back(15.);

    HiggsFcn theFCN(meas, pos, var);

    MnUserParameters upar;
    upar.Add("v0_px", 0.3 * MET_ET * TMath::Cos(met_phi), 0.01);
    upar.Add("v0_py", 0.3 * MET_ET * TMath::Sin(met_phi), 0.01);
    upar.Add("v0_pz", 0., 0.01);
    upar.Add("v1_px", 0.3 * MET_ET * TMath::Cos(met_phi), 0.01);
    upar.Add("v1_py", 0.3 * MET_ET * TMath::Sin(met_phi), 0.01);
    upar.Add("v1_pz", 0., 0.01);
    upar.Add("v2_pz", 0., 0.01);

    upar.SetLimits("v0_pz", -100., +100.);
    upar.SetLimits("v1_pz", -100., +100.);
    upar.SetLimits("v2_pz", -100., +100.);

    // starting value for parameters
    // use the midpoint of MET
    std::vector<double> init_par;
    init_par.push_back(0.3 * MET_ET * TMath::Cos(met_phi));
    init_par.push_back(0.3 * MET_ET * TMath::Sin(met_phi));
    init_par.push_back(0.);
    init_par.push_back(0.3 * MET_ET * TMath::Cos(met_phi));
    init_par.push_back(0.3 * MET_ET * TMath::Sin(met_phi));
    init_par.push_back(0.);
    init_par.push_back(0.);

    // starting values for initial uncertainties
    std::vector<double> init_err;
    init_err.push_back(0.1);
    init_err.push_back(0.1);
    init_err.push_back(0.1);
    init_err.push_back(0.1);
    init_err.push_back(0.1);
    init_err.push_back(0.1);
    init_err.push_back(0.1);


//    MnMigrad migrad(theFCN, upar);
    MnSimplex msim(theFCN, init_par, init_err);

    // minimize
//    VariableMetricMinimizer theMinimizer;
//    FunctionMinimum min = theMinimizer.Minimize(theFCN, init_par, init_err);
    FunctionMinimum min = msim();
//    FunctionMinimum min = migrad();
    if (!min.IsValid()) {
//        cout << log(min.Fval()) << ", invalid" << endl;
        return -1;
    }
//    MnMinos minos(theFCN, min);
//
//    // 1-sigma MINOS errors
//    std::pair<double, double> e0 = minos(0);
//    std::pair<double, double> e1 = minos(1);
//    std::pair<double, double> e2 = minos(2);
//    std::pair<double, double> e3 = minos(3);
//    std::pair<double, double> e4 = minos(4);
//    std::pair<double, double> e5 = minos(5);
//    std::pair<double, double> e6 = minos(6);
//
//    // output
//    cout << "MET: " << MET_ET << ", MET_Phi: " << met_phi << endl;
//    cout << "met_x: " << MET_ET * TMath::Cos(met_phi) << ", met_y: " << MET_ET * TMath::Sin(met_phi) << endl;
//    std::cout << "v0_px: " << min.UserState().Value(0) << ", error: " << e0.first << " " << e0.second << std::endl;
//    std::cout << "v0_py: " << min.UserState().Value(1) << ", error: " << e1.first << " " << e1.second << std::endl;
//    std::cout << "v0_pz: " << min.UserState().Value(2) << ", error: " << e2.first << " " << e2.second << std::endl;
//    std::cout << "v1_px: " << min.UserState().Value(3) << ", error: " << e3.first << " " << e3.second << std::endl;
//    std::cout << "v1_py: " << min.UserState().Value(4) << ", error: " << e4.first << " " << e4.second << std::endl;
//    std::cout << "v1_pz: " << min.UserState().Value(5) << ", error: " << e5.first << " " << e5.second << std::endl;
//    std::cout << "v2_pz: " << min.UserState().Value(6) << ", error: " << e6.first << " " << e6.second << std::endl;
//    std::cout << "log(H): " << log(min.Fval()) << std::endl;
//    std::cout << "Edm: " << min.Edm() << std::endl;
//    std::cout << "nfcn: " << min.NFcn() << std::endl;
//    std::cout << "IsValid: " << min.IsValid() << std::endl;
//    std::cout << "HasValidParameters: " << min.HasValidParameters() << std::endl;

    return min.Fval();
}

bool nTuple_Analysis::dz_cut() {
    bool result =
            (abs(tl->lep_ID_0) == 11 ? tl->lep_d0_0 / tl->lep_sigd0PV_0 < 5 : tl->lep_d0_0 / tl->lep_sigd0PV_0 < 3) &&
            (abs(tl->lep_ID_1) == 11 ? tl->lep_d0_1 / tl->lep_sigd0PV_1 < 5 : tl->lep_d0_1 / tl->lep_sigd0PV_1 < 3) &&
            (abs(tl->lep_ID_2) == 11 ? tl->lep_d0_2 / tl->lep_sigd0PV_2 < 5 : tl->lep_d0_2 / tl->lep_sigd0PV_2 < 3);
    result &= abs(tl->lep_Z0SinTheta_0) < 0.5 && abs(tl->lep_Z0SinTheta_1) < 0.5 && abs(tl->lep_Z0SinTheta_2) < 0.5;

    result &= ((abs(tl->lep_ID_0) == 11) ? fabs(tl->lep_Eta_0) < 2.47 &&
                                           !(fabs(tl->lep_Eta_0) >= 1.37 && (fabs(tl->lep_Eta_0) <= 1.52)) :
               fabs(tl->lep_Eta_0) < 2.5) &&
              ((abs(tl->lep_ID_1) == 11) ? fabs(tl->lep_Eta_1) < 2.47 &&
                                           !(fabs(tl->lep_Eta_1) >= 1.37 && (fabs(tl->lep_Eta_1) <= 1.52)) :
               fabs(tl->lep_Eta_1) < 2.5) &&
              ((abs(tl->lep_ID_2) == 11) ? fabs(tl->lep_Eta_2) < 2.47 &&
                                           !(fabs(tl->lep_Eta_2) >= 1.37 && (fabs(tl->lep_Eta_2) <= 1.52)) :
               fabs(tl->lep_Eta_2) < 2.5);

    return result;
}





