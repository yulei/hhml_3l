//
// Created by Zhang Yulei on 7/6/21.
//

#include "nTuple_Analysis.h"
#include "Control.h"

#include <iostream>
#include <fstream>

using std::cout, std::endl;

vector<TString> Generate_DIR(const TString &sample_name) {
    vector<const char *> fv;
    vector<TString> rv;

    if (sample_name == "data") {
        // Data
        fv.push_back(Form("%s_y15", sample_name.Data()));
        fv.push_back(Form("%s_y16", sample_name.Data()));
        fv.push_back(Form("%s_y17", sample_name.Data()));
        fv.push_back(Form("%s_y18", sample_name.Data()));
    } else if (sample_name.Contains("_a") || sample_name.Contains("_d") || sample_name.Contains("_e")) {
        fv.push_back(sample_name);
    } else {
        fv.push_back(Form("%s_a", sample_name.Data()));
        fv.push_back(Form("%s_d", sample_name.Data()));
        fv.push_back(Form("%s_e", sample_name.Data()));
    }

    for (auto f : fv) {
        char buffer[256];
        strncpy(buffer, "LocalRootDir/", sizeof(buffer) - strlen(buffer) - 1);
        strncat(buffer, f, sizeof(buffer) - strlen(buffer) - 1);
        cout << "==> Sample: " << buffer << endl;

        std::ifstream f_(buffer);
        TString rootfile;
        while (f_ >> rootfile) {
        //cout << "-- Chain \033[1;31m" << f << "\033[0m Add File: " << rootfile << " ..." << endl;
            rv.emplace_back(rootfile);
        }
    }

    return rv;
}

void Analysis(const vector<TString> &dirs, const TString &Sample_Name, Sample_Type st, double sw) {
    TString out_dir = dControl->out_dir;

    nTuple_Analysis na;
    na.read_tree(dirs, "nominal", Sample_Name, st, sw);
    if (st == Sample_Type::FAKE_MC || st == Sample_Type::FAKE_DATA)
        na.Book_Tree(Form("%s/%s_fake.root", out_dir.Data(), Sample_Name.Data()), "total");
    else
        na.Book_Tree(Form("%s/%s_selected.root", out_dir.Data(), Sample_Name.Data()), "total");
    na.Loop_Tree();
    na.Save_Tree();

    cout << "-- "<< Sample_Name<<": Done" << endl;
}

int main(int argc, char *argv[]) {

    auto run_number = std::atoi(argv[1]);
    bool run_fakes = false;
    if (argc == 3)
        run_fakes = (TString(argv[2]) == "-f");

    Control::CreateInstance();
    dControl->ReadYAML("default.yaml");
    cout << "[Control] ==> Available group size: " << dControl->groups.size() << endl;
    cout << "[Control] ==> Running for group: " << run_number << "/" << dControl->groups.size() - 1 << endl;

    for (const auto &g : dControl->groups.at(run_number)) {
        auto rv = Generate_DIR(g.first);

        // Change Sample Type
        auto s_t = g.second;
        if (run_fakes) {
            if (g.second == Sample_Type::DATA) s_t = Sample_Type::FAKE_DATA;
            else if (g.second == Sample_Type::PROMPT || g.second == Sample_Type::MCFAKE) s_t = Sample_Type::FAKE_MC;
            else {
                std::cerr << "[ERROR] ==> No fakes allowed for this type ..." << std::endl;
                return -1;
            }
        }
        if (rv.empty()) {
            std::cout << "[Warning] ==> Empty ntuple address for sample: " << g.first << std::endl;
            continue;
        }

        Analysis(rv, g.first, s_t, 1.0);
    }

    cout << "==> Done..." << endl;

    return 0;
}

